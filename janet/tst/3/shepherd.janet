(def skadi @{ :name "Skadi" :type "German Shepherd"} )
(def odin @{ :name "Odin" :type "German Shepherd"} )

(def people
  [{ :name "Ian" :dogs [skadi odin] }
   { :name "Kelsey" :dogs [skadi odin] }
   { :name "Jeffrey" :dogs [] } ]
  )

(pp people)

(defn main [&]
  (set (odin :type) "Well mostly German Shepherd, a bit mixed")
  (pp people)
  )
