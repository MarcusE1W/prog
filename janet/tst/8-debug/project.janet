(declare-project
  :name "8-debug"
  :description ```play around with debugging ```
  :version "0.0.0")

(declare-executable
  :name "8-debug"
  :entry "8-debug/init.janet")