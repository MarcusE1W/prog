(declare-project
  :name "example"
  :description ```Example to set up an executable program with jpm ```
  :version "0.0.0")

(declare-executable
  :name "example"
  :entry "example/init.janet")