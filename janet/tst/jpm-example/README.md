# example

1. create new project 

```
jpm new-exe-project example
```

this creates next this project.janet file

```
(declare-project
  :name "example"
  :description ```Example project.janet file to create executable ```
  :version "0.0.0")

(declare-executable
 :name "example"
 :entry "example/init.janet"
)
```

2. execute jpm to create executable

`jpm build`

3. run new prog

`./build/example`

 
