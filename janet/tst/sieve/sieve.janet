#!/usr/bin/env janet

# Sieve

(defn initSieve 
  "Initialise Sieve mit null"
  [sieve maxSieve]
  (for i 0 maxSieve (put sieve i true))
  )

(defn tickSieve
  "set non primes to flase"
  [i sieve maxSieve]
  (var j i)
  (while (<= j maxSieve)
	(put sieve j false)
	(+= j i))
  )


(defn checkSieve
  "check all prime numbers"
  [sieve maxSieve]
  (var j 0)
  (for i 2 maxSieve
	(if (= (in sieve i) true)
	  (do # do required because if only accepts one expression for 'true' branch
		(tickSieve i sieve maxSieve)
		(prin " " i ", "))))
  )
				

(defn main
  "Sieve of Erastotholes for prime numbers"
  [& args]  #?

  (def [_ arg_two _] args) # pattern matching to get second parameter and ignore rest
  (def maxSieve (scan-number arg_two)) # convert string to number

  (def sieve (array/new (+ 1 maxSieve ))) # +1 to get an actual index maxSieve

  (print "let's start")
  (initSieve sieve maxSieve)
  (checkSieve sieve maxSieve)
  (print "\nfinished") #new line and "finished"
  )
