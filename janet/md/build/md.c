#include <janet.h>
static const unsigned char bytes[] = {215, 0, 205, 0, 221, 0, 0, 5, 0, 0, 205, 127, 255, 255, 255, 4, 12, 0, 2, 206, 4, 109, 97, 105, 110, 206, 13, 109, 100, 47, 105, 110, 105, 116, 46, 106, 97, 110, 101, 116, 206, 10, 35, 32, 72, 101, 97, 100, 101, 114, 32, 49, 215, 0, 205, 0, 220, 0, 0, 3, 1, 1, 1, 1, 3, 0, 2, 206, 9, 99, 104, 101, 99, 107, 76, 105, 110, 101, 218, 2, 215, 0, 205, 0, 220, 0, 0, 3, 1, 1, 1, 1, 2, 0, 2, 206, 11, 99, 104, 101, 99, 107, 72, 101, 97, 100, 101, 114, 218, 2, 206, 33, 60, 72, 69, 65, 68, 62, 32, 83, 97, 109, 112, 108, 101, 32, 72, 101, 97, 100, 101, 114, 32, 76, 105, 110, 101, 32, 60, 47, 72, 69, 65, 68, 62, 0, 2, 0, 207, 6, 104, 101, 97, 100, 101, 114, 0, 2, 1, 207, 11, 99, 104, 101, 99, 107, 72, 101, 97, 100, 101, 114, 42, 2, 0, 0, 3, 2, 0, 0, 9, 1, 0, 1, 0, 3, 0, 207, 3, 116, 120, 116, 0, 3, 1, 207, 9, 99, 104, 101, 99, 107, 76, 105, 110, 101, 47, 0, 0, 0, 42, 2, 0, 0, 52, 2, 0, 0, 19, 3, 0, 3, 0, 3, 216, 5, 112, 114, 105, 110, 116, 215, 0, 205, 0, 220, 0, 0, 2, 0, 0, 0, 1, 2, 0, 1, 206, 5, 104, 101, 108, 108, 111, 218, 2, 206, 6, 72, 101, 108, 108, 111, 33, 0, 2, 0, 207, 5, 104, 101, 108, 108, 111, 42, 1, 0, 0, 3, 1, 0, 0, 1, 1, 0, 1, 0, 12, 0, 207, 4, 97, 114, 103, 115, 0, 12, 1, 207, 4, 109, 97, 105, 110, 42, 2, 0, 0, 47, 2, 0, 0, 42, 3, 1, 0, 51, 2, 3, 0, 47, 2, 0, 0, 42, 4, 2, 0, 51, 3, 4, 0, 42, 4, 3, 0, 51, 2, 4, 0, 47, 2, 0, 0, 42, 4, 2, 0, 52, 4, 0, 0, 25, 10, 0, 10, 0, 10, 0, 10, 0, 3, 0, 3, 0, 3, 1, 10, 0, 10, 0, 3, 0, 3, 0, 3};

const unsigned char *janet_payload_image_embed = bytes;
size_t janet_payload_image_embed_size = sizeof(bytes);

int main(int argc, const char **argv) {

#if defined(JANET_PRF)
    uint8_t hash_key[JANET_HASH_KEY_SIZE + 1];
#ifdef JANET_REDUCED_OS
    char *envvar = NULL;
#else
    char *envvar = getenv("JANET_HASHSEED");
#endif
    if (NULL != envvar) {
        strncpy((char *) hash_key, envvar, sizeof(hash_key) - 1);
    } else if (janet_cryptorand(hash_key, JANET_HASH_KEY_SIZE) != 0) {
        fputs("unable to initialize janet PRF hash function.\n", stderr);
        return 1;
    }
    janet_init_hash_key(hash_key);
#endif

    janet_init();

    /* Get core env */
JanetTable *env = janet_core_env(NULL);
JanetTable *lookup = janet_env_lookup(env);
JanetTable *temptab;
int handle = janet_gclock();    /* Unmarshal bytecode */
    Janet marsh_out = janet_unmarshal(
      janet_payload_image_embed,
      janet_payload_image_embed_size,
      0,
      lookup,
      NULL);

    /* Verify the marshalled object is a function */
    if (!janet_checktype(marsh_out, JANET_FUNCTION)) {
        fprintf(stderr, "invalid bytecode image - expected function.");
        return 1;
    }
    JanetFunction *jfunc = janet_unwrap_function(marsh_out);

    /* Check arity */
    janet_arity(argc, jfunc->def->min_arity, jfunc->def->max_arity);

    /* Collect command line arguments */
    JanetArray *args = janet_array(argc);
    for (int i = 0; i < argc; i++) {
        janet_array_push(args, janet_cstringv(argv[i]));
    }

    /* Create enviornment */
    temptab = env;
    janet_table_put(temptab, janet_ckeywordv("args"), janet_wrap_array(args));
    janet_table_put(temptab, janet_ckeywordv("executable"), janet_cstringv(argv[0]));
    janet_gcroot(janet_wrap_table(temptab));

    /* Unlock GC */
    janet_gcunlock(handle);

    /* Run everything */
    JanetFiber *fiber = janet_fiber(jfunc, 64, argc, argc ? args->data : NULL);
    fiber->env = temptab;
#ifdef JANET_EV
    janet_gcroot(janet_wrap_fiber(fiber));
    janet_schedule(fiber, janet_wrap_nil());
    janet_loop();
    int status = janet_fiber_status(fiber);
    janet_deinit();
    return status;
#else
    Janet out;
    JanetSignal result = janet_continue(fiber, janet_wrap_nil(), &out);
    if (result != JANET_SIGNAL_OK && result != JANET_SIGNAL_EVENT) {
      janet_stacktrace(fiber, out);
      janet_deinit();
      return result;
    }
    janet_deinit();
    return 0;
#endif
}
