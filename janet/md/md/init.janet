(defn hello
  `Evaluates to "Hello!"`
  []
  "Hello!")

(def header1 "# Header 1")
(def header3 "### Header 3")

(defn checkHeader
  "parse Header line"
  [header]

  (peg/match ~(sequence (capture (some "#")) (capture (any (choice :w :W))) ) "### Hhhhh")

  "<HEAD> Sample Header Line </HEAD>"
  )
		

(defn checkLine
  "parse one line"
  [txt]
  (checkHeader txt)
  #checkBold
  #checkItalics
  #checkLink -- number reference with link later ?
  #checkPicLink
  #checkList
  #checkNumList
  )
  

(defn main
  [& args]
  (print (checkLine header1))
  (print (hello)))
