(declare-project
  :name "md"
  :description ```A simple markdown to HTML converter ```
  :version "0.0.1")

(declare-executable
  :name "md"
  :entry "md/init.janet")
