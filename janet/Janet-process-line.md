Janet-process-line

Sure, here's an example program in Janet that reads a file line by line and processes each line according to the value before the : character:

janet code:

``` janet
(defn process-line [line]
  (let [parts (string/split line #":")
        action (trim (nth parts 0))
        value (trim (nth parts 1))]
    (case action
      "print" (print value)
      "add" (print (string/concat "Added: " value))
      "multiply" (print (* (tonumber value) 2))
      ; add more cases here for other actions
      :default (print (string/concat "Invalid action: " action)))))

(defn process-file [filename]
  (with-open [file (file/reader filename)]
    (each line (file/lines file)
      (process-line line))))

```

This program defines a process-line function that takes a single line of input, splits it into two parts using the : character as the separator, and then performs an action based on the value of the first part. The process-file function reads the file line by line using the with-open macro and passes each line to the process-line function.

In this example, the program only handles three different actions: print, add, and multiply. If the first part of a line doesn't match any of these actions, it prints an error message. You can add more cases to the case expression in the process-line function to handle other actions as needed.