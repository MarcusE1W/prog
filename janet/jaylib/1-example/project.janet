(declare-project
  :name "1-example"
  :description ```jaylib example ```
  :version "0.0.0")

(declare-executable
  :name "1-example"
  :entry "1-example/init.janet")