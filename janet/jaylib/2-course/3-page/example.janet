(import jaylib :as jl)

(def screen-width  800)
(def screen-height 400)

(defn init-game
  []
  (jl/init-window screen-width
                  screen-height
                  "Title Goes Here")
  (jl/set-target-fps 60)
  (jl/hide-cursor))

(defn update-game
  []
  (comment "Nothing to update at the moment."))

(defn draw-game
  []
  (let [[x y] (jl/get-mouse-position)]
    (jl/draw-circle-gradient x y 40 :lime :red)))

(defn main
  [& args]
  (init-game)
  (while (not (jl/window-should-close))
    (update-game)
    (jl/begin-drawing)
    (jl/clear-background [0 0 0])
    (draw-game)
    (jl/end-drawing))
  (jl/close-window))
