(declare-project
  :name "fmj"
  :description ```Janet Awesome Terminal File Manager ```
  :version "0.0.0")

(declare-source
  :prefix "fmj"
  :source ["src/init.janet"])

(declare-executable
 :name "fmj"
 :entry "fmj/init.janet"
#  :install false
)
