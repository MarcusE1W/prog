(defn redrawScreen
  `Write screen based on current state`
  []
  # write menu
  (print "Menu")
  # write file windows
  (print "File Window")
  # write status line
  (print "statusLine")
  )

(defn main
  [& args]
  (def quit false)
  (redrawScreen)
)
