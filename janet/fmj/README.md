# Janet Awesome file manager

- Two (std) or three windows (optional)
- settings are read from a file

# Functional Spec
## file functions
- open file (v 2.0)
- Copy
- Move
- Delete
- Extract (v 1.5)
- Rename
- Create folder
- Size of folders in window (v 1.5)
- Folder size tree (v 2.0)
- last update per file and directory (v 2.0)

# Shortcuts

Active line of shortcuts shown over windows
Two rows, 5 per row
- adapt to window size change (v 2.0)

## Direct
- SPACE : mark
- F1: Help
- 
- F5: refresh

## Shift

F3: Bookmarks of files (v 2.0)

F1: Set Bookmarks of folders
F1: Show Bookmarks of folders (incl. delete) 
F10: exit

## Control
- C-x
- C-v
- C-c
- C-z
- C-e: exit
- C-n
- C-p
- C-a
- C-e
- 

# Technical Spec

## Logic


Init window:
- enter raw mode
- get window size


Event loop with:
  - redraw the window
  - Wait for key
  - Execute function
  - update state

## Window structure
Top Line 1-2 line: Menu structure in inverse colour
Bottom Line 1: Status line in inverse colour

# Data structures

## folder content 1-3

## Function key abbreviation and shortcut
Associated function

## file to app association
Use as a suggestion he file hat is selected
Read system file assiciations (v 2.0)
# Settings

## colour
Use terminal colour



