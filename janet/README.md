# Install

on manjaro ARM
```
pacman -S janet-lang-git
```

# Editor support

see here : https://github.com/ahungry/awesome-janet#editors
see Janet main page (scroll down): https://janet-lang.org/

## emacs

Emacs: M-/ cycles through possible completions

### emacs treesittter

 (erlang     "https://github.com/WhatsApp/tree-sitter-erlang" "main" "src")

https://github.com/sogaiu/janet-ts-mode

# getting started

## Basics

### printing

```
(print x) ## prints x and a new-line
(prin x) ## prints x, no new-line
(pp x) ## pretty print for data structures
(printf "...%.." c)
```

`\n` for a new-line in a string

### getting text input
(getline)

### main function
- [& args] first argument is programme name

http://www.unexpected-vortices.com/janet/notes-and-examples/language-basics.html

## Data and data structures

https://janet.guide/values-and-references/

### :Number

every number is a 64 bit floating point

### :Boolean

```
true
false
(toggle) # swap the value of a boolean
```

Operators:
(and a b)
(or a b)

### :Symbol
for macros

### :String: / :Buffer


### :Tupel: / :Array
### :Struct: / :Table

### Type conversions

#### convert string/buffer into number
(scan-number)

#### mutable table -> immutable struct

useful if the table gets build dynamically but at some point won't change anymore.

```
(table/to-struct cards) # convert table to immutable struct
```

### Comparisons
https://janet-lang.org/docs/comparison.html

### Immutable constants and Variables
https://janet-lang.org/docs/bindings.html

Constant:
(def a 3)

Variable:
(var b 5)
Then change values with set
(set b 7)

TODO: What is the difference of:
(def a 5)
(def a 7) ## works

to 

(var b 5)
(set b 7)
?????

## The Janet core language
https://janet-lang.org/docs/syntax.html

https://janet-lang.org/docs/specials.html


## Functions

### defn
the last experession is the return value

to return a variable, just state the name

### fn
for anonymous functions

```
(fx [x y] (+ x y))
(def txt-files (map (fn [x] (string dir x)) txt-files))
```

### short fn
### varfn ??
### Currying
- with anonymous functions

http://www.unexpected-vortices.com/janet/notes-and-examples/functions.html

## Flow Control

https://janet.guide/control-flow/

### Conditional execution 
#### If
#### Case
#### Cond
### Looping
#### For
#### each / eachk / eachk
#### loop
#### map
- Filter ?
## Destructuring / pattern matching

[ a b ] = ( 100 200) ## or so
[ a _ ] = (100 200)

- TODO: can you decomposition in a case? probaly math
- Pattern matching: same as destructuring ?

### Debugging

https://janet.guide/testing-and-debugging/

(debug)
janet -d xx.janet
(.locale)
(.step)
(.next)

### Testing

## Documentation
https://janet-lang.org/docs/documentation.html

http://www.unexpected-vortices.com/janet/notes-and-examples/docs.html

## PEG
https://bakpakin.com/writing/how-janets-peg-works.html
https://articles.inqk.net/2020/09/19/how-to-use-pegs-in-janet.html
https://gitlab.com/sogaiu/margaret/-/tree/master
https://janet.guide/pegular-expressions/

## Scripting

```
(def [_ first_argument _] args) # ignores the first arg, the programme.name and all the rest ?
```
https://janet.guide/scripting/


## Modules and data hiding??

https://janet.guide/control-flow/
http://www.unexpected-vortices.com/janet/notes-and-examples/modules-packages-libraries.html

## Generic programming
### Prototypes
## Macros

https://janet.guide/macros-and-metaprogramming/

https://janet.guide/macro-mischief/

## Concurrency

https://janet.guide/concurrency-and-coroutines/

- fibres
- Coro
## Multiprocessing
## Networking

## Janet and C : Foreign function interface (FFI)

https://janet-lang.org/docs/ffi.html

## Embedding Janet

https://janet.guide/embedding-janet/

## Object oriented programming

https://janet-lang.org/docs/object_oriented.html

https://janet.guide/tables-and-polymorphism/


# Using janet tools

## janet

### run janet programm

```
janet test.janet
```

### REPL
#### Load prog into REPL
```
(import ./test) # load test.janet from local directory, functions will be prefixed with the module name, use :as to set a prefix
(use .test) # load test.janet from local directory, functions will be loaded without prefix
```


#### Use (doc)
```
(doc) # for help. e.g. `(doc import)
(doc print) # for docu on print
(doc "print" # search for every expression with print in the name
```

#### Types
```
(type a) # to see the type of a 
```

#### To close the REPL
```
(quit)
or
(os/exit)
```
or `Ctrl-d` or `Ctrl-c`


### Compiling to image

https://janet.guide/compilation-and-imagination/

-> use jpm
### Error messages
Error messages are so so

### Linting
https://janet-lang.org/docs/linting.html


## jpm

### install packages
### create new project

> jpm new-project <name>

this creates a new folder with name <name> including a 

```
(declare-project
  :name "<name>"
  :description ```jpm test ```
  :version "0.0.0")

(declare-source
  :prefix "<name>"
  :source ["src/init.janet"])
```

### building / compiling

```
(declare-project
  :name "fmj"
  :description ```Janet Awesome Terminal File Manager ```
  :version "0.0.0")

(declare-source
  :prefix "fmj"
  :source ["src/init.janet"])

(declare-executable
 :name "fmj"
 :entry "fmj/init.janet"
 # :install false
)
```

Command to build an executable:
`jmp build`

## Janet abstract machine
https://janet-lang.org/docs/abstract_machine.html

# Janet questions

## what exactly does immortable mean

## jpm

### source: has "src/.." but folder is <name>/..

# Janet by example

## Adding sets to Janet
https://janet.guide/xenofunctions/

## Databases

http://www.unexpected-vortices.com/janet/notes-and-examples/db.html


# Standard library


# references

https://github.com/ahungry/awesome-janet

https://janetdocs.com/

https://janet-lang.org/docs/index.html

https://ianthehenry.com/posts/janet-game/

- Janet for Mortals: [1](https://janet.guide/)

http://www.unexpected-vortices.com/janet/notes-and-examples/language-basics.html

## References
### Long list of explainations and examples
https://gitlab.com/sogaiu/margaret/-/blob/master/tutorials/tutorial.janet

### Step by step instructions
https://gitlab.com/sogaiu/margaret/-/blob/master/tutorials/steps.janet


### Open questions
- PEG how to capture any character (e.g. till end of line)
- what is exectly the difference between def and var while using it
