(import spork/sh)

(defn getSize
  "get and add size per file"
  [file]

  (def stat (os/stat file))
  ##(print (get stat :size))
  (get stat :size)
  )

# (defn list-all-files
#   "List the files in the given directory recursively. Return the paths to all
#   files found, relative to the current working directory if the given path is a
#   relative path, or as an absolute path otherwise."
#   [dir &opt into]
#   (default into @[])
#   (each name (try (os/dir dir) ([_] @[]))
#     (def fullpath (path/join dir name))
#     (case (os/stat fullpath :mode)
#       :file (array/push into fullpath)
#       :directory (list-all-files fullpath into)))
#   into)



(defn main
  [& args]

  (def files (sh/list-all-files "."))
  ## ( pp files)
  
  ## calk size per file and sumup
  (var sizesum 0)
  (each file files (set sizesum (+ sizesum (getSize file))))
  ## (for [file in: files} (addSize file))
  ## (map (fn [x] 

  (print "Folder size: ")
  (print sizesum)
  )
