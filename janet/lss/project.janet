(declare-project
  :name "lss"
  :description ```List size files an subdirectories ```
  :version "0.1.0")

(declare-executable
  :name "lss"
  :entry "lss/init.janet")
