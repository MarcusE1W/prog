(declare-project
  :name "acetducey"
  :description ```Conversion of the BASIC game Acey Ducey to Janet ```
  :version "0.0.0")

(declare-executable
  :name "acetducey"
  :entry "acetducey/init.janet")