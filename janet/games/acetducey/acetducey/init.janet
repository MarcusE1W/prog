(defn print-greetings
  "Prints the welcome screen"
  []

  (print "\nACEY DUCEY CARD GAME")
  (print "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY")
  (print " ")
  (print " ")
  (print "ACEY-DUCEY IS PLAYED IN THE FOLLOWING MANNER ")
  (print "THE DEALER (COMPUTER) DEALS TWO CARDS FACE UP")
  (print "YOU HAVE AN OPTION TO BET OR NOT BET DEPENDING")
  (print "ON WHETHER OR NOT YOU FEEL THE CARD WILL HAVE")
  (print "A VALUE BETWEEN THE FIRST TWO.")
  (print "IF YOU DO NOT WANT TO BET, INPUT A 0")
  )

(defn create-cards
  "create cards stack"
  []
  (def cards @{}) #temporarily a table to fill the values
  (for i 2 11  ## eleven is not included in loop
	(put cards i (string i))
	)
  (put cards 11 "JACK")
  (put cards 12 "QUEEN")
  (put cards 13 "KING")
  (put cards 14 "ASS")
  (table/to-struct cards) # convert table to immutable struct
  )

(defn randomize
  []
  (math/seedrandom (os/cryptorand 8)) //use os/time or so
)

(defn get-random-card
  []
  (+ 2 (math/trunc(* 13 (math/random))))
  )

(defn print-balance
  [stash]
  (prin "\nYOU HAVE ")
  (prin stash)
  (print "DOLLARS")
  )

(defn print-card 
  [cards card]
  (prin (get cards card))
)

(defn draw-dealer-cards
  [cards]
  (var cardA (get-random-card))
  (var cardB (get-random-card))
  (if (> cardA cardB)
	(do
	  (def tmp cardA)
	  (set cardA cardB)
	  (set cardB tmp)
	  )
	)
  (prin "HERE ARE YOUR NEXT TWO CARDS: ")
  (prin (get cards cardA))
  (prin " ")
  (print (get cards cardB))
  [cardA cardB]
  )

(defn draw-player-card
  [cards]
  (def cardC (get-random-card))
  (print (get cards cardC))
  cardC
)


(defn read-bet
  []
  (def bet (getline))
  (scan-number (string/trim bet))
)

(defn get-bet
  [stash]
  (var bet -1)
  (print "")
  (while (or (< bet 0) (> bet stash))
	(do
	  (prin "WHAT IS YOUR BET :")
	  (set bet (read-bet))
	  (if (> bet stash)	
		(do
		  (print "SORRY, MY FRIEND, BUT YOU BET TOO MUCH.")
		  (prin "YOU HAVE ONLY ")
		  (prin stash)
		  (print " DOLLARS TO BET.")
		  )
		) #if
	  ) #do
	) #while
  bet
  )

(defn play
  [stash cards]
  (var money stash)
  (print-balance money)
  (def [cardA cardB] (draw-dealer-cards cards))
  (def bet (get-bet stash))
  (if (> bet 0)
	(do
	  (def cardC (draw-player-card cards))
	  (if (and (< cardA cardC) (> cardB cardC))
		(do
		  (+= money bet)
		  (print "YOU WIN !!")
		  )
		(do
		  (-= money bet)
		  (print "SORRY, YOU LOOSE")
		  )
		)	  
	  )
	(print "CHICKEN")
	)
  money
  )

(defn play-again
  []
  (def again false)
  again
  )

(defn main
  [& args]
  (def cards (create-cards))
  (print-greetings)
 
  (var tryagain true) 
  (while tryagain
	(do
	  (var stash 100)
      (while (> stash 0)
		(set stash (play stash cards))
		)
	  (print "SORRY, FRIEND, BUT YOU BLEW YOUR WAD.\n")
	  (set tryagain play-again)
	  )
	)
  (print "O.K., HOPE YOU HAD FUN!")
  )

