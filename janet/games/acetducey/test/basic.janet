(use ../acetducey/init)

(for i 1 100
 (do
   (def r (get-random-card))
   (print r)
   (assert (and (> r 1) (< r 15)))))
