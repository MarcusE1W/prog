// The Swift Programming Language
// https://docs.swift.org/swift-book

var sieve:[Bool] = Array(repeating:false, count:101)

for i in sieve[2...] {  --all elements in sieve from to to the end
    if sieve[i] == false {
        print( i )
        for j in stride(from:i to:100 by:i) {
            sieve[j] = true
        }//for
    }//if
}//for

print("Hello, world!")
