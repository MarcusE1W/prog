# Convert

read list of directories
open output file
for each directory
  read list of files
  for each file
    get sub-string from start to first "-" to construct a simpler filename
    replace any space char with "-"
    build new txt filename
    build command
    run command


# Create file

Map
| Value | field |
| --- | --- |
| Combination: | passwd |
| URL: | url |
| User Name: user |
| Password: passwd |
| Number: | -> ID: Social Security Number?
| bank name: |
| PIN: 5680 | -> note
| security code:
| card number:
| card holder:
| Expiration:
| acct#: -> note |
| branch#: -> note |
| Note: | -> note |
| Passport Number: -> ID
| Surname:
| pob: | placeofbirth
| dob: | dateofbirth
| Issue Date:
| doe: 08/10/2017
| Note: Place of issue: (Gemeinde Laer)




Bitwarden generic csv

folder,favorite,type,name,notes,fields,login_uri,login_username,login_password,login_totp


files
@["Sparkasse_Laer_Card.txt" "Slant.txt" "Natwest_joint_account_Card.txt" "GMX_Sure.txt" "British_Social_Security.txt" "Natwestreward.txt" "Obike.txt" "Metro_Card.txt" "HSBC_Card.txt" "Aviva_Pension.txt" "Natwest_App.txt" "Quora.txt" "HSBC_Online.txt" "Firefox_Password_Locker.txt" "Sparkasse_Online.txt" "Passport.txt" "NHS_Patient_access.txt" "indiegogo.txt" "Tagpacker.txt" "Mastodon.txt" "DKB_Online.txt" "Sparkasse_Online.txt" "NHS_Medical_Card.txt" "Streetlife.txt" "DKB_Visa.txt" "HSBC_Online.txt" "Afraid.txt" "NI_NO.txt" "Natwestreward.txt" "Natwest_joint_account_Card.txt" "Cloud9.txt" "Github.txt" "DKB_Visa.txt" "Coinbase.txt" "Paypal.txt" "GMX.txt" "Flybe.txt" "DKB_Online.txt" "Slack.txt" "HSBC_Card.txt" "Slant.txt" "John_Lewis_Partnership_Card.txt" "Natwest_Online.txt" "Metro_online.txt" "Flybe.txt" "Huawei_Wifi_mobile.txt" "Gmail_wendelwang33.txt" "Passport.txt"]
