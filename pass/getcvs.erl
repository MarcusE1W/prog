-module(getcvs).
-export([pass/0]).

print(X) ->
 io:format(X),
 io:format("~n").

files_do(S) ->
	[S1 | _] = string:split(S, "-" ),
	S2 = string:replace( S1, " ", "-"),
	Command1 = "pdftotext " ++ "/home/mmw/local/pass/test/" ++ "'" ++  S ++ "'",
	Command2 = " /home/mmw/local/pass/test/" ++ S2 ++ ".txt",
	os:cmd(Command1 ++ Command2).
	

pass() ->
	Dir = "/home/mmw/local/pass/test",

	%% read directory
	{ok, Files} = file:list_dir(Dir),

	%% process each file with files_do
    Files2 = lists:map( fun files_do/1, Files),

    %% Print
	lists:map(fun print/1, Files2).
	