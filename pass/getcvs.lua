-- read directory
dirname = 'wallet'
f = io.popen('ls ' .. dirname)

-- TODO: check where this should be opened
fileout = io.open(dirname .. ".csv", "w")
for name in f:lines() do

  -------------------
  -- convert pdf to txt
  -------------------

  mark = string.find(name, "-")
  name_part = string.sub(name,1,mark-1)

  -- remove space from output filename
  name_part2 = string.gsub(name_part, " ", "-")
  output_name = dirname .."/" .. name_part2 .. ".txt"

  command = "pdftotext " .. dirname .. "/'" .. name .. "'" .." " .. output_name
  -- print (command)
  res = io.popen(command)

  ----------------
  --process the converted file
  ----------------

  fileout:write(name_part2 .. "\n")

end
io.close(fileout)
