#!/usr/bin/env janet

(defn per-file-do
  [pdf-file]

  # remove clutter and replace space with -
  # (string/has-suffix? sfx str)
  (def [txt-file _ ] (string/split "-" pdf-file))
  (def txt-file (string/replace-all " " "_" txt-file))

  # create and execute pdftotext command
  (def from-file (string pdf-file ))
  (def to-file (string txt-file ".txt"))
  (os/execute @[ "pdftotext" from-file to-file ] :p)
  
  to-file
  )

(defn process-line
  [line arr]

  # delete trailing whitespace
  (def line (string/trim line ))
  
  (if (> (length line) 1)
	(do 
	  (prin (length line))
	  (print " " line)
	  # PEG experiment
	  (def line-split (peg/match ~(capture( sequence (thru ":") (position))) line))
	  (if (not= line-split nil) # check if an action has been found
		(do 
		  (def line-action (get line-split 1))
		  (print "line action:" line-action)
		  (def line-value (peg/match ~(sequence (thru ":")  (capture  (any (choice :w :W)))  ) line))
		  #TODO: is (choice :w :W) really the best way to get all characters?
		  (pp line-value)
		  (def line-data (get line-value 0))
		  (def line-data (string/trim line-data))
		  (case line-action # TODO: can a table be used for all the actions ?
			"URL:" (array/push arr [:url line-data])
			#"Contact Name:" (array/push arr [ :contact line-data])
			"User Name:" (array/push arr [ :user line-data])
			"Password:" (array/push arr [ :passwd line-data])
			(array/push arr [ :note line]) #default fallback
			) #case
		  ) #do
		(array/push arr [ :note line]) # else expression
		) #if
	  (pp arr)
	  
	  (print "-- end line --\n")
	  arr
	  ) # do
	) # if
  ) 

(defn process-txt-file
  [txt-file]
  ( print "\n----------")
  (def arr (array)) #empty array
  
  (print "New file: " txt-file)

  (with [file (file/open txt-file :r)]
		# first line is titel
		(var line (file/read file :line))
		(var line (string/trim line))
		(array/push arr [ :titel line])
		# process rest of file
      (while true
		(var line (file/read file :line))
		(if line
		  (def arr (process-line line arr))
		  (break))))
  (print "** arr **")
  (pp arr)
  arr
)


(defn build-csv-file
  [list]

  (def header @"folder,favorite,type,name,notes,fields,login_uri,login_username,login_password,login_totp\n")
  (def folder "OneSafe")
  (def type "login")
  (var name "")
  (var notes "\"")
  (var login_uri "")
  (var login_username "")
  (var login_password "")

  (def file-handle (file/open "tst.csv" :w))
  (file/write file-handle header)
  (each card list (do
					(print "--card--")
					(var name "")
					(var notes "")
					(var login_uri "")
					(var login_username "")
					(var login_password "")
					(var csv-line "")
					(each tup card (do
									 (def [action data] tup)
									 (case action
									   :titel (set name data)
									   :note (set notes (string notes " " data "\n"))
									   :url (set login_uri (string login_uri data))
									   :user (set login_username (string login_username data))
									   :passwd (set login_password (string login_password data))
									   (print "Problem: " action " : " data)
									   ) #case
									  
									 )) #do each
					(print "Notes: " notes)
					(print "User: " login_username)
					(print "Passwd: "login_password)
					(print "URI: " login_uri)
					(print "Titel: " name)

					# TODO: trip data ?

					(set csv-line (string folder ",," type "," name ",\"" notes "\",," login_uri "," login_username "," login_password ",,\n"))
					# write line"
					(file/write file-handle csv-line)
										  
					) #do
		) #each

		(file/close file-handle)
  )

(defn main [& args]

  # get dir from shell argument or set to local dirctory
  (var dir "")
  (if (= 1 (length args))
	(set dir "./")
	(set dir (in args 1)))
  (pp dir)

  (print "Los Gehts")
  # read files from dir into dir-files
  (var dir-files @[])
  (os/dir dir dir-files)

  # convert pdf to text files
  (def txt-files (map per-file-do dir-files))
  # concat dir and txt-file name
  (def txt-files (map (fn [x] (string dir x)) txt-files))
  (pp txt-files)

  # parse text files
  (def full-list (map process-txt-file txt-files))
  # (def full-list (process-txt-file "GMX.txt"))

  (print "\n ===Final List ========")
  (pp full-list)

  (print "\n ==== CSV start")
  (build-csv-file full-list)
  
  (print "Fettich")
)
