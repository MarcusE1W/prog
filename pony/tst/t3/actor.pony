actor Main
	new create (env: Env) =>
		let doggy = Dog( "Wuschl")
		doggy.eat(5)
		env.out.print (doggy.name + " hat Hungerlevel " + doggy.is_hungry().string())

actor Dog
  let name: String
  var _bauch_voll: U64 = 0
  let _energy_consumption: U64 = 2

  new create(name': String) =>
    name = name'

  be eat(amount: U64) =>
    _bauch_voll = _bauch_voll + amount

  be run() => 
  	if _bauch_voll < _energy_consumption 
  		then 0 
  		else _bauch_voll = _bauch_voll - _energy_consumption
		end
		
  fun is_hungry(): U64 => _bauch_voll
  	