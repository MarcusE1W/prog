actor BankAccount
    var _balance: I64
    let _env: Env

    new create( env': Env, balance: I64 ) =>
        _env = env'
        _balance = balance

    be deposit( amount: I64, sender: Person ) =>
        _balance = _balance + amount
        sender.depositResponse( "Successfull deposit: New balance is " + _balance.string() )

    be withdrawl( amount: I64, sender: Person ) =>
        let new_balance = _balance - amount

        if ( _balance -amount) < 0 then
            sender.withdrawlResponse( "Not enough funds, available is: " + _balance.string() )
        else
            _balance = _balance - amount
            sender.withdrawlResponse( "Fine, new balance is: " + _balance.string() )
        end

    be printBalance() =>
        _env.out.print( "Current balance is: " + _balance.string() )

actor Person
    let env: Env
    let name: String

    new create( env': Env, name': String ) =>
        env = env'
        name = name'

    be withdrawlResponse( message: String ) =>
        env.out.print( name + ":" + message )

    be depositResponse( message: String ) =>
        env.out.print( name + ":" + message )


actor Main
    new create( env: Env ) =>
        var account = BankAccount( env, 100 )
        account.printBalance()

        var alice = Person(env, "Alice")
        var bob = Person(env, "Bob")

        account.withdrawl( 101, alice )
        account.deposit( 1, bob )
        account.withdrawl( 101, alice )
        account.printBalance()
        account.deposit(5, alice)
        account.withdrawl(10, bob)
        account.printBalance()

        env.out.print( "The final word" )



