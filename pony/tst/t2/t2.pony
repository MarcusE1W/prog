primitive Red
primitive Green
primitive Blue

type Colour is (Red | Green | Blue)


actor Main
	new create (env: Env) =>
		let things: Array[(Colour | U64)] = [Red; U64(3); Red; Blue; Green; U64(100)]
		for thing  in things.values() do
			env.out.print(
				match thing
					| 100 => "one hundred"
					| let n: U64 => n.string()
					| Green => "Green"
					else "Puup"
				end
			)
		end
		
		