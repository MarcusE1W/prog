const std = @import("std");

const c1: i32 = -5; // interger 32 bit
var v2: u32 = 12;

pub fn main() void {
    std.debug.print("Hello, {s}!\n", .{"World"});
}
