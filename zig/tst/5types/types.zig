const std = @import( "std" );

pub fn main () void {

   // Variables
   const a1:i32 = 5;
   _ = a1;
   const   b1:u32 = 7;
   _ = b1;
   const c1:u8 = 'a';  // character
    _ = c1;
   // Arrays

   const a2 = [5]u8{ 'h', 'e', 'l', 'l', 'o' };
    _ = a2;
   const b2 = [_]u8{ 'h', 'e', 'l', 'l', 'o' };
    _ = b2;
	// const l2 = a2.len;



	// Multi dimensional Arrays

	// const a3 = [3][3]f32

}
