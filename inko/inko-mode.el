;; == references ==
;; http://xahlee.info/emacs/emacs/elisp_syntax_coloring.html
;; https://www.emacswiki.org/emacs/GenericMode
;; http://emacs-fu.blogspot.com/2010/04/creating-custom-modes-easy-way-with.html
;; https://www.emacswiki.org/emacs/DerivedMode


;; define default for tab-width
  (defvar inko-tab-width 4 "Width of a tab for InkoL mode")

;; == create the list for font-lock. ==
;; == each category of keyword is given a particular face ==
(setq inko-font-lock-keywords
      (let* (
            ;; define several category of keywords
            (x-structual-keywords '("fn" "if" "loop" "import" "class" "for" "trait" "while" "match" "recover" "else" "impl" "throw" "try?" "try!" "try" "recover" ))
            (x-types '("Array" "Bool" "ByteArray" "Channel" "Float" "Int" "Map" "Nil" "Option" "Result" "String" "Never" "true" "false" ))
            (x-events '("async" "ref" "mut" "extern" "pub" "self" "enum" "static" "builtin" "nil" "uni"))
            (x-functions '("case" "as" "let" "return" "move" "next" "and" "or" "break"))

            ;; generate regex string for each category of keywords
            (x-structual-keywords-regexp (regexp-opt x-structual-keywords 'words))
            (x-types-regexp (regexp-opt x-types 'words))
            (x-events-regexp (regexp-opt x-events 'words))
            (x-functions-regexp (regexp-opt x-functions 'words)))

        `(
          (,x-types-regexp . 'font-lock-type-face)
          (,x-events-regexp . 'font-lock-builtin-face)
          (,x-functions-regexp . 'font-lock-function-name-face)
          (,x-structual-keywords-regexp . 'font-lock-keyword-face)
          ;; note: order above matters, because once colored, that part won't change.
          ;; in general, put longer words first
          )))

;; == define inko-mode ==
;;;###autoload
(define-derived-mode inko-mode prog-mode "Inko mode"
  "Simple Major mode for editing the Inko language"

  ;; code for syntax highlighting
  (setq font-lock-defaults '((inko-font-lock-keywords)))

    ;; when there's an override for tabs, use it
    (when inko-tab-width
      (setq tab-width inko-tab-width))
  
    ;; for comments
    (setq comment-start "#")
    (setq comment-end "")
  
    (modify-syntax-entry ?# "< b" inko-mode-syntax-table)
    (modify-syntax-entry ?\n "> b" inko-mode-syntax-table)
  
  )

;; add the mode to the `features' list
(provide 'inko-mode)

