# Install

- download nightly arm-linux
- unpack
- link `roc` to ~/.local/bin/roc

# editor support

- install `roc-ts-mode`
- run `roc-ts-install-treesit-grammar`

