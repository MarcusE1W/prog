app [main!] { pf: platform "https://github.com/roc-lang/basic-cli/releases/download/0.19.0/Hj-J_zxz7V9YurCSTFcFdu6cQJie4guzsPMUi5kBYUk.tar.br" }

import pf.Stdout

birds = 3

iguanas = 2

total = add_and_stringify(birds, iguanas)

main! = |_args|
    Stdout.line!("Hi there, totally ${total} animals from inside a Roc app. 🎉")

add_and_stringify = |num1, num2|
    sum = num1 + num2

    if sum == 0 then
        ""
    else if sum < 0 then
        "negative"
    else
       Num.to_str(sum)
