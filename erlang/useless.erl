- module (useless).

- export ([ add/2, greet_and_add/2 ]).

% comment commemnt
add(A,B) -> 
    A + B.

hello() ->
    io:format("Greetings Erlangs~n").

greet_and_add(A,B) ->
    hello(),
    add(A,B).
