

# Examples

## SHT30 temperature sensor - example
  * https://github.com/atomvm/atomvm_examples/tree/master/erlang/i2c_example
  
  same?
  https://github.com/atomvm/AtomVM/blob/master/examples/erlang/esp32/sht31.erl
  
## Display - atomgl extension

driver ?
https://github.com/atomvm/atomgl

## PSM3001 air quality sensor

## M5 extension
https://github.com/pguyot/atomvm_m5
