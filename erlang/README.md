# Install

yay -S erlang
yay -S erlang_ls

# Emacs support

## treesitter

https://github.com/wingyplus/erlang-ts-mode
Currently the latest version is in a PR

 (erlang     "https://github.com/WhatsApp/tree-sitter-erlang" "main" "src")
