-module (test).
-export([greet/2, add/2, age/1]).

greet(m, Name) ->
    io:format("Hello dear ~s ~n", [Name]);
greet(f, Name) ->
    io:format("Hello dearest ~s ~n", [Name]);
greet(_, Name) ->
    io:format("Moin ~s", [Name]).

add(A, B) ->
    A + B.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Age
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

age(X) when X >0 andalso X < 100 ->
    true;

age(X) when X = "heinz" ->
    true;

age(_) ->
    false.
