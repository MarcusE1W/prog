
fn main() {
   const MAX_SIEVE:usize = 10;

   // type SieveType 

   // let array = [bool; MAX_SIEVE+1] = [true; MAX_SIEVE+1];  // array initialised with true
   let sieve = [true; MAX_SIEVE+1];  // array initialised with true

   println!( "Let's start " );

   println!( "Lenght of sieve {0}", sieve.len() );

   for	number in 2..=MAX_SIEVE  {
   	   println!( "sieve {0} with {1}", number, sieve[number]);
   }
   
   println!( "Finished !yay " );
}
