use std::io;

fn main() {
    println!("Hello dear traveler,play the guessing Game!");

    println!("Please guess a number and key it in:");

    let mut guess = String::new();

    io::stdin()
        .read_line(&mut guess)
        .expect("Error reading a number");

    println!("Tou guessed: {guess}" );  // und hier mal ein comment, nur so
}
