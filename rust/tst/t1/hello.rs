// lovely line comment
fn main() {
    println!("Hello, Dies und das.");
    println!("tada {number}", number = 2);
    println!("tada {number:>5}", number = 3); // the >5 is the spacing in front
    println!("tada {number:>1}", number = 12);
    println!("tada {0:>5}", 13);
    println!("tada {}", 14); // simple version
}

