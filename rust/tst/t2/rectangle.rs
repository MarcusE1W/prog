struct Point {
	   x: f32,
	   y: f32 }


struct Rectangle {
	   top_left: Point,
	   bottom_right: Point}

fn rect_area ( r: Rectangle) -> f32 {
   // let Rectangle{ top_left.x: x1, top_left.y, bottom_right.x.: x2, bottom_right.y: y2 };
   return (r.bottom_right.x - r.top_left.x) * (r.bottom_right.y - r.top_left.y)}

fn main() {

   let point1 = Point { x: 1.0, y: 2.0}; // type inferred

   let Point { x: x1, y: y1} = Point{x: 3.0, y: 4.0};

   let point2 = Point { x: x1, y:y1};

   let rect = Rectangle { top_left: point1, bottom_right: point2 };

   let rec_area = rect_area( rect );
   println!( "Area {0}", rec_area);
}
