package basics

import "core:fmt"
import "core:os"
import "core:strings"
import "core:time"
import "core:math/rand"

text_io :: proc () {

	get_line :: proc() -> string {
		buf: [256]byte
		n, err := os.read(os.stdin, buf[:])
		if err < 0 {
			// Handle error
			return "Error"
		}
		str :string= strings.clone_from_bytes(buf[:n])
		return str
	}

	fmt.print( "\nYour input please:")
	s1 : string = get_line();
	fmt.println( "Thank you for: ", s1)
	

	fmt.println("OS: ", ODIN_OS, "ARCH: ", ODIN_ARCH, "VERSION: ", ODIN_VERSION)
	fmt.print("this" )
	fmt.print(" and ", "that")
	fmt.print("\n")
	fmt.println("with new line")
	x,y : int = 5,6
	fmt.printf("tescht: %v %v \n", x, y ) // no new-line
}

random_numbers :: proc() {
	randomize :: proc()  {
		t := time.now()
		h,m,s :=time.clock_from_time(t)
		seed:=h+m+s		
		rand.set_global_seed(u64(seed))
	}
	
	getRandomValue :: proc(from,to : int) -> int {
		// random number from 'from' to 'to' inclusive
		return rand.int_max(to-from+1) + from
	}

	getRandomValue2 :: proc(max : int) -> int {

		// random number between 2 and 15
		/* return rand.int_max(13) + 2  */
		return rand.int_max(max)
	}

	randomize()

	from1::5
	to1::15
	fmt.println("--Random numbers between", from1, " and ", to1, " --")
	a:[to1-from1+1]int
	for i:=1; i<=100; i+=1 do a[getRandomValue(5, 15)-from1]+=1
	for i:=from1; i<=to1; i+=1 do fmt.print( i, ":", a[i-from1], " ")

	from2::-50
	to2::50
	fmt.println("\n--Random numbers between", from2, " and ", to2, " --")
	b:[to2-from2+1]int
	for i:=1; i<=100000; i+=1 do b[getRandomValue(from2, to2)-from2]+=1
	for i:=from2; i<=to2; i+=1 do fmt.print( i, ":", b[i-from2], "||")
	fmt.println( "\n--")
}


main :: proc() {

	text_io()
	random_numbers()
}
