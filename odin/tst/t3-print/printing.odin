package printing

import "core:fmt"

/* just
a
comment */


main :: proc() {
	 x : int : 5 // constant
	 y : f64 = f64(x) + 1.5 // type conversion

	fmt.println("OS: ", ODIN_OS, "ARCH: ", ODIN_ARCH, "VERSION: ", ODIN_VERSION)
	fmt.print("this" )
	fmt.print(" and ", "that")
	fmt.print("\n")
	fmt.printf("tescht: %v %v \n", x, y )

	fmt.println( type_info_of(type_of(x)) )
	
}
