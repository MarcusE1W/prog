package sudoku1

import "core:fmt"

/* just
a
comment */

SquareT :: matrix[3,3]int

testSetT:: bit_set[1..=9]

LineT :: [9]int

SudokuT :: [9][9]int

checkSquare :: proc ( m : SquareT ) -> (x,y : int) {
	check: testSetT
	for i in 0..=2 {
		for j in 0..=2 {
			if m[i,j] in check { // check if bit_set is already set
				x=i
				y=j
			return x, y
			}
			else {
				check += {m[i,j]}  // add to bit_set
			}
		}
	}
	return 10, 11 // all is well
} // checkSquare

checkLine :: proc( line : LineT ) -> (x: int) {
	check: testSetT
	for i in 0..=8 {
			if line[i] in check { // check if bit_set is already set
				x=i
			return x
			}
			else {
				check += {line[i]}  // add to bit_set
			}
	}
	return 10
}

getColumn :: proc( s: SudokuT, column: int ) -> (line: LineT) {
	for i in 0..=8 do line[i] = s[i][column]
	return line
}


main :: proc() {	 
	fmt.println("Sudoku tester numero 1")
	// fmt.println( type_info_of(type_of(x)) )

	fmt.println("\n Squares")
	m1:= SquareT {
		1,2,3,
		4,6,6,
		7,8,9, }

	fmt.println("m1:", m1)

	x1,y1 := checkSquare( m1)
	fmt.printf(" %v %v \n", x1, y1 )
	if x1 == 10 && y1 == 11 {
		fmt.println( "🙂 This looks all right " )
	} else {
		fmt.printf( "🙁 There appears to be some unsharpness in line %v column  %v \n", x1+1, y1+1)
	}

    // -------------------------------------------------------

	fmt.println("\n Lines !!!")
	m2 := SudokuT {
		{0, 9, 1, 0, 0, 2, 0, 0, 3,},
		{0, 0, 0, 0, 7, 0, 6, 0, 0,},
		{0, 8, 0, 4, 0, 0, 0, 1, 0,},
		{0, 0, 5, 0, 0, 3, 8, 7, 0,},
		{8, 2, 5, 0, 0, 0, 0, 0, 0,},
		{0, 0, 6, 0, 0, 5, 1, 3, 0,},
		{0, 7, 0, 3, 0, 0, 0, 2, 0,},
		{0, 0, 0, 0, 4, 0, 9, 0, 0,},
		{0, 6, 2, 0, 0, 8, 0, 0, 7, },}

	l2 := getColumn(m2, 2)
	fmt.println("line[2]:", l2)

	x2 := checkLine( l2)
	if x2 == 10 {
		fmt.println( "🙂 This looks all right " )
	} else {
		fmt.printf( "🙁 There appears to be some unsharpness in position %v \n", x2+1)
	}

}
