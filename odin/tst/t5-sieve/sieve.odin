package sieve

import p "core:fmt"

main :: proc() {

	maxSieve: int : 1000000 //const

	SieveRange :: int
	sieve : [maxSieve+1]bool = true // initialised with true

	for i in 2..<maxSieve {
		if sieve[i] == true {
			for j:=i; j<maxSieve; j+=i {
				sieve[j] = false
			}
			p.println( i )
		}
	}
	
}
