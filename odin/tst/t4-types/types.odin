package odintypes

import "core:fmt"
import "core:strconv"
import "core:unicode/utf8"

/* just /* a */
comment */

number_example :: proc() {
	fmt.println( "-- Integer --")

	x : int : 5 // constant
	
	x1 : int : 16 / 5 // -> 3
	x2 : int : 16 % 5 // -> 1
	fmt.println("Integer division and modulo (16/5):", x1, x2)

	h,k : int = 5,6  // -> h=5 k=6
	
	// float
	y : f64 = f64(x) + 1.5 // type conversion required -> 6.5
}


rune_example :: proc() {
	fmt.println( "-- Rune --")

	r1 : rune = 'R'
	r2 : rune = '☕'
	
	rune_array : []rune={ 'R', 'u', 'n', 'e' }
	s1 := utf8.runes_to_string( rune_array )
	fmt.println( "Runestring: ", s1)
}


string_example :: proc() {
		fmt.println( "-- String --")

	int_to_string::proc(i:int) -> string{  // this does not work
		k:=i
		s:string
		for k > 0 {
			r := k%10
			r1 := u8(r)
			r2 := rune(r1)
			// s = ??
			fmt.print( r2, " ")
			k = k / 10 // int devision
		}
		fmt.println( "" )
		return "Haus"
	}
	
	s : string = "abc"
	s2:        = "def"

	// string to int
	num, ok := strconv.parse_int("1234") // without prefix, inferred base 10
	if !ok do fmt.println( "Bummer, something went wrong, it's the Strings")
	fmt.println( "Converted string to int: ", num)

	fmt.println(strconv.atoi("42"))

	// homemade solution
	// s5 : string = int_to_string(2345)
	
	// int to string
	buf: [4]byte
	s3 :string= strconv.itoa(buf[:], 42)
	fmt.println(buf) // "[52, 50, 0, 0]"
	fmt.println(s3) // "42"
	s4 :string= strconv.itoa(buf[:], 4711)
	fmt.println( s3, ":", s4) // 47 : 4711  ?? why, a slice ? no idea : length set in first call ?
	
}


fixed_array_example :: proc() {
	fmt.println( "-- Fixed Array --")

	max : int : 5
	array : [max]bool = true //initialise array_slice_example

	a2 : [max]int // just an empty array
	a3 := [max]int{1,2,3,4,5} // array with content

	// initialise array
	i := 2
	foo := [?]int {
		0 = 123,
		5..=9 = 54,
		10..<16 = i*3 + (i-1)*2,
	}

	Point :: struct {
		x: int,
		y: int,
	}
	points_array : [max]Point

	TwoDimensionalArrayT :: [9][9]int

	
}

array_slice_example :: proc() {
	fmt.println( "-- Array slices --")
}

dynamic_array__example :: proc() {
	fmt.println( "-- Dynamic arrays --")
}

matrix_example :: proc() {
	
}



enum_example :: proc() {
	fmt.println( "-- Enums / Unions --")
}

union_example :: proc() {
	fmt.println( "-- Enums / Unions --")
}

struct_example :: proc() {
	fmt.println( "-- Structs --")
}

map_example :: proc() {
	fmt.println( "-- Map --")

	mapT :: map[int]string // type of map
	// fmt.println( type_id( m))
	m1 := make(mapT)
	m2 := make(map[int]string)	
}

vector_example :: proc() {
	fmt.println( "-- Vector --")
	// #SIMD
}

matrix_example :: proc() {
	fmt.println( "-- Matrix --")

}

complex_example :: proc() {
	fmt.println( "-- Complex Numbers --")

	c1 : complex64 : 2 + 3i // irrelevant
	c2 :: complex(1.0, 2.0) // irrelevant

}


bit_example :: proc() {
	fmt.println( "-- Bit fiddel --")

}


soa_example :: proc() {
	fmt.println( "-- SOA --")
//#SIMD
}



main :: proc() {

	t:int=5
	p.println("Type of t :", type_info_of(type_of(t)))
	
	number_example()
	rune_example()
	string_example()
	fixed_array_example()
	array_slice_example()
	dynamic_array__example()
	enum_example()
	union_example()
	struct_example()
	map_example()
	vector_example()
	matrix_example()
	complex_example()
	bit_example()	
	soa_example()
}
