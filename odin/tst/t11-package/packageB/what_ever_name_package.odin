package doesanyonereallycare
import "core:fmt"

// if you only have one file to declare a package,
// then the package name decalration and
// the filename of the package do not matter much
// the folder name is used as the package name to import

complaint :: "What am I supposed to say? I am the package name declaration, nobody cares"

printHello_whatever :: proc() {
	fmt.println("This is a message fromt the package file what_ever_name.odin in package packageB")
	fmt.println("I feel that  don't matter")
}
