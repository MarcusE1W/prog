package packageA

// - in one folder you can have several files but the package declaration has to be for the same package
// - if there is more than one file in a directory for one package the package declaration name has to match the folder name
// - the file-names can of course be different

import "core:fmt"

@(private)
hello2 :: "Hello too from file package_aa.odin in packageA"

printHello2 :: proc() {
	fmt.println(hello2)
}
