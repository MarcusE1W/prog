package packageA
// - the foldername isthe relevant name for package import
// - the package name can be different from the folder name
// - the package file name can be different from the folder name and the package name
import "core:fmt"

printHello_too :: proc() {
	fmt.println("Hello from file package_aa.odinb in packageA")
}
