package packageMain

import "core:fmt"
import "packageA"
import "packageB"
import "packageB/packageC"

main :: proc() {
	// packageA
	packageA.printHello()
	packageA.printHello2()
	// packageB
	packageB.printHello_whatever()
	fmt.println(packageB.complaint)
	// packageC
	packageC.printBaby()
}

