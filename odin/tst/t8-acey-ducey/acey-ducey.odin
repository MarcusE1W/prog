package aceyducey

import p "core:fmt"
import "core:strconv"
import "core:math/rand"
import "core:time"
import "core:os"
import "core:strings"

print_greetings :: proc() {
	p.println( "\nACEY DUCEY CARD GAME")
	p.println( "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY")
	p.println( " ")
	p.println( " ")
	p.println( "ACEY-DUCEY IS PLAYED IN THE FOLLOWING MANNER ")
	p.println( "THE DEALER (COMPUTER) DEALS TWO CARDS FACE UP")
	p.println( "YOU HAVE AN OPTION TO BET OR NOT BET DEPENDING")
	p.println( "ON WHETHER OR NOT YOU FEEL THE CARD WILL HAVE")
	p.println( "A VALUE BETWEEN THE FIRST TWO.")
	p.println( "IF YOU DO NOT WANT TO BET, INPUT A 0")
}

CardsT :: [15]string // array of strings

create_cards :: proc() -> CardsT {

	cards := CardsT {"-", "-", "2","3","4","5","6","7","8","9","10", "JACK", "QUEEN", "KING", "ASS"}

	// buf: [4]u8
	/* for i in 2..=10 { */
	/* 	cards[i] = strconv.itoa(buf[:], i) */
	/* 	p.println(  i, " : ", cards[i]) */
	/* } */

	return cards	
}	

randomize :: proc()  {
	h,m,s :=time.clock_from_time(time.now())
	seed:=h+m+s
	rand.set_global_seed(u64(seed))
}

get_random_card :: proc() -> int {
	return rand.int_max(13) + 2
}

print_balance :: proc ( stash : int) {
	p.println( "\nYOU HAVE ", stash, "DOLLARS")
}

print_card :: proc (card:int, cards : CardsT){

	p.print( cards[card])
}


draw_dealer_cards :: proc( cards:CardsT) -> (int,int) {

	swap_cards :: proc (a, b : int) -> (int, int) {
		return b, a
	}
	
	cardA : int = get_random_card()
	cardB : int = get_random_card()
	if cardA > cardB do cardA, cardB = swap_cards( cardA, cardB)

	p.print( "HERE ARE YOUR NEXT TWO CARDS: " )
	p.println( cards[cardA], " ", cards[cardB])
	
	return cardA, cardB
}


draw_player_card :: proc( cards:CardsT) -> int {
	cardC : int = get_random_card()
	p.println( cards[cardC])
	return cardC
}


get_line :: proc() -> string {
	buf: [256]byte
	n, err := os.read(os.stdin, buf[:])
	if err < 0 {
		// Handle error
		return "Error"
	}
	str :string= strings.clone_from_bytes(buf[:n-1]) // -1 to cut off the "\n"
	return str
}

read_bet :: proc () -> int {
	
	input := get_line()
	bet, ok := strconv.parse_int(input)
	if !ok do bet = -1
	return bet
}

get_bet :: proc( stash: int) -> int {
	bet: int = -1
	p.println( "" )
	for bet < 0 || bet > stash {
		p.print( "WHAT IS YOUR BET :" )
		bet = read_bet()
		if bet > stash {
			p.println( "SORRY, MY FRIEND, BUT YOU BET TOO MUCH." )
			p.println( "YOU HAVE ONLY ", stash, " DOLLARS TO BET." )
		}
	}
	return bet
}

play :: proc( stash:int, cards:CardsT) -> int {
	money := stash
	print_balance(money)
	cardA, cardB := draw_dealer_cards(cards)
	bet := get_bet(stash)
	if bet > 0 {
		cardC := draw_player_card(cards)
		if cardA < cardC && cardC < cardB {
			money += bet
			p.println( "YOU WIN !!" )
		} else {
			money -= bet
			p.println( "SORRY, YOU LOOSE" )
		}
	} else {
		p.println ( "CHICKEN" )
	}
	
	return money
}

play_again :: proc() -> bool {
	again : bool = false
	// TODO:
	return again
}

main :: proc() {

	randomize()
	cards := create_cards()
	print_greetings()

	tryagain : bool = true

	for tryagain {
		stash : int = 100
		for stash > 0 {
			stash = play( stash, cards)
		}
		p.println( "SORRY, FRIEND, BUT YOU BLEW YOUR WAD.")
		tryagain = play_again()
	}
	p.println( "O.K., HOPE YOU HAD FUN!")
}
