package odinparams

import "core:fmt"

main :: proc() {
	fmt.println("OS: ", ODIN_OS)
	fmt.println("ARCH: ", ODIN_ARCH)
	fmt.println("ENDIAN: ", ODIN_ENDIAN)
	fmt.println("VENDOR: ", ODIN_VENDOR)
	fmt.println("VERSION: ", ODIN_VERSION)
	fmt.println("DEBUG: ", ODIN_DEBUG)
}
