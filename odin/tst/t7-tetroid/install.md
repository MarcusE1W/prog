# Odin with raylib on Pinebook Pro

## starting position

If you install and compile Odin from git then it provides compiled libraries in the /vendor folder for those libraries. However only for officially supported targets like AMD64 (Linux/Windows/MacOS) and ARM64 (MacOS) binary libraries are supported. Understandably.
If you install odin for the aarch64 architecture on Linux (not MacOS) then Odin 


```
/bin/ld: skipping incompatible ///home/.../.local/odin/vendor/raylib/linux/libraylib.a when searching for -l:/home/.../.local/odin/vendor/raylib/linux/libraylib.a
/bin/ld: skipping incompatible ///home/.../.local/odin/vendor/raylib/linux/libraylib.a when searching for -l:/home/.../.local/odin/vendor/raylib/linux/libraylib.a
/bin/ld: cannot find -l:/home/mmw/.local/odin/vendor/raylib/linux/libraylib.a: No such file or directory
/bin/ld: skipping incompatible ///home/.../.local/odin/vendor/raylib/linux/libraylib.a when searching for -l:/home/.../.local/odin/vendor/raylib/linux/libraylib.a
/bin/ld: skipping incompatible ///home/.../.local/odin/vendor/raylib/linux/libraylib.a when searching for -l:/home/.../.local/odin/vendor/raylib/linux/libraylib.a
clang-14: error: linker command failed with exit code 1 (use -v to see invocation)
```

## Install Raylib from git

```
git clone ...
cd raylib/src
make PLATFORM=PLATFORM_DESKTOP
```
This downloads a lot but then compiles rahter quickly.
The result is `libraylib.a`in th same folder.

## provide Raylib for Odin

```
mv linux linux-AMD64
mkdir linux
cp ~/.../raylib/src/libraylib.a linux
```

