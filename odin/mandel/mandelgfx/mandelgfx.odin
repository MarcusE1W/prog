// odin build -build-mode=exe -out=mandelbrot mandelbrot.odin -ldflags="-lraylib -lwayland-client -lwayland-cursor -lwayland-egl -lEGL -lGL -lm -lpthread -ldl -lrt -lX11 -lxkbcommon"


package main

import "vendor:raylib"

screenWidth  :: 800 //constants
screenHeight :: 600

maxIterations :: 1000

Complex :: struct {
    real: f64,
    imag: f64,
}

mandelbrot :: proc(c: Complex) -> int {
    z := Complex{}
    iterations := 0

    for iterations < maxIterations && (z.real*z.real+z.imag*z.imag) < 4.0 {
        z = Complex{z.real*z.real - z.imag*z.imag + c.real, 2*z.real*z.imag + c.imag}
        iterations += 1
    }

    return iterations
}

mandelpoint :: proc(c : complex64, max_iter : int) -> int {
	z : complex64
	
	for i in 0 ..= max_iter {
		z = z * z + c
		if abs(z) > 4.0 do return i
	}
	return max_iter
}

mapRange::proc(value, inMin, inMax, outMin, outMax: f64) -> f64 {
    return (value-inMin)*(outMax-outMin)/(inMax-inMin) + outMin
}

main::proc() {
	
    raylib.InitWindow(screenWidth, screenHeight, "Mandelbrot Set")
    defer raylib.CloseWindow()

    for !raylib.WindowShouldClose() {
        raylib.BeginDrawing()

        for y := 0; y < screenHeight; y+=1 {
            for x := 0; x < screenWidth; x+=1 {
                cx := mapRange(f64(x), 0, screenWidth, -2, 1)
                cy := mapRange(f64(y), 0, screenHeight, -1, 1)

                /* c := complex64(cx, cy) */
                //iterations := mandelbrot(c)
                /* iterations := mandelpoint(c, maxIterations) */
                iterations := mandelpoint(complex(cx, cy), maxIterations)
				

                /* color := raylib.NewColor(byte(mapRange(f64(iterations), 0, maxIterations, 0, 255)), */
                /*     byte(mapRange(f64(iterations), 0, maxIterations, 0, 255)), */
                /*     byte(mapRange(f64(iterations), 0, maxIterations, 0, 255)), 255) */
				color := raylib.RED

				if iterations < maxIterations do color = raylib.BLACK

                raylib.DrawPixel(i32(x), i32(y), color)
            }
        }

        raylib.EndDrawing()
    }
}
