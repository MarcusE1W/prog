package main

import "raylib"

const (
    screenWidth  = 800
    screenHeight = 600
    maxIterations = 1000
)

type Complex struct {
    real, imag float64
}

func mandelbrot(c Complex) int {
    z := Complex{}
    iterations := 0

    for iterations < maxIterations && (z.real*z.real+z.imag*z.imag) < 4.0 {
        z = Complex{z.real*z.real - z.imag*z.imag + c.real, 2*z.real*z.imag + c.imag}
        iterations++
    }

    return iterations
}

func mapRange(value, inMin, inMax, outMin, outMax float64) float64 {
    return (value-inMin)*(outMax-outMin)/(inMax-inMin) + outMin
}

func main() {
    raylib.InitWindow(screenWidth, screenHeight, "Rotating Mandelbrot Set")
    defer raylib.CloseWindow()

    rotationAngle := 0.0

    for !raylib.WindowShouldClose() {
        raylib.BeginDrawing()

        raylib.ClearBackground(raylib.Black)

        centerX := float64(screenWidth) / 2
        centerY := float64(screenHeight) / 2

        for y := 0; y < screenHeight; y++ {
            for x := 0; x < screenWidth; x++ {
                rotatedX := float64(x) - centerX
                rotatedY := float64(y) - centerY

                // Apply rotation
                newX := rotatedX*raylib.Cos(rotationAngle) - rotatedY*raylib.Sin(rotationAngle)
                newY := rotatedX*raylib.Sin(rotationAngle) + rotatedY*raylib.Cos(rotationAngle)

                cx := mapRange(newX, -centerX, centerX, -2, 1)
                cy := mapRange(newY, -centerY, centerY, -1, 1)

                c := Complex{cx, cy}
                iterations := mandelbrot(c)

                color := raylib.NewColor(byte(mapRange(float64(iterations), 0, maxIterations, 0, 255)),
                    byte(mapRange(float64(iterations), 0, maxIterations, 0, 255)),
                    byte(mapRange(float64(iterations), 0, maxIterations, 0, 255)), 255)

                raylib.DrawPixel(x, y, color)
            }
        }

        raylib.EndDrawing()

        rotationAngle += 0.01
    }
}
