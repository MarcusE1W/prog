package mandel

import p "core:fmt" //prefix p instead of fmt


mandelpoint :: proc(c : complex64, max_iter : int) -> int {
	z : complex64
	
	for i in 0 ..= max_iter {
		z = z * z + c
		if abs(z) > 4.0 do return i
	}
	return max_iter
}


main :: proc() {
	
	charmap := [10]string{" ", ".", ":", "-", "+", "*", "=", "#", "%", "@" }

	max_iter : int : 100

	xres : int : 60
	yres :: 30

	xmin : f64 : -2.0
	ymin : f64 : -1.7

	xmax : f64 : 1.5
	ymax : f64 : 1.7

	xstep : f64 : (xmax - xmin) / f64(xres)
	ystep : f64 : (ymax - ymin) / f64(yres)

	cx : f64 = 0.0
	cy : f64 = 0.0

	color : int = 0
	
	cy = ymin
	for y in 0 ..< yres {
		cx = xmin
		for x in 0 ..< xres {
			color = mandelpoint( complex(cx, cy), max_iter)
			
			if color == max_iter do p.print( " " ) 
			else do p.print( charmap[color%10])
			
			cx += xstep
		}
		cy += ystep
		p.println("")
	}
}


