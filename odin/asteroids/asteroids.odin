package main














import "core:fmt"
import "core:time"
import "core:math/rand"
import rl "vendor:raylib"
import "core:math"
import c "core:c/libc"




// ----
// defines
// ----
Float:: f32
PLAYER_BASE_SIZE :: 20.0
PLAYER_SPEED :: 6.0
PLAYER_MAX_SHOOTS::   10
METEORS_SPEED::       2
MAX_BIG_METEORS::     4
MAX_MEDIUM_METEORS::  8
MAX_SMALL_METEORS::   16


//---------------------------------------------------------------------
// Types and Structures Definition
//-------------------------------------------------------------------------
Player :: struct {
    position: rl.Vector2,
    speed: rl.Vector2,
    acceleration: Float,
    rotation: Float,
    collider: rl.Vector3,
    color: rl.Color,
}

Shoot:: struct  {
    position: rl.Vector2,
    speed: rl.Vector2,
    radius: Float,
    rotation: Float,
    lifeSpawn: int,
    active: bool,
    color:rl.Color,
}

Meteor:: struct {
    position: rl.Vector2,
    speed: rl.Vector2,
    radius: Float,
    active: bool,
    color:rl.Color,
}

//----------------------------------------------------------------------
// Global Variables Declaration
//----------------------------------------------------------------------
screen_width ::  800
screen_height ::  450

gameOver: bool : false
pause: bool : false
victory: bool : false

// NOTE: Defined triangle is isosceles with common angles of 70 degrees.
ship_height: Float : 0.0

player := Player{}
shoot :[PLAYER_MAX_SHOOTS]Shoot
bigMeteor : [MAX_BIG_METEORS]Meteor
mediumMeteor : [MAX_MEDIUM_METEORS]Meteor
smallMeteor : [MAX_SMALL_METEORS]Meteor

midMeteorsCount :int= 0
smallMeteorsCount :int= 0
destroyedMeteorsCount: int = 0

randomize :: proc()  {
	t := time.now()
	h,m,s :=time.clock_from_time(t)
	seed:=h+m+s		
	rand.set_global_seed(u64(seed))
}
getRandomValue :: proc(from,to : int) -> int {
		return rand.int_max(to-from+1) + from
	}

main :: proc() {

	randomize()

	rl.InitWindow(  c.int(screen_width), c.int(screen_height), "classic game: asteroids")

	initGame()
	

	

	rl.SetTargetFPS(60)



	for !rl.WindowShouldClose() {   // Detect window close button or ESC



		updateDrawFrame()

	}

    // De-Initialization

	unloadGame()  // Unload loaded data (textures, sounds,

	rl.CloseWindow()
	fmt.println("Type of t :", type_info_of(type_of(rl.DEG2RAD)) )
}


//------------------------------------------------------
// Module Functions Definitions (local)
//------------------------------------------------------

// Initialize game variables
initGame :: proc() {

	posx,posy : int = 0,0
	velx,vely : int = 0,0
	correct_range : bool = false
	victory : bool = false
	pause : bool = false

	ship_height :f32= (PLAYER_BASE_SIZE/2.0) / math.tan_f32(20.0*rl.DEG2RAD)

    // Initialization player
    player.position = rl.Vector2{ f32(screen_width)/2.0, f32(screen_height)/2.0 - f32(ship_height) /2.0}
    player.speed = rl.Vector2{0, 0};
    player.acceleration = 0
    player.rotation = 0
    player.collider = rl.Vector3{player.position.x + math.sin(player.rotation*rl.DEG2RAD)*(ship_height/2.5),
								 player.position.y - math.cos(player.rotation*rl.DEG2RAD)*(ship_height/2.5),
								 12};
    player.color = rl.LIGHTGRAY

    destroyedMeteorsCount = 0

    // Initialization shoot
    for i := 0; i < PLAYER_MAX_SHOOTS; i+=1
    {
        shoot[i].position = (rl.Vector2){0, 0};
        shoot[i].speed = (rl.Vector2){0, 0};
        shoot[i].radius = 2.0;
        shoot[i].active = false;
        shoot[i].lifeSpawn = 0;
        shoot[i].color = rl.WHITE;
    }


	
	for  i:= 0; i < MAX_BIG_METEORS; i+=1 {
        posx = getRandomValue(0, screen_width)

        for !correct_range {
            if posx >  (screen_width/2 - 150) && posx < screen_width/2 + 150 do posx = getRandomValue(0, screen_width)
            else do correct_range = true
        }

        correct_range = false;

        posy = getRandomValue(0, screen_height);

        for !correct_range
        {
            if posy > screen_height/2 - 150 && posy < screen_height/2 + 150 do posy = getRandomValue(0, screen_height);
            else do correct_range = true;
        }
		
        bigMeteor[i].position = rl.Vector2{f32(posx), f32(posy)};

        correct_range = false;
        velx = getRandomValue(-METEORS_SPEED, METEORS_SPEED);
        vely = getRandomValue(-METEORS_SPEED, METEORS_SPEED);

        for !correct_range
        {
            if velx == 0 && vely == 0
            {
                velx = getRandomValue(-METEORS_SPEED, METEORS_SPEED);
                vely = getRandomValue(-METEORS_SPEED, METEORS_SPEED);
            }
            else do correct_range = true;
        }

        bigMeteor[i].speed = rl.Vector2{f32(velx), f32(vely)};
        bigMeteor[i].radius = 40;
        bigMeteor[i].active = true;
        bigMeteor[i].color = rl.BLUE;
    }

    for i := 0; i < MAX_MEDIUM_METEORS; i+=1 {
        mediumMeteor[i].position = rl.Vector2{-100.0, -100.0};
        mediumMeteor[i].speed = rl.Vector2{0.0, 0.0};
        mediumMeteor[i].radius = 20;
        mediumMeteor[i].active = false;
        mediumMeteor[i].color = rl.BLUE;
    }

    for i := 0; i < MAX_SMALL_METEORS; i+=1 {
        smallMeteor[i].position = rl.Vector2{-100.0, -100.00};
        smallMeteor[i].speed = rl.Vector2{0.0,0.0};
        smallMeteor[i].radius = 10;
        smallMeteor[i].active = false;
        smallMeteor[i].color = rl.BLUE;
    }

    midMeteorsCount = 0;
    smallMeteorsCount = 0;

}

updateGame :: proc() {
}

drawGame :: proc() {
}

unloadGame :: proc() {
}

updateDrawFrame :: proc () {
	updateGame()
	drawGame()
}

