# Odin notes

## Install

## Compiling from source
At the time of writing (Aug 2023) Odin requires llvw v14 and the latest version installed in Archlinux is llvm v15.
However, a package for llvm14 is available.

So the first task is to install these package.
```
pacman -S llvm14
pacmam -S clang14
```

After that set the environment variable CC and CPP accordingly
```
export CC=/usr/lib/llvm14/bin/clang-14
export CPP=/usr/lib/.../XXX
```

In my case you still had to make sure that clang is on the path. You can link it to a folder that is already on the path for example:
```
ln -s /usr/lib/clang/.../clang ~/.local/bin
```
if `~/.local/bin` is already on your path.

After this preperation you can follow the normal instructions to compile Odin.

Odin comes with many common graphics libraries ready to use. This requires that the respective libraries are available in binary form. Odin provides this for X86 systems. On an aarch64 system the correct libraries have to be installed.

Install ? complile ?
TODO: what did I actually do.

In the end link libray to Odin folder.
Is that actually required or can the Odin folder just be deleted ?

## Editor support

### Emacs

#### Emacs mode
https://github.com/mattt-b/odin-mode

(load-file “File location here”)

#### OLS - LSP
needs odin nightly

#### treesitter
several grammars but no direct Emacs support

### Niggles

- no range types
- array index always from 0

- Error handling flexible but not enforced

- seems to be much slower than zig in benchmarks

- function calls should always have brackets()

- `odin check . --vet` doe not show unsued procedures ?

### Docu

- TODO: link odin/examples to Wiki or so.
