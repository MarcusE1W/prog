#ifndef _DRAWING_
#define _DRAWING_

// Generated by Oberon+ IDE (Mono) 0.9.96 on 2023-10-14T21:43:42

#include "OBX.Runtime.h"
#include "Fibonacci.h"
#include "Collections.9c3c6a04b5.h"

// Declaration of module Drawing

struct Drawing$1$Class$;
struct Drawing$1;
struct Drawing$drawAll$I$Class$;
struct Drawing$drawAll$I;
struct Drawing$Figure$Class$;
struct Drawing$Figure;
struct Drawing$Circle$Class$;
struct Drawing$Circle;
struct Drawing$Square$Class$;
struct Drawing$Square;
struct Drawing$1 {
    struct Drawing$1$Class$* class$;
    int32_t x;
    int32_t y;
};

extern void Drawing$1$init$(struct Drawing$1*);
struct Drawing$drawAll$I {
    struct Drawing$drawAll$I$Class$* class$;
    int32_t count;
};

extern void Drawing$drawAll$I$init$(struct Drawing$drawAll$I*);
struct Drawing$Figure {
    struct Drawing$Figure$Class$* class$;
    struct Drawing$1 position;
};

extern void Drawing$Figure$init$(struct Drawing$Figure*);
struct Drawing$Circle {
    struct Drawing$Circle$Class$* class$;
    struct Drawing$1 position;
    int32_t diameter;
};

extern void Drawing$Circle$init$(struct Drawing$Circle*);
struct Drawing$Square {
    struct Drawing$Square$Class$* class$;
    struct Drawing$1 position;
    int32_t width;
};

extern void Drawing$Square$init$(struct Drawing$Square*);
extern struct Collections$9c3c6a04b5$Deque * Drawing$figures;
extern struct Drawing$Circle * Drawing$circle;
extern struct Drawing$Square * Drawing$square;
void Drawing$Figure$draw(void* this);
void Drawing$Circle$draw(void* this);
void Drawing$Square$draw(void* this);
void Drawing$drawAll();
void Drawing$drawAll$I$apply(void* this, struct Drawing$Figure * * figure);
struct Drawing$1$Class$ {
    struct Drawing$1$Class$* super$;
};
extern struct Drawing$1$Class$ Drawing$1$class$;

struct Drawing$drawAll$I$Class$ {
    struct Collections$9c3c6a04b5$Iterator$Class$* super$;
    void (*apply)(void* this, struct Drawing$Figure * * figure);
};
extern struct Drawing$drawAll$I$Class$ Drawing$drawAll$I$class$;

struct Drawing$Figure$Class$ {
    struct Drawing$Figure$Class$* super$;
    void (*draw)(void* this);
};
extern struct Drawing$Figure$Class$ Drawing$Figure$class$;

struct Drawing$Circle$Class$ {
    struct Drawing$Figure$Class$* super$;
    void (*draw)(void* this);
};
extern struct Drawing$Circle$Class$ Drawing$Circle$class$;

struct Drawing$Square$Class$ {
    struct Drawing$Figure$Class$* super$;
    void (*draw)(void* this);
};
extern struct Drawing$Square$Class$ Drawing$Square$class$;

extern void Drawing$init$(void);
extern OBX$Cmd Drawing$cmd$(const char* name);
#endif
