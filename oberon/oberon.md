# Oberon

# OBNC

# Editor
- Micro
Add [this]() file in ~/.config/micro/syntax
- Lite
## Emacs / Spacemacs
Add `oberon` in section `dotspacemacs-additional-packages` directly below the layers in Spacemacs in your `.spacemacs` file.
Add
```
  ;; config oberon melpa module
  (add-to-list 'auto-mode-alist '("\\.obn\\'" . oberon-mode))
  (autoload 'oberon-mode "oberon" nil t)
  (add-hook 'oberon-mode-hook (lambda () (abbrev-mode t))) ;; optional; change keywords to CAPITAL letter
```

# Useful links
https://rsdoiel.github.io/blog/2020/04/11/Mostly-Oberon.html
