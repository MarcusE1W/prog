/* voc 2.1.0 [2020/11/29] for clang LP64 on ubuntu xtpamM */

#define SHORTINT INT8
#define INTEGER  INT16
#define LONGINT  INT32
#define SET      UINT32

#include "SYSTEM.h"
#include "Out.h"







export int main(int argc, char **argv)
{
	__INIT(argc, argv);
	__MODULE_IMPORT(Out);
	__REGMAIN("hello", 0);
/* BEGIN */
	Out_String((CHAR*)"Hello.", 7);
	Out_Ln();
	__FINI;
}
