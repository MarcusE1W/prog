   PROGRAM
     ObnToMd
   FUNCTION
     This is a simple program that reads Oberon modules
     from standard in and re-renders that to standard output
     such that it is suitable to process with Pandoc or other
     text processing system.
   EXAMPLE
     Read the source for this program and render a file
     called "blog-post.md". Use Pandoc to render HTML.
       ObnToMd <ObnToMd.Mod > blog-post.md
       pandoc -s --metadata title="Blog Post" \
           blog-post.md >blog-post.html
   BUGS
     It uses a naive line analysis to identify the module
     name and then the end of module statement. Might be
     tripped up by comments containing the same strings.
     The temporary file created is called "o2m.tmp" and
     this filename could potentially conflict with another
     file.
   
