#[
/*******************************************************************************************
*
*   raylib - classic game: asteroids
*
*   Sample game developed by Ian Eito, Albert Martos and Ramon Santamaria
*
*   This game has been created using raylib v1.3 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2015 Ramon Santamaria (@raysan5)
*
********************************************************************************************/
]#

import raylib, std/[lenientops, random, strformat]
import std/math
import std/macros

const deg2rad = PI / 180.0'f32
# ##################################
# Some Defines
# ##################################
const
  screenWidth = 800
  screenHeight = 450

  playerBaseSize = 20.0
  playerSpeed = 6.0
  playerMaxShoots = 10

  meteorsSpeed = 2
  maxBigMeteors = 2
  maxMediumMeteors = 8
  maxSmallMeteors = 16

# ##################################
# Types and Structures Definition
# ##################################


type
  PlayerT = object
    position: Vector2
    speed: Vector2
    acceleration: float32
    rotation: float32
    collider: Vector3
    color: Color

  ShootT = object
     position: Vector2
     speed: Vector2
     radius: float32 = 0.0
     rotation: float32 = 0.0
     lifeSpawn: int32 = 0
     active: bool = false
     color: Color

  MeteorT = object
    position: Vector2
    speed: Vector2
    radius: float = 0.0
    active: bool = false
    color: Color

  Floppy = object
    position: Vector2
    radius: int32
    color: Color

                        
# #############################
# Global Variables Declaration
# #############################

var
  gameOver:bool = false
  pause:bool = false
  victory:bool = false

  shipHeight:float32 = 0.0

  player: PlayerT
  floppy: Floppy
                                          
  shoot: array[playerMaxShoots, ShootT] 
        
  bigMeteor: array[maxBigMeteors, MeteorT]
  mediumMeteor: array[maxMediumMeteors, MeteorT]
  smallMeteor: array[maxSmallMeteors, MeteorT]
  
  midMeteorsCount = 0
  smallMeteorsCount = 0
  destroyedMeteorsCount = 0

# ##########################  
#  Program main entry point
# ##########################

# ##########################  
# Module Functions Definitions (local)
# ##########################

proc initGame =
  var
    posx, posy: int32 = 0
    velx, vely: int32 = 0
    correctRange:bool = false
    victory:bool = false
    pause:bool = false
    

  shipHeight = (playerBaseSize/2.0'f32) / tan(20.0*deg2rad)

  # Initialization player
  player.position = Vector2(x:screenWidth/2.0'f32, y:screenHeight/2.0'f32 - shipHeight/2.0'f32)
  player.speed = Vector2(x:0.0, y:0.0)
  player.acceleration = 0.0'f32
  player.rotation = 0.0'f32
  player.collider = Vector3(x: player.position.x + sin(player.rotation*deg2rad)*(shipHeight/2.5'f32), y: player.position.y - cos(player.rotation*deg2rad)*(shipHeight/2.5'f32), z: 12.0)

  player.color = LightGray
  destroyedMeteorsCount = 0

  for i in 0..<playerMaxShoots:
    shoot[i].position = Vector2(x:0.0, y:0.0)
    shoot[i].speed = Vector2(x:0.0, y:0.0)
    shoot[i].radius = 2
    shoot[i].active = false
    shoot[i].lifeSpawn = 0
    shoot[i].color = White

  for i in 0..maxBigMeteors:
    posx = int32(rand(screenWidth-1))
    while not correctRange:
      if (posx > screenWidth/2 - 150 and posx < screenWidth/2 + 150):
        posx = int32(rand(screenWidth-1))
      else:
        correctRange = true

    correctRange = false
    posy = int32(rand(screenHeight-1))

    
  


    
proc updateGame =
  var
    a = 0

proc drawGame =
  beginDrawing()
  clearBackground(RayWhite)
  endDrawing()

# TODO: What does the {.cdecl.} do ? !!
proc updateDrawFrame {.cdecl.} =
  # Update and Draw (one frame)
  updateGame()
  drawGame()

proc unloadGame =
  # Unload game variables
  # TODO: Unload all dynamic loaded data (textures, sounds, models...)
  discard

  
# ##########################
# Program main entry point
# ##########################

proc main =

  # Initialization
  initWindow(screenWidth, screenHeight, "classic game: asteroids")
  try:
    initGame()
    when defined(emscripten):
      emscriptenSetMainLoop(updateDrawFrame, 60, 1)
    else:
      setTargetFPS(60)

      # Main game loop
      while not windowShouldClose(): # Detect window close button or ESC key
        # Update and Draw
        updateDrawFrame()
        
        
    # De-Initialization
    unloadGame() # Unload loaded data (textures, sounds, models...)
  finally:
    closeWindow() # Close window and OpenGL context

main()
