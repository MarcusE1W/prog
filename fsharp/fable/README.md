# Install

https://dotnet.microsoft.com/en-us/download/dotnet/thank-you/sdk-6.0.416-linux-arm64-binaries
https://dotnet.microsoft.com/en-us/download

https://fable.io/docs/getting-started/setup.html

## prerequisits
- node.js
- ...
- Femto: https://fable.io/blog/2019/2019-06-29-Introducing-Femto.html

# Courses / learning

## Fable

### Fable docu
https://fable.io/docs/
- ok for setup but not much more

### Starting with Fable
https://dev.to/semuserable/starting-with-fable-f-kbi

### F# |> BABEL
http://tpetricek.github.io/Fable/

### Conquer the JavaScript Ecosystem with F# and Fable - Audience Level: Beginner
https://skillsmatter.com/skillscasts/9732-conquer-the-javascript-ecosystem-with-f-sharp-and-fable-audience-level-beginner

### F# Interop with Javascript in Fable
https://medium.com/@zaid.naom/f-interop-with-javascript-in-fable-the-complete-guide-ccc5b896a59f

### Exploring The F# Frontend Landscape
https://dev.to/tunaxor/exploring-the-f-frontend-landscape-13aa



## Elmish
https://zaid-ajaj.github.io/the-elmish-book/#/

https://elmish.github.io/elmish/docs/basics.html
