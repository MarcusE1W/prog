# Issues

## ada_ts_mode
- After semicolon, jump of X characters to the right

## als build error
```
   [Ada]          tgen-marshalling.adb
   [Ada]          tgen-context.ads
tgen-types.ads:190:17: (style) redundant parentheses [-gnatyz]
tgen-types.ads:191:26: (style) redundant parentheses [-gnatyz]

   compilation of tgen-types.adb failed

gprbuild: *** compilation phase failed
error: Command ["gprbuild", "-s", "-j0", "-p", "-P", "/home/mmw/.local/share/alire/builds/libadalang_tools_23.0.0_75f92679/fcb7a945c1fce37a70b825a9065edd076d4026de570164bdac6be6ddf03809a3/src/lal_tools.gpr"] exited with code 4
error: Compilation failed.
```

## als crate wit gnat_extensa_elf
Why does alire install gnat_xtensa_elf as dependency?

```
ada_language_server=23.0.0 successfully retrieved.
Dependencies were solved as follows:                                          

   +📦 gnat             14.2.1 (new,gnat_xtensa_elf,indirect,binary)
   +   gnatcoll         23.0.0 (new)
   +   gnatcoll_gmp     23.0.0 (new,indirect)
   +   gnatcoll_iconv   23.0.0 (new,indirect)
   +   langkit_support  23.0.0 (new,indirect)
   +   libadalang       23.0.0 (new)
   +   libadalang_tools 23.0.0 (new)
   +📦 libgmp           6.3.0  (new,indirect,system package)
   +   libgnatdoc       23.0.0 (new)
   +   libgpr           23.0.0 (new,indirect)
   +   markdown         23.0.0 (new,indirect)
   +   templates_parser 23.0.0 (new,indirect)
   +   vss              23.0.0 (new)
   +   xmlada           23.0.0 (new,indirect)

 mmw  ~/.local/git/ada  alr toolchain
CRATE           VERSION STATUS    NOTES                                                                   
gprbuild        22.0.0  Available Detected at (...)/alire/toolchains/gprbuild_22.0.1_19730c52/bin/gprbuild
gprbuild        22.0.1  Default                                                                           
gnat_xtensa_elf 14.2.1  Available                                                                         
gnat_arm_elf    14.2.1  Available                                                                         
gnat_native     14.2.1  Default                                                                           
gnat_external   14.2.0  Available Detected at (...)e/alire/toolchains/gnat_native_14.2.1_dc6f8344/bin/gnat
 mmw  ~/.local/git/ada  
```

## Error libadalang_tools


```
...

   [Ada]          libadalang-unparsing_implementation.adb
   [Ada]          libadalang-generic_impl.adb
   [Ada]          libadalang-rewriting_implementation.adb
   [Ada]          libadalang-generic_introspection.adb
Build Libraries
   [gprlib]       adalang.lexch
   [bind SAL]     adalang
   [Ada]          b__adalang.adb
   [objcopy]      p__adalang_0.o
   [archive]      libadalang.a
ⓘ Building libadalang_tools=24.0.0/src/lal_tools.gpr (1/2)...
Setup
   [mkdir]        object directory for project LAL_Tools
   [mkdir]        library directory for project LAL_Tools
Compile
   [Ada]          tgen.ads
   [Ada]          tgen-types.adb
   [Ada]          tgen-types-record_types.adb
   [Ada]          tgen-types-real_types.adb
   [Ada]          tgen-types-int_types.adb
   [Ada]          tgen-types-enum_types.adb
   [Ada]          tgen-types-discrete_types.adb
   [Ada]          tgen-types-constraints.adb
   [Ada]          tgen-types-array_types.adb
   [Ada]          tgen-strings.adb
   [Ada]          tgen-strategies.adb
   [Ada]          tgen-random.adb
   [Ada]          tgen-numerics.ads
   [Ada]          tgen-marshalling_lib.adb
tgen-marshalling_lib.adb:775:19: warning: types for unchecked conversion have different sizes [-gnatwz]
tgen-marshalling_lib.adb:880:19: warning: types for unchecked conversion have different sizes [-gnatwz]
tgen-random.adb:424:04: warning: types for unchecked conversion have different sizes [-gnatwz]
tgen-random.adb:427:04: warning: types for unchecked conversion have different sizes [-gnatwz]

   compilation of tgen-marshalling_lib.adb failed
   compilation of tgen-random.adb failed

gprbuild: *** compilation phase failed
error: Command ["gprbuild", "-s", "-j0", "-p", "-P", "/run/user/1000/alr-afxv.tmp/libadalang_tools_24.0.0_d864b5a8/src/lal_tools.gpr"] exited with code 4
error: Build failed, cannot perform installation
```


### libadalang_tools_installer

```
   +📦 gnat             14.2.1 (new,gnat_xtensa_elf,indirect,binary)
   +   gnatcoll         23.0.0 (new,indirect)
   +   gnatcoll_gmp     23.0.0 (new,indirect)
   +   gnatcoll_iconv   23.0.0 (new,indirect)
   +   langkit_support  23.0.0 (new,indirect)
   +   libadalang       23.0.0 (new,indirect)
   +   libadalang_tools 23.0.0 (new)
   +📦 libgmp           6.3.0  (new,indirect,system package)
   +   libgpr           23.0.0 (new,indirect)
   +   templates_parser 23.0.0 (new,indirect)
   +   xmlada           23.0.0 (new,indirect)
```
why gant_xtensa_elf ???
How to uninstall 

# Install

Install Alire from 

# compile
to only comile and not build
`gnatmake -c file.adb`

# beginners guide

## Text IO

print: string, int, float, array, record..
get: int, string

## package name shortening
```
with Ada.Text_IO;
procedure main is
package Tio renames Ada.Text_IO;
begin
  Tio.put_Line( "Tescht");
end main;
```


```
with Text_IO;
procedure main is
begin
  Text_IO.put_Line( "Tescht");
end main;
```




## naming conventions

Types: first letter capital, last letter capital "T"
variables: first letter small
procedures: first letter small
Packages: first letter capital

snake_Case ?

