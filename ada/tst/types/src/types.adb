with Ada.Text_IO; use Ada.Text_IO;

procedure Types is

   procedure Loops1 is
      -- use types to run the show for loops
      type LoopType is new Integer range 1 .. 5;

   begin
      Put_Line ("Type Loop");

      for i in LoopType loop
         Put (LoopType'Image (i));
      end loop;

      New_Line;
   end Loops1;

   -------------------------------------------------------------

   procedure Loops2 is
      -- iterate through array without index
      type Vector_Type is array (1 .. 5) of Integer;

      procedure Negate (vector : in out Vector_Type) with
        Pre  => (for all Element of vector => Element /= Integer'First),
        Post =>
         (for all i in vector'First .. vector'Last =>
            vector (i) = -vector'Old (i))
      is
      begin
         for element of vector loop
            element := -element;
         end loop;
      end Negate;

      function Negate_Func (vector : in out Vector_Type) return Vector_Type is
      -- in out is required to make vector and with that element a variable rather than a constant
      begin
         for element of vector loop
            element := -element;
         end loop;
         return vector;
      end Negate_Func;

      v : Vector_Type := (2, -3, 4, 5, -12);
   begin
      Put_Line ("Cool shit");
      Negate (v);
      for e of v loop
         Put (e'Image);
      end loop;
      New_Line;

      v := Negate_Func (v);
      for e of v loop
         Put (e'Image);
      end loop;
      New_Line;

      New_Line;
   end Loops2;

   -------------------------------------------------------------

   procedure BitOperations is
      -- use packed arrays for bit fumbeling insted of raw bits

      type Bit_Type is
        mod 2**1;  -- unsigned type base 2 with 1 bit with mod in case of overflow

      type Byte_Bit is array (0 .. 7) of Bit_Type;
      pragma Pack (Byte_Bit);

      b       : Bit_Type := 0;
      c, d, e : Byte_Bit := (0, 0, 0, 0, 0, 0, 0, 0);

   begin
      Put_Line ("Bit Games");
      b := 1; -- only 0 or 1 allowed

      c (3) := 1;  --set bit to 1
      c (4) :=
        Bit_Type'Succ
          (c
             (4)); -- flip bit from 0 to 1 or from 1 to 0, depending on where it was

      d (5) := 1;

      -- iterate over all bits
      for bit of c loop
         bit := not bit;
         Put (Bit_Type'Image (bit));
      end loop;

      -- e := c or d;

      New_Line;
   end BitOperations;

   -----------------------------------------------------------------

   procedure BitOperations2 is
      -- define the exact size of recorde and data-types

      -- 4 bits
      type Field is range 1 .. 16 with
        Size => 4;
        -- same as
      type Field2 is range 0 .. 15 with
        Size => 4; -- both works, a matter of taste

        -- 1 bit, automatic assignment of values
      type Edge_Detection is (Rising_Edge, Falling_Edge) with
        Size => 1;

        -- assign values directly (optioinal)
      for Edge_Detection use (Rising_Edge => 0, Falling_Edge => 1);

      -- 2 bits, only 2 values used and 2 values reserved/unaccessable
      type Power_Value is (Min_VCC, Max_VCC) with
        Size => 2;
      for Power_Value use (Min_VCC => 0, Max_VCC => 3);
      --  In this case, values 1 and 2 are reserved by the hardware
      --  manufacturer, so not represented at the software level.
   begin
      Put_Line ("Bit sizing");
      -- Some Type'Size gives you the size of the type
      Put_Line
        ("the Size of Power_Value (2bits) is:" &
         Integer'Image (Power_Value'Size));
      New_Line;
   end BitOperations2;

   ----------------------------------------------------------------

   -- procedure Register is
   --      type Field1 is … with Size => 5;
   -- begin
   --      New_Line;
   ----------------------------------------------------------------

   type BitArray is array (Natural range <>) of Boolean;
   pragma Pack (BitArray);

   type Monitor_Info is record
      On     : Boolean;
      Count  : Natural range 0 .. 127;
      Status : BitArray (0 .. 7);
   end record;

   for Monitor_Info use record
      On     at 0 range 0 ..  0;
      Count  at 0 range 1 ..  7;
      Status at 0 range 8 .. 15;
   end record;

   ----------------------------------------------------------------
   -- Matrizes,	 multidimensional array,	 array of arrays
   ----------------------------------------------------------------
   
   procedure MatrixProc is
      type LineT is array (1..2) of Integer;
   --    type MatrixT is array (1 .. 2, 1..2) of Integer;
      type MatrixT is array (1 .. 2) of LineT; -- array of arrays

	  m1 : MatrixT := (others => (others => 42));
	  l1 : LineT;
   begin
	  l1 := m1 (1);
	  m1(1)(2) := 12;
   end MatrixProc;

   ----------------------------------------------------------------


   procedure Strings_and_Characters is
      c1 : Character := 'a';
	  i1 : Integer := 13;
	  begin

	  Put_Line(" ============ Characters =============");
   -- Convert Int to Char, char to int
   c1 := Character'Val (65); --converts integer to ASCII code character -> A
   Put (c1);
   New_Line;
	  i1 := Character'Pos ('A');
	  Put_Line ("Code for A: " & i1'Image);
	  	  Put_Line(" ============ Strings =============");
	  -- create var with string of unknown lenght. declare something ...
   end Strings_and_Characters;

   ----------------------------------------------------------------


   procedure Attributes is
   begin
      null;
   -- Character'Pos( int ) _> returns char of Int
   -- Character'Val( char ) -> returns integer of char
   -- AnyType'Image -> returns string representation
   end Attributes;

      ----------------------------------------------------------------
   
begin

   Loops1;
   Loops2;
   BitOperations;
   BitOperations2;
   Strings_and_Characters;
   Put_Line ("Finished");
end Types;
