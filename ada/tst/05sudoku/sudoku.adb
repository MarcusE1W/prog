with Ada.Text_IO;
with Ada.Integer_Text_IO;

procedure Sudoku is
    package Tio renames Ada.Text_IO;
    package Iio renames Ada.Integer_Text_IO;

    subtype Su_ValueR is Integer range 0 .. 9;
    subtype Su_LineR is Su_ValueR range 1 .. 9;
    type Su_LineT is array (Su_LineR) of Su_ValueR;
    type SudokuT is array (Su_LineR, Su_LineR) of Su_ValueR;
    -- type SudokuT is array (Su_LineR) of Su_LineT;

    subtype Square_LineR is Integer range 1 .. 3;
    type SquareT is
       array (Square_LineR, Square_LineR) of Su_ValueR;       -- 2 dim. array

    type TestSetT is array (Su_LineR) of Boolean;

    type ResultT is record
        x, y : Integer;
    end record;

    function squareCheck (sq : in SquareT) return ResultT is
        check : TestSetT := (others => False);
        r     : ResultT  := (x => 0, y => 0);
    begin
        Tio.Put_Line ("squareCheck");
        for i in Square_LineR loop
            for j in Square_LineR loop
                if sq (i, j) /= 0 then
                    if check (sq (i, j)) then --  number already used
                        r.x := i;
                        r.y := j;
                        return r;
                    else
                        check (sq (i, j)) := True; -- mark: number first time used
                    end if;
                end if;
            end loop;
        end loop;
        return r;
    end squareCheck;

    function lineCheck (line : in Su_LineT) return Integer is
        check : TestSetT := (others => False);
    begin
        for i in Su_LineR loop
            if line (i) /= 0 then
                if check (line (i)) = True then
                    return Integer (i);
                else
                    check (line (i)) := True;
                end if;
            end if;
        end loop;
        return 22;
    end lineCheck;

    function getColumn (su : in SudokuT; col : in Integer) return Su_LineT
    is
        line : Su_LineT;
    begin
        for i in Su_LineR loop
           line (i) := su (i, col);
        end loop;
        return line;
    end getColumn;

	function checkLines (su : SudokuT) return ResultT is
 	  r: ResultT := (0, 0);
	  line_r: Integer := 0;
	  row, col : Integer := 0;
	begin
      --check rows
      for row in Su_LineR loop
         if line_r /= 0 then
            r.x := row;
            r.y := line_r;
            return r;
         end if;
      end loop;
	  return r;
   end checkLines;
	
    procedure printSudoku (su : in SudokuT) is 
   begin
        Tio.Put_Line ("");
        Tio.Put_Line (" ===================== ");
        for i in Su_LineR loop
            for j in Su_LineR loop
                Iio.Put (Integer (su (i, j)), 3);
            end loop;
            Tio.Put_Line ("");
        end loop;
        Tio.Put_Line (" ===================== ");
    end printSudoku;

    m1 : SquareT      := (others => (others => 0));   -- initialise with 0
    r1 : ResultT      := (0, 0);
    s2 : SudokuT      := (others => (others => 0));
    l2 : Su_LineT := (others => 0);
    r2 : Integer      := 0;

begin
    Tio.Put_Line (" == Sudoku checking, yeah");

    m1 := ((0, 0, 3), (4, 5, 6), (7, 8, 9));
    r1 := squareCheck (m1);

    if r1.x = 0 then
        Tio.Put_Line ("The matrix seems ok.");
    else
        Iio.Put (r1.x, 3);
        Iio.Put (r1.y, 3);
        Tio.Put_Line (" There is a glitch in the matrix.");
    end if;

    Tio.Put_Line (" ===================== ");

    s2 :=
       ((0, 9, 1, 0, 0, 2, 0, 0, 3), (0, 0, 0, 0, 7, 0, 6, 0, 0),
        (0, 8, 0, 4, 0, 0, 0, 1, 0), (0, 0, 5, 0, 0, 3, 8, 7, 0),
        (8, 2, 5, 0, 0, 0, 0, 0, 0), (0, 0, 6, 0, 0, 5, 1, 3, 0),
        (0, 7, 0, 3, 0, 0, 0, 2, 0), (0, 0, 0, 0, 4, 0, 9, 0, 0),
        (0, 6, 2, 0, 0, 8, 0, 0, 7));
    printSudoku (s2);

    l2 := getColumn (s2, 3);

    for i in Su_LineR loop
        Iio.Put (Integer (l2 (i)), 3);
    end loop;
    Tio.Put_Line ("");

    r2 := lineCheck (l2);
    if r2 = 22 then
        Tio.Put_Line ("Diese Zeile ist ok");
    else
        Tio.Put ("something fishy here: ");
        Iio.Put (r2);
        Tio.Put_Line ("");
    end if;

    Tio.Put_Line ("");
    Tio.Put_Line (" the end ");

end Sudoku;
