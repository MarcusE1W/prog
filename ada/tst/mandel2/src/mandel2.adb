with Ada.Text_IO; use Ada.Text_IO;

procedure Mandel is

	type X_Res is range 0 .. 100;
	type Y_Res is range 0 .. 50;


	xmin : constant Float := -2.0;
	ymin : constant Float := -1.7;

	xmax : constant Float := 1.5;
	ymax : constant Float := 1.7;

	max_iter : constant Integer := 100;

	subtype Iter_Range is Integer range 1 .. max_iter;

	xstep : constant Float := ( xmax - xmin ) / Float( X_Res'last );
	ystep : constant Float := ( ymax - ymin ) / Float( Y_Res'last );

	cx : Float := 0.0;
	cy : Float := 0.0;

	iter : Iter_Range  := 1;

	function Mandelpoint( c_real, c_imag: Float; max_iter: Integer ) return Iter_Range is

		z0x : Float := 0.0;
		z0y : Float := 0.0;
		z1x : Float := 0.0;
		z1y : Float := 0.0;

	    r_abs : Float := 0.0;

		iteration : Iter_Range := 1;


	begin

		while iteration <  max_iter and r_abs < 4.0 loop
			z1x := z0x * z0x - z0y * z0y;
			z1y := 2.0 * (z0x * z0y); -- z0x * z0y + z0x * z0y

			z1x := z1x + c_real;
			z1y := z1y + c_imag;

            z0x := z1x;
            z0y := z1y;

            r_abs := (z1x * z1x) + (z1y * z1y);

		    iteration := iteration + 1;
		end loop;

		return iteration;
	end Mandelpoint;


begin

    cy := ymin;
	for j in Y_Res loop

	  cx := xmin;
	  for i in X_Res loop

        iter := Mandelpoint( c_real => cx, c_imag => cy, max_iter => max_iter );
        case iter is
        	when max_iter => Put( " ");
        	when others => Put( "*");
        end case;

	  	cx := cx + xstep;
	  end loop;

	  cy := cy + ystep;
	  Put_Line ( "|");
	end loop;

end Mandel;
