-- We are importing two items: the ‘elements’ child of LISP, which we
-- haven’t yet shown (it is on the right; this would normally create a
-- circular-dependency, and I will show you Ada’s way of handling it),
-- and one of Ada’s language-defined containers.
With
LISP.elements,
Ada.Containers.Indefinite_Vectors;
-- ‘Use Type’ was a visibility modifier introduced in Ada 95 to allow
-- a type’s operators (+, -. *, /, etc) to be visible without having
-- to use a ‘use’ clause, which would “pollute the namespace.”
-- Ada 2012 extends this with ‘All’, indicating that all the type’s
-- primitive operations are to be made visible.
Use All Type LISP.Elements.Element;

Package LISP.Lists is
-- The following package, “implementation”, is the instantiation of
-- a generic package; a generic must be explicitly instantiated
-- before its use in your program text.

Package Implementation is new Ada.Containers.Indefinite_Vectors
(Index_Type => Positive, Element_Type => LISP.Elements.Element);
-- (PS: The above is using named association, which can be helpful.)
-- The following line declares inheritance, with no extension to the
-- data of the base object/class; there are, however, ‘methods’
-- which are added below.

Type LIST is new Implementation.Vector with null record;

  -- Primitive operations are those subprograms which interact with
  -- a type and are declared before the type is frozen. (Read as:
  -- in the same declaring unit, before some point which the compiler
  -- must commit to a type.)
  --
  -- The following are primitive operations for LIST.
  Function Evaluate(Input : List) Return List;
  Function Head(Input : List) Return LISP.Elements.Element;
  Function Tail(Input : List) Return List;
  Function To_String (Input : List) Return String;
  Function Homoginized_List( Input : List ) Return Boolean;

End Lisp.Lists;