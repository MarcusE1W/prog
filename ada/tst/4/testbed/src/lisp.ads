Package LISP is
Pragma Pure;

-- Parse_Error is used to signal when there has been a problem parsing input; such as unbalanced parentheses.
Parse_Error,
-- Undefined_Element is used to signal when there is a reference to an undefined entity; usually a function.
Undefined_Element :Exception;
End LISP;
