-- ‘With’ introduces a compilation unit dependency to this compilation unit;
-- it’s roughly analogous to the ‘using’ of C#… except that it doesn’t alter visibility.
-- Note: a compilation unit is a procedure, function, or package.

With
Ada.Text_IO;-- Ada.Text_IO: exactly what it says.

-- This names the current procedure “testbed”.
Procedure Testbed is

  -- Stubs for Read, Eval, and Print.

  Procedure Read is
  begin
    -- The reserved-word ‘null’ can be used to indicate a null-statement.
    -- This explicit naming of the null-statement means that the compiler
    -- can detect stray semi-colons.
    null;
  end Read;

  Procedure Eval is
  begin
    null;
  end Eval;

  Procedure Print is
  begin
    null;
  end Print;

  -- Hack to terminate the loop; we will replace this later, with an
  -- orderly shutdown-mechanism.
  Loop_Count : Positive:= 1;

Begin

    -- We can name loops, and declaration blocks, this allows for stability when
    -- adding or deleting them, as is common when altering algorithms. This can
    -- help because instead of numbering the constructs to ‘break’, the exit is
    -- tied to the loop that it is associated with.
    --
    -- EXAMPLE: Below we name the Read-Eval-Print Loop as “REPL”.

    REPL:
    loop
    -- Here we use an attribute, “image”, to obtain the string-value of the
    -- counter. All discrete types, including enumerations, have this.

    Ada.Text_IO.Put_Line("[Count:" & Positive'Image(Loop_Count) & ']');

    -- Call read, eval, and print: this is the lungs, heart and blood of our interpreter.
    Read;
    Eval;
    Print;

    -- Set up the exit-condition; the name is optional but useful when dealing
    -- with nested loops & declare-blocks.
    Exit REPL when Loop_Count = 10;

    -- Update the loop-counter.
    Loop_Count:= Loop_Count + 1;

    end loop REPL;

    -- Print some text to indicate an orderly shutdown was achieved.
    Ada.Text_IO.Put_Line( "Goodbye." );

End Testbed;
