Limited with LISP.Lists;

Package LISP.Elements is

  Type Data_Type is (Empty_Type,
                   Integer_Type,
                   String_Type, Name_Type,
                   List_Type);

  Type Element(<>) is tagged private;

  Function Create Return Element;
  Function Create( Item : Integer ) Return Element;
  Function Create( Item : String ) Return Element;
  Function Create( Item : Lists.List ) Return Element;

  Function To_String(Item: Element) return String;
  Function Get_Type (Item: Element) return Data_Type;

  Function To_List (Item: Element) return Lists.LIST;
  Function As_List (Item: Element) return Lists.LIST;

Private

  Type Element( Data : Data_Type ) is tagged record
    Case Data is
    when Empty_Type =>Null ;
    when Integer_Type => Int_Val : Integer ;
    when Name_Type |
         String_Type => Str_val : not null access
Standard.String;
    when List_Type => List_Val : Not Null Access
LISP.Lists.List‘Class;
    End case;
  End Record;

  Function Create Return Element is ( Data => Empty_Type );
  Function Get_Type (Item: Element) return Data_Type is ( Item.Data );

End Lisp.Elements;
