with Ada.Text_IO;
with Ada.Integer_Text_IO;

procedure sieve is

	maxSieve : constant Integer := 100;

	-- needs to be subtype, otherwise type conversion is required at every asignment with integer
	subtype SieveRange is Integer range 2 .. maxSieve;
	type SieveType is array (SieveRange) of Integer;


	procedure printSieve( sieve: IN SieveType) is
		lc : Integer :=1 ; -- lineCount
	begin
		Ada.Text_IO.New_Line;
		for element of sieve loop
			Ada.Integer_Text_IO.Put( element, 3 );

			if lc = 10 then
				Ada.Text_IO.New_Line;
				lc := 0;
			end if;
			lc := lc + 1;
		end loop;
		Ada.Text_IO.New_Line;
	end printSieve;


	procedure checkSieve( sieve: IN OUT SieveType ) is

		x : Integer := 0;
		wait : Character := ' ';

	begin
		for i in sieve'range loop

			if sieve(i) = 0 then
		    	x := i;
		    	while x <= maxSieve loop
		    		sieve(x) := 1;
		    		x := x + i;
		    	end loop;
		    	sieve(i) := i;

				printSieve( sieve );
				Ada.Text_IO.Get_Immediate(wait); -- wait for any keypress, no RTN required
		    end if;

		end loop;
	end checkSieve;

	sieve : SieveType := (others => 0); -- init with zero

begin
	Ada.Text_IO.New_Line;
	printSieve( sieve ); --print the empty sieve
	checkSieve( sieve );
end sieve;

