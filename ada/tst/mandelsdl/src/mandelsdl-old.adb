--with Ada.Calendar;
with Ada.Text_IO; use Ada.Text_IO;
-- with Ada.Unchecked_Conversion;
-- with SDL;
with SDL.Error;
with SDL.Log;
with SDL.Video.Palettes;
with SDL.Video.Rectangles;
with SDL.Video.Renderers;
with SDL.Video.Renderers.Makers;
with SDL.Video.Windows.Makers;
with Interfaces.C; use Interfaces.C;

procedure Mandelsdl is
    -- Window stuff
	window : SDL.Video.Windows.Window;
--	Pixel    : SDL.Video.Rectangles.Point := (X => 200, Y => 200);
--	Colour : SDL.Video.Palettes.Colour := (Red => 100, Green => 150, Blue => 200, Alpha => 255);

	dark : SDL.Video.Palettes.Colour := (Red => 10, Green => 10, Blue => 10, Alpha => 255);
	light : SDL.Video.Palettes.Colour := (Red => 220, Green => 230, Blue => 240, Alpha => 220);


	MyRenderer : SDL.Video.Renderers.Renderer;

	wait: Character;

	-- Mandel stuff
	type X_Res is range 0 .. 80;
	type Y_Res is range 0 .. 60;


	xmin : constant Float := -1.9;
	ymin : constant Float := -1.7;
	xmax : constant Float := 1.5;
	ymax : constant Float := 1.7;

	max_iter : constant Integer := 100;

	subtype Iter_Range is Integer range 1 .. max_iter;

	xstep : constant Float := ( xmax - xmin ) / Float( X_Res'last );
	ystep : constant Float := ( ymax - ymin ) / Float( Y_Res'last );

	cx : Float := 0.0;
	cy : Float := 0.0;

	iter : Iter_Range  := 1;

	procedure Point(
				renderer: in out SDL.Video.Renderers.Renderer;
				x : Integer;
				y : Integer;
				 colour  : in out SDL.Video.Palettes.Colour)
	is
		pixel      : SDL.Video.Rectangles.Point;
	begin

			-- convert
			pixel.x := Int(x);
			pixel.y := Int(y);

			-- draw point
			SDL.Video.Renderers.Set_Draw_Colour( renderer, colour );
			SDL.Video.Renderers.Draw( renderer, pixel);

			-- update renderer and window
			SDL.Video.Renderers.Present( renderer);
	end Point;


	function Mandelpoint( c_real, c_imag: Float; max_iter: Integer ) return Iter_Range is

			z0x : Float := 0.0;
			z0y : Float := 0.0;
			z1x : Float := 0.0;
			z1y : Float := 0.0;

		  r_abs : Float := 0.0;

			iteration : Iter_Range := 1;


	begin

			while iteration <  max_iter and r_abs < 4.0 loop
				z1x := z0x * z0x - z0y * z0y;
				z1y := 2.0 * (z0x * z0y); -- z0x * z0y + z0x * z0y

				z1x := z1x + c_real;
				z1y := z1y + c_imag;

	            z0x := z1x;
	            z0y := z1y;

	            r_abs := (z1x * z1x) + (z1y * z1y);

			    iteration := iteration + 1;
			end loop;

			return iteration;
	end Mandelpoint;



begin
	-- turn on debugging
	SDL.Log.Set (Category => SDL.Log.Application, Priority => SDL.Log.Debug);

	-- init Window
	if SDL.Initialise (Flags => SDL.Enable_Screen) = True then
		SDL.Video.Windows.Makers.Create (
      			Win    => window,
                Title  => "Surface (Esc to exit)",
				X      => 100,
				Y      => 100,
                Width  => 801,
                Height => 601,
                Flags  => SDL.Video.Windows.Resizable);

		-- create renderer
		SDL.Video.Renderers.Makers.Create (MyRenderer, window);

		-- Point( myRenderer, pixel, colour);

		-- Mandel stuff
	  cy := ymin;
		for j in Y_Res loop

		  cx := xmin;
		  for i in X_Res loop

	        iter := Mandelpoint( c_real => cx, c_imag => cy, max_iter => max_iter );

	        case iter is
	        	when max_iter => Point( myRenderer, Integer(i), Integer(j), dark);
	        	when others => Point( myRenderer, Integer(i), Integer(j), light);
	        end case;

		  	cx := cx + xstep;
		  end loop;

		  cy := cy + ystep;
		end loop;





		-- wait for key press in terminal
		SDL.Log.Put_Debug ("Press any key here in the terminal to exit");
		Get_Immediate( wait);

		-- clean up window
		window.Finalize;
		SDL.Finalise;
   end if;
end Mandelsdl;
