--with Ada.Calendar;
with Ada.Text_IO; use Ada.Text_IO;
-- with Ada.Unchecked_Conversion;
with SDL;
with SDL.Error;
with SDL.Log;
with SDL.Video.Palettes;
with SDL.Video.Rectangles;
with SDL.Video.Renderers;
with SDL.Video.Renderers.Makers;
with SDL.Video.Windows;
with SDL.Video.Windows.Makers;
with Interfaces.C; use Interfaces.C;
--with SDL.Timers;

procedure PointTest is
    -- Window stuff
	window : SDL.Video.Windows.Window;
--	Pixel    : SDL.Video.Rectangles.Point := (X => 200, Y => 200);
--	Colour : SDL.Video.Palettes.Colour := (Red => 100, Green => 150, Blue => 200, Alpha => 255);

	dark : SDL.Video.Palettes.Colour := (Red => 10, Green => 10, Blue => 10, Alpha => 255);
	light : SDL.Video.Palettes.Colour := (Red => 220, Green => 230, Blue => 240, Alpha => 220);


	myRenderer : SDL.Video.Renderers.Renderer;

	-- Mandel stuff
	type X_Res is range 0 .. 20;
	type Y_Res is range 0 .. 10;

	procedure Point(
				renderer: in out SDL.Video.Renderers.Renderer;
				x : Integer;
				y : Integer;
				 colour  : in out SDL.Video.Palettes.Colour)
	is
		pixel      : SDL.Video.Rectangles.Point;
	begin

			-- convert
			pixel.x := Int(x);
			pixel.y := Int(y);

			-- draw point
			SDL.Video.Renderers.Set_Draw_Colour( renderer, colour );
			SDL.Video.Renderers.Draw( renderer, pixel);

			-- update renderer and window
-- 		  SDL.Video.Renderers.Present( renderer);
	end Point;


procedure Window_Init( window : in out SDL.Video.Windows.Window; 
						                   myRenderer : in out SDL.Video.Renderers.Renderer) 
is
begin
	-- turn on debugging
	SDL.Log.Set (Category => SDL.Log.Application, Priority => SDL.Log.Debug);
	-- init Window
	if SDL.Initialise (Flags => SDL.Enable_Screen) = True then
		   SDL.Video.Windows.Makers.Create (
      			Win    => window,
        Title  => "Point Test 0.1",
	       	X      => SDL.Video.Windows.Undefined_Window_Position,
	       	Y      => SDL.Video.Windows.Undefined_Window_Position,
        Width  => 201,
        Height => 101);
--         Flags  => SDL.Video.Windows.Resizable);
 end if;

		-- create renderer
		SDL.Video.Renderers.Makers.Create (MyRenderer, window);
end Window_Init;


begin

    Window_Init( window, myRenderer);				

		-- loop per point
		for j in Y_Res loop

			  for i in X_Res loop
		        Point( myRenderer, Integer(i), Integer(j), light);
			  end loop;

		end loop;
		SDL.Video.Renderers.Present( myRenderer);

		-- 
		-- SDL.Timers.Wait_Delay (100);						

-- 		SDL.Log.Put_Debug ("Press any key here in the terminal to exit");
-- 		Ada.Text_IO.Get_Immediate(wait);


		-- clean up window
		window.Finalize;
		SDL.Finalise;

end PointTest;
