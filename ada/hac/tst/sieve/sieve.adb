with HAT;
-- Use HAT;

 procedure sieve is

 	maxSieve : constant Integer := 100;

	-- needs to be subtype, otherwise type conversion is required at every asignment with integer
	subtype SieveRange is Integer range 2 .. maxSieve;
	type SieveType is array (SieveRange) of Integer;
	sieve : SieveType := (others => 0); -- init with zero	

	procedure printSieve( sieve: IN SieveType) is
		lc : Integer :=1 ; -- lineCount
	begin
		HAT.New_Line;
		for element in SieveRange loop
			HAT.Put( sieve(element), 3 );
			
			if lc = 10 then 
				HAT.New_Line; 
				lc := 0; 
			end if;
			lc := lc + 1;
		end loop;
		HAT.New_Line;
	end printSieve;


	procedure checkSieve( sieve: IN OUT SieveType ) is
	
		x : Integer := 0;
		wait : Character := ' ';

	begin
		for i in SieveRange loop

			if sieve(i) = 0 then
		    	x := i;
		    	while x <= 100 loop -- and another Baehh
		    		sieve(x) := 1;
		    		x := x + i;
		    	end loop;
		    	sieve(i) := i;

				printSieve( sieve );
	--			HAT.Get_Immediate(wait); -- wait for any keypress, no RTN required
		    end if;

		end loop;
	end checkSieve;

begin
	HAT.New_Line;
	printSieve( sieve ); --print the empty sieve to start
	checkSieve( sieve );
end sieve;

