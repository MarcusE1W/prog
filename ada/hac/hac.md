# Installation on Linux

Prerequisit: To compile HAC a Ada compiler like GNAT has to be already installed.

Choose a folder where you want to install HAc and then type (or cut and paste):

```
git clone https://github.com/zertovitch/hac
cd hac
gantmake -P hac
```

to test type

```
cd exm
../hac gallery.adb
```


## Install

To make the commad 'hac' available in your path you can either link it to '/usr/bin'
e.g. in the hac folder type

If you are using a local 'bin' directory in e.g. '~/.local/bin' then type

```
??
```
## Copy to another linux distro

- build on a system that has a GNAT  compiler
- copy /usr/lib/aarch64-linux-gnu/libgnat-9.so.1 to hac folder
- copy the whole hac folder to new distro
- add to $PATH 

```
ln -s hac ~/.local/bin
```
if you don't have you might need to add this to you $PATH env variable

```
export PATH=~/.local/bin:$PATH
```

To make the file avaialbe system wide you need 'sudo'

```
sudo ln -s hac /usr/bin/hac
```


# Getting started

Here is a first programme. HAC is a subset of Ada but for the time beeing there are a few specialities to consider.

Create the text file 'hello.adb' with the editor of your choice.

```
with HAC_Pack; use HAC_Pack;

procedure hello is

begin

   Put_Line ("Learning Ada with HAC ");

end hello;
```

run with 'hac hello.adb'.

> with HAC_Pack; use HAC_Pack;

The first line is important to note. For the time beeing this line is required as the first line of all HAC programms. It makes a small subset of the Ada Standard Library aavailable 

# HAC language description

The HAC language descriptioin is a subset of Ada. All HAC programms can also be compiled bye a complete Ada compiler like GNAT.

## Types

- Integer
- Ranges
- Natural
- Real
- Enumerations
- Arrays
- String and VString

## Procedures

## Text IO


### Simple Types
- Constants
https://github.com/zertovitch/hac/blob/master/test/constants.adb
- Integer
https://github.com/zertovitch/hac/blob/master/test/integers.adb
	- var initialisation
https://github.com/zertovitch/hac/blob/master/test/var_init.adb
- Real
https://github.com/zertovitch/hac/blob/master/test/floats.adb
- String
	- concatination
https://github.com/zertovitch/hac/blob/master/test/strings.adb
- Character
- Duration ?


### Type Conversion

```
y := x + Real (i);
j := i + To_Int (3.51);
```

https://github.com/zertovitch/hac/blob/master/test/type_conversion.adb

### Arrays
https://github.com/zertovitch/hac/blob/master/test/sorting_tests.adb

### Enumerations

https://github.com/zertovitch/hac/blob/master/test/enumerations.adb

## Conditions

### If

https://github.com/zertovitch/hac/blob/master/test/if_then_elsif_else.adb

### Case

https://github.com/zertovitch/hac/blob/master/test/case_statement.adb

## Loops


### For

### While
https://github.com/zertovitch/hac/blob/master/test/loops.adb

## Recursion
https://github.com/zertovitch/hac/blob/master/test/recursion.adb

## Tasks
