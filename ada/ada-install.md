# Ada on Archlinux

```
https://github.com/reznikmm/aarch64-alire-index
```

a) download latest version of Alire and add to PATH
b) follow README

## cross-compiler for Arm microcontroller M0...
```
alr toolchain --select gnat_arm_elf
```


# Ada on Armbian

## Install

- gnat
- gprbuild
- gnat-gps

## for Cortex-M4

```
sudo apt install gnat-9-arm-linux-gnueabi
sudo apt install gcc-arm-none-eabi
```


There are three ARM compiler packages for `gnat-9` in Debian for ARM:
1. `gnat-9` - This is the native compiler
2. `gnat-9-arm-linux-gnueabi` - cross compiler, eabi stands for "embedded application interface"
3. `gnat-9-arm-linux-gnueabihf` - cross compiler with hard floating points

The third package is a special case and not used here. The first two package gice you these two compilers in the path:
1. `gnat` - native compiler
2. `arm-linux-gnueabi-gnat` - cross compiler for wich target ? What does the linux mean ?


## Compile a test

```
arm-linux-gnueabi-gnatmake -mlittle-endian -mfloat-abi=soft -mcpu=cortex-m0 -mthumb -march=armv6s-m first.adb
```

```
arm-linux-gnueabi-gnatmake -v -mlittle-endian -mfloat-abi=soft -mcpu=cortex-m0 -mthumb -march=armv6s-m first.adb

GNATMAKE 9.3.0
Copyright (C) 1992-2019, Free Software Foundation, Inc.
  "first.ali" being checked ...
End of compilation
  "first" missing.
arm-linux-gnueabi-gnatbind-9 -x first.ali
arm-linux-gnueabi-gnatlink-9 first.ali -mlittle-endian -mfloat-abi=soft -mcpu=cortex-m0 -mthumb -march=armv6s-m
/usr/lib/gcc-cross/arm-linux-gnueabi/9/../../../../arm-linux-gnueabi/bin/ld: first: warning: thumb-1 mode PLT generation not currently supported
collect2: error: ld returned 1 exit status
arm-linux-gnueabi-gnatlink-9: error when calling /usr/bin/arm-linux-gnueabi-gcc-9
arm-linux-gnueabi-gnatmake: *** link failed.
```

```
arm-linux-gnueabi-gnatlink -v  first.ali

GNATLINK 9.3.0
Copyright (C) 1995-2019, Free Software Foundation, Inc.
arm-linux-gnueabi-gcc-9 -c -mlittle-endian -mfloat-abi=soft -mcpu=cortex-m0 -mthumb -gnatA -gnatWb -gnatiw -gnatws /home/mmw/prog/ada/tst/first-Cortex-M4/b~first.adb
/usr/bin/arm-linux-gnueabi-gcc-9 b~first.o ./first.o -o first -L./ -L/usr/lib/gcc-cross/arm-linux-gnueabi/9/adalib/ -lgnat-9 -shared-libgcc
/usr/lib/gcc-cross/arm-linux-gnueabi/9/../../../../arm-linux-gnueabi/bin/ld: first: warning: thumb-1 mode PLT generation not currently supported
collect2: error: ld returned 1 exit status
arm-linux-gnueabi-gnatlink: error when calling /usr/bin/arm-linux-gnueabi-gcc-9
```

# Inspire Cortex-M tutorial

https://comp.lang.ada.narkive.com/wNnLES7b/inspirels-ada-on-cortex-tutorial-linker-issue

# Alire Ada toolchain - old

```
alr toolchain --select
alr toolchain --install gnat_arm_elf
```

Result:

```
CRATE         VERSION  STATUS    NOTES                        
gprbuild      21.0.0-1 Default                                
gprbuild      2019.0.0 Available Detected at /usr/bin/gprbuild
gnat_arm_elf  10.3.0-1 Available                              
gnat_native   10.3.0-1 Default                                
gnat_external 9.3.0    Available Detected at /usr/bin/gnat   
```
