The first surprise is that `alr`somehow wants to build a library for Hello World.
If I look at the .gpr file a confusing structure emerges.

`hello.gpr` imports ` config/hello_config.gpr` which imports `libhello.gpr`. At this point it becomes a bit overwhelming for a Hello World programme. 'libhello.gpr' is not in the downloaded folder. Instead it's in `.local/share/alire/builds/libhello_1.0.1_3c.../libhello.gpr`
So here the definition of a library starts for some reason.
Moving on, 'libhello.grp' finally imports `libhello_config.gpr`.

