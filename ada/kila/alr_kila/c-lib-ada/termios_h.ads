pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with bits_types_h;
limited with bits_termios_struct_h;
with bits_termios_h;

package termios_h is

   --  arg-macro: function CCEQ (val, c)
   --    return (c) = (val)  and then  (val) /= _POSIX_VDISABLE;
   subtype pid_t is bits_types_h.uu_pid_t;  -- termios.h:30

   function cfgetospeed (uu_termios_p : access constant bits_termios_struct_h.termios) return bits_termios_h.speed_t  -- termios.h:48
   with Import => True, 
        Convention => C, 
        External_Name => "cfgetospeed";

   function cfgetispeed (uu_termios_p : access constant bits_termios_struct_h.termios) return bits_termios_h.speed_t  -- termios.h:51
   with Import => True, 
        Convention => C, 
        External_Name => "cfgetispeed";

   function cfsetospeed (uu_termios_p : access bits_termios_struct_h.termios; uu_speed : bits_termios_h.speed_t) return int  -- termios.h:54
   with Import => True, 
        Convention => C, 
        External_Name => "cfsetospeed";

   function cfsetispeed (uu_termios_p : access bits_termios_struct_h.termios; uu_speed : bits_termios_h.speed_t) return int  -- termios.h:57
   with Import => True, 
        Convention => C, 
        External_Name => "cfsetispeed";

   function cfsetspeed (uu_termios_p : access bits_termios_struct_h.termios; uu_speed : bits_termios_h.speed_t) return int  -- termios.h:61
   with Import => True, 
        Convention => C, 
        External_Name => "cfsetspeed";

   function tcgetattr (uu_fd : int; uu_termios_p : access bits_termios_struct_h.termios) return int  -- termios.h:66
   with Import => True, 
        Convention => C, 
        External_Name => "tcgetattr";

   function tcsetattr
     (uu_fd : int;
      uu_optional_actions : int;
      uu_termios_p : access constant bits_termios_struct_h.termios) return int  -- termios.h:70
   with Import => True, 
        Convention => C, 
        External_Name => "tcsetattr";

   procedure cfmakeraw (uu_termios_p : access bits_termios_struct_h.termios)  -- termios.h:76
   with Import => True, 
        Convention => C, 
        External_Name => "cfmakeraw";

   function tcsendbreak (uu_fd : int; uu_duration : int) return int  -- termios.h:80
   with Import => True, 
        Convention => C, 
        External_Name => "tcsendbreak";

   function tcdrain (uu_fd : int) return int  -- termios.h:86
   with Import => True, 
        Convention => C, 
        External_Name => "tcdrain";

   function tcflush (uu_fd : int; uu_queue_selector : int) return int  -- termios.h:90
   with Import => True, 
        Convention => C, 
        External_Name => "tcflush";

   function tcflow (uu_fd : int; uu_action : int) return int  -- termios.h:94
   with Import => True, 
        Convention => C, 
        External_Name => "tcflow";

   function tcgetsid (uu_fd : int) return bits_types_h.uu_pid_t  -- termios.h:99
   with Import => True, 
        Convention => C, 
        External_Name => "tcgetsid";

end termios_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
