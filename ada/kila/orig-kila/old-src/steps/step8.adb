with Ada.Text_IO;
with Ada.Characters.Handling; --Is_Control
-- with Ada.Characters.Conversions;

-- Step 8

-- Character'Pos(c) -> ASCII.no of char in integer
-- Integer'Image(i) -> String of number

procedure Kila is
   c : Character := 'a';
begin
   while c /= 'q' loop
       if Ada.Characters.Handling.Is_Control(c) then
          Ada.Text_IO.Put_Line( Integer'Image(Character'Pos(c)) ); -- Char -> Asci.No -> String
       else
          Ada.Text_IO.Put_Line( Integer'Image(Character'Pos(c)) & ' ' & Character'Image(C) );
       end if;
       Ada.Text_IO.Get_Immediate(c);
   end loop;
end Kila;
