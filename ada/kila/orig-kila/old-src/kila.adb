with Ada.Text_IO;
with Ada.Characters.Handling; --Is_Control
with trendy_terminal.linux;
-- with Ada.Characters.Conversions;

-- Step 9

-- Character'Pos(c) -> ASCII.no of char in integer
-- Integer'Image(i) -> number i to string

procedure Kila is
   c : Character := 'a';

   procedure Enable_Raw_Mode is
       fd : Trendy_Terminal.Linux.FD;
       Raw : access Trendy_Terminal.Linux.Termios;
       Ok : Trendy_Terminal.Linux.BOOL;
   begin
      fd := Trendy_Terminal.Linux.Fileno(Trendy_Terminal.Linux.Stdin);
      Ok := Trendy_Terminal.Linux.Tcgetattr(Fd, Raw.all'Address);
   end;

-- der parameter access type ist vielleciht nicht richtig

begin
   Enable_Raw_Mode;

   while c /= 'q' loop
       if Ada.Characters.Handling.Is_Control(c) then
          Ada.Text_IO.Put_Line( Integer'Image(Character'Pos(c)) ); -- Char -> Asci.No -> String
       else
          Ada.Text_IO.Put_Line( Integer'Image(Character'Pos(c)) & ' ' & Character'Image(C) );
       end if;
       Ada.Text_IO.Get_Immediate(c);
   end loop;
end Kila;
