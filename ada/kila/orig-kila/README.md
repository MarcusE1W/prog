# generate ada-module from termios.h

- check where termios.h is: `whereis termio.h`
- create folder for translation: `mkdir lib && cd lib`
- copy termios.h: `cp /path-to/termios.h .`
- create subfolder for includes in termios.h: `mkdir bits`
- copy includes over as well: `cp /path-to/bits/* bist`
- translate termios.h and related includes: `gcc -c -fdump-ada-spec termios.h`

this should give `bits_termios_h.ads  bits_termios_struct_h.ads  bits_types_h.ads  termios_h.ads` in your `lib` folder.


# trendy_terminal (don't work)

## build library

```
gprbuild -P trendy_terminal.gpr -XTrendy_Terminal_Platform=linux
```



## build example

```
cd example
gprbuild -P trendy_terminal_example.gpr -XTrendy_Terminal_Platform=linux
```

## links

termios struct
https://www.mkssoftware.com/docs/man5/struct_termios.5.asp

tcsetattr
https://linux.die.net/man/3/tcsetattr
