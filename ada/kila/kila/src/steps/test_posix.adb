with Ada.Text_IO;  use Ada.Text_IO;
with Interfaces.C; use Interfaces.C;
with System;
with Posix;

procedure Read_Character is
   STDIN_FILENO : constant int := 0;
   Char         : Character;
   -- Result       : ssize_t;
   Result       : Natural;
begin
   Result := Posix.read (STDIN_FILENO, Char'Address, 1);

   if Result = 1 then
      Put_Line ("Character read: " & Char);
   else
      Put_Line ("Failed to read character");
   end if;
end Read_Character;
