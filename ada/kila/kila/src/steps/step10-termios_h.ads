with Interfaces.C; use Interfaces.C;
with System;

-- partially based on trendy_terminal package
	 	 
package Termios_H is
   STDIN_FILENO : constant int := 0;
   TCSANOW : constant int := 0;
   TCSAFLUSH: constant int := 2;
   TCSADRAIN: constant int := 1;	


   function read
     (fd : int; buf : System.Address; count : Natural) return Natural;
   pragma Import (C, read, "read");  -- change to with

   type tcflag_t is new unsigned;
   type cc_t is new unsigned_char;

   -- Flags
   -- ECHO_F : constant tcflag_t := 8#000_0010#;

    --!pp off
    type c_lflag_bit is (ISIG,
                       ICANON,
                       XCASE,
                       ECHO,
                       ECHOE,
                       ECHOK,
                       ECHONL,
                       NOFLSH,
                       TOSTOP,
                       ECHOCTL,
                       ECHOPRT,
                       ECHOKE,
                       FLUSHO,
                       PENDIN,
                       IEXTEN,
                       EXTPROC)
       with Size => tcflag_t'Size;

    for c_lflag_bit use
      (ISIG    => 8#0000001#,
       ICANON  => 8#0000002#,
       XCASE   => 8#0000004#,
       ECHO    => 8#0000010#,
       ECHOE   => 8#0000020#,
       ECHOK   => 8#0000040#,
       ECHONL  => 8#0000100#,
       NOFLSH  => 8#0000200#,
       TOSTOP  => 8#0000400#,
       ECHOCTL => 8#0001000#,
       ECHOPRT => 8#0002000#,
       ECHOKE  => 8#0004000#,
       FLUSHO  => 8#0010000#,
       PENDIN  => 8#0040000#,
       IEXTEN  => 8#0100000#,
       EXTPROC => 8#0200000#
      );

type c_iflag_bit is (
        IGNBRK,   -- Ignore break condition.  
        BRKINT,   -- Signal interrupt on break.  
        IGNPAR,   -- Ignore characters with parity errors.  
        PARMRK,   -- Mark parity and framing errors.  
        INPCK,	   -- Enable input parity check.  
        ISTRIP,  	  -- Strip 8th bit off characters.  
        INLCR,		  	  -- Map NL to CR on input.  
        IGNCR,	  	  -- Ignore CR.  
        ICRNL,	  	  -- Map CR to NL on input.  
        IUCLC,	  	  -- Map uppercase characters to lowercase on input (not in POSIX).  
        IXON,	  	  -- Enable start/stop output control.  
        IXANY,	  	  -- Enable any character to restart output.  
        IXOFF,	  	  -- Enable start/stop input control.  
        IMAXBEL,   -- Ring bell when input queue is full (not in POSIX).  
        IUTF8)    -- Input is UTF8 (not in POSIX).  
       with Size => tcflag_t'Size;
 
 for c_iflag_bit use (
        IGNBRK	=>	  8#0000001#,
        BRKINT	=>	  8#0000002#,
        IGNPAR	=>	  8#0000004#,
        PARMRK	=>	  8#0000010#,
        INPCK	=>	  8#0000020#,
        ISTRIP	=>	  8#0000040#,
        INLCR	=>	  8#0000100#,
        IGNCR	=>	  8#0000200#,
        ICRNL	=>	  8#0000400#,
        IUCLC	=>	  8#0001000#,
        IXON	=>	  8#0002000#,
        IXANY	=>	  8#0004000#,
        IXOFF	=>	  8#0010000#,
        IMAXBEL =>	  8#0020000#,
        IUTF8 =>	  8#0040000# 
 );	     


	  
    --!pp on

    pragma Warnings (Off, "bits of *unused"); -- to turn off the warning that the last 16 bits are unused
    type c_lflag_t is array (c_lflag_bit) of Boolean with
        Pack,
        Size => tcflag_t'Size;

	type c_iflag_t is array (c_iflag_bit) of Boolean with
         Pack,
         Size => tcflag_t'Size;
    pragma Warnings (On, "bits of *unused");

   
   type cc_array is array (0 .. 31) of cc_t with Convention => C;
   type termios is record
      c_iflag : c_iflag_t;  -- input flag
      c_oflag : tcflag_t;  -- output flag
      c_cflag : tcflag_t;  -- control flag
      c_lflag : c_lflag_t;  -- new l_flags ; local flags (mixed flags)
      c_cc    : cc_array;
   end record;
   pragma Convention (C_Pass_By_Copy, termios);

   function tcgetattr (fd : int; termios_p : access termios) return int;
   pragma Import (C, tcgetattr, "tcgetattr");

   function tcsetattr(fd : int; optional_actions : int; termios_p : access constant termios)
      return int;
   pragma Import (C, tcsetattr, "tcsetattr");

end Termios_H;
