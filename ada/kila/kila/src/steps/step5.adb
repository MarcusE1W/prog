	with Ada.Text_IO; use Ada.Text_IO;
with Posix; use Posix;
with Interfaces.C; use Interfaces.C;
-- Step 5
procedure Kila is

   -- package InC renames Interfaces.C;
   procedure enableRawMode is

      Term   : aliased Posix.termios;
      Result : int;

   begin

      Result := Posix.tcgetattr (Posix.STDIN_FILENO, Term'Access);

      if Result /= 0 then
         Put_Line ("get failed");
         return;
      end if;

      -- toggle ECHO
      Term.c_lflag := Term.c_lflag xor Posix.ECHO;

      Result :=
        Posix.tcsetattr (Posix.STDIN_FILENO, Posix.TCSANOW, Term'Access);

      if Result /= 0 then
         Put_Line ("set failed");
         return;
      end if;

      Put_Line ("Set done");
   end enableRawMode;

   c : Character := 'a';
begin
   Put_Line ("Let's Start");
   enableRawMode;
   while (Posix.Read (Posix.STDIN_FILENO, c'Address, 1) = 1 and c /= 'q') loop
      null;
   end loop;
   Put_Line ("The End");
end Kila;
	
