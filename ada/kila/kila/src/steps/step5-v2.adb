	with Ada.Text_IO; use Ada.Text_IO;
-- with Trendy_Terminal_Linux;
-- with Trendy_Terminal_Kila;
-- with Interfaces.C.Strings;
with Posix; use Posix;
with Interfaces.C; use Interfaces.C;
-- Step 5
procedure Kila is

   procedure enableRawMode is

      Raw   : aliased Posix.termios;
      Result : int;

   begin

      Result := Posix.tcgetattr (Posix.STDIN_FILENO, Raw'Access);
	  -- Put_Line ("Status vor: " & Term.c_lflag (ECHO)'Image);
      -- Term.c_lflag (ECHO) := False;

      Raw.c_lflag (ECHO) := not Raw.c_lflag (ECHO);  -- flip the flag

        	  -- Put_Line ("Status nach: " & Term.c_lflag (ECHO)'Image);
	  
      Result := Posix.tcsetattr (Posix.STDIN_FILENO, Posix.TCSANOW, Raw'Access);

   end enableRawMode;

   c : Character := 'a';
begin
   Put_Line ("Let's Start");
   enableRawMode;
   while (Posix.Read (Posix.STDIN_FILENO, c'Address, 1) = 1 and c /= 'q') loop
      null;
   end loop;
   Put_Line ("The End");
end Kila;
	
