with Ada.Text_IO; use Ada.Text_IO;
with Posix; use Posix; -- here for operators, still use Posix. in Code
with Interfaces.C; use Interfaces.C;

	 
-- Step 5
procedure Kila is

   orig_termios: aliased Posix.termios;
   
   procedure disableRawMode is
      Result : int;
   begin
      Result := Posix.tcsetattr (Posix.STDIN_FILENO, Posix.TCSANOW, orig_termios'Access);
   end disableRawMode;
		  
   procedure enableRawMode is
      Raw   : aliased Posix.termios;
      Result : int;
   begin
      Result := Posix.tcgetattr (Posix.STDIN_FILENO, orig_termios'Access);

	  Raw := orig_termios;

	  Raw.c_lflag (ECHO) := not Raw.c_lflag (ECHO);  -- flip the flag

	  Result := Posix.tcsetattr (Posix.STDIN_FILENO, Posix.TCSANOW, Raw'Access);
   end enableRawMode;



   
   c : Character := 'a';
begin
   Put_Line ("Let's Start");
   enableRawMode;

   while (Posix.Read (Posix.STDIN_FILENO, c'Address, 1) = 1 and c /= 'q') loop
      null;
   end loop;

   disableRawMode;			
   Put_Line ("The End");
end Kila;
	
