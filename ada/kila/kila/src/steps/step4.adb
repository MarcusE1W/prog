	with Ada.Text_IO; use Ada.Text_IO;
	-- with Trendy_Terminal_Linux;
	-- with Trendy_Terminal_Kila;
	-- with Interfaces.C.Strings;
	with Posix;
	
	-- Step 4
	procedure Kila is
	c          : Character := 'a';
	begin
	Put_Line ("Let's Start");
	while( Posix.Read( Posix.STDIN_FILENO, c'Address, 1) = 1 and c /= 'q' ) loop Null; End loop;
	Put_Line ("The End");
	end Kila;
	
