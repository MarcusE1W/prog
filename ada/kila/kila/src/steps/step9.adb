with Ada.Text_IO; use Ada.Text_IO;
with Posix; use Posix; -- here for operators, still use Posix. in Code
with Interfaces.C; use Interfaces.C;
with Ada.Characters.Handling;
	 
-- Step 9
procedure Kila is

   orig_termios: aliased Posix.termios;  -- save the original terminal state here
   
   procedure disableRawMode is
      Result : Interfaces.C.int;
   begin
      Result := Posix.tcsetattr (Posix.STDIN_FILENO, Posix.TCSANOW, orig_termios'Access);
   end disableRawMode;
		  
   procedure enableRawMode is
      Raw   : aliased Posix.termios;
      Result : int;
   begin
      Result := Posix.tcgetattr (Posix.STDIN_FILENO, orig_termios'Access); -- get terminal state and save it as original
      -- not atexit solution for Ada found
	  
	  Raw := orig_termios; -- create a copy of the original terminal state
	  Raw.c_lflag (ECHO) := not Raw.c_lflag (ECHO);  -- flip the ECHO flag
	  Raw.c_lflag (ICANON) := not Raw.c_lflag (ICANON);  -- flip the ICANON flag
	  Raw.c_lflag (ISIG) := not Raw.c_lflag (ISIG);  -- flip the ISIG flag

	  Result := Posix.tcsetattr (Posix.STDIN_FILENO, Posix.TCSANOW, Raw'Access); -- set terminal state based on Raw
   end enableRawMode;
   
   C : Character := '-';
begin
   Put_Line ("Let's Start");
   enableRawMode;

   while (Posix.Read (Posix.STDIN_FILENO, C'Address, 1) = 1 and C /= 'q') loop
      if Ada.Characters.Handling.Is_Control(C) then
	  	 Put_Line( C'Image );
	  else
		 Put(Character'Pos(C)'Image);
		 Put(" ");	
		 Put( C );
		 New_line;	
	  end if;
   end loop;

   disableRawMode;	-- no solution for atexit()
   Put_Line ("The End");
end Kila;
	
