with Interfaces.C; use Interfaces.C;
with System;

package Posix is
   STDIN_FILENO : constant int := 0;
   TCSANOW : constant int := 0;


   function read
     (fd : int; buf : System.Address; count : Natural) return Natural;
   pragma Import (C, read, "read");  -- change to with

   type tcflag_t is new unsigned;
   type cc_t is new unsigned_char;

   -- Flags
   ECHO : constant tcflag_t := 8#000_0010#;

    pragma Warnings (Off, "bits of *unused"); -- to turn off the warning that the last 16 bits are unused
    type c_lflag_t is array (c_lflag_bit) of Boolean with
        Pack,
        Size => tcflag_t'Size;
    pragma Warnings (On, "bits of *unused");

   
   type cc_array is array (0 .. 31) of cc_t with Convention => C;
   type termios is record
      c_iflag : tcflag_t;
      c_oflag : tcflag_t;
      c_cflag : tcflag_t;
      c_lflag : tcflag_t;
      c_cc    : cc_array;
   end record;
   pragma Convention (C_Pass_By_Copy, termios);

   function tcgetattr (fd : int; termios_p : access termios) return int;
   pragma Import (C, tcgetattr, "tcgetattr");

   function tcsetattr(fd : int; optional_actions : int; termios_p : access constant termios)
      return int;
   pragma Import (C, tcsetattr, "tcsetattr");

end Posix;
