with Ada.Text_IO; use Ada.Text_IO;
with Termios_H; use Termios_H; -- here for operators, still use Termios_H. in Code
with Interfaces.C; use Interfaces.C;
with Ada.Characters.Handling;
	 
-- Step 9
procedure Kila is

   orig_termios: aliased Termios_H.termios;  -- save the original terminal state here
   
   procedure disableRawMode is
      Result : Interfaces.C.int;
   begin
      Result := Termios_H.tcsetattr (Termios_H.STDIN_FILENO, Termios_H.TCSANOW, orig_termios'Access);
   end disableRawMode;
		  
   procedure enableRawMode is
      Raw   : aliased Termios_H.termios;
      Result : int;
   begin
      Result := Termios_H.tcgetattr (Termios_H.STDIN_FILENO, orig_termios'Access); -- get terminal state and save it as original
      -- not atexit solution for Ada found
	  
	  Raw := orig_termios; -- create a copy of the original terminal state
	  -- flip l_flags
	  Raw.c_lflag (ECHO) := not Raw.c_lflag (ECHO);  
	  Raw.c_lflag (ICANON) := not Raw.c_lflag (ICANON); 
	  Raw.c_lflag (ISIG) := not Raw.c_lflag (ISIG);  

	  -- flip i_flags
	  Raw.c_iflag (IXON) := not Raw.c_iflag (IXON);  

	  Result := Termios_H.tcsetattr (Termios_H.STDIN_FILENO, Termios_H.TCSANOW, Raw'Access); -- set terminal state based on Raw
   end enableRawMode;
   
   C : Character := '-';
begin
   Put_Line ("Let's Start");
   enableRawMode;

   while (Termios_H.Read (Termios_H.STDIN_FILENO, C'Address, 1) = 1 and C /= 'q') loop
      if Ada.Characters.Handling.Is_Control(C) then
	  	 Put_Line( C'Image );
	  else
		 Put(Character'Pos(C)'Image);
		 Put(" ");	
		 Put( C );
		 New_line;	
	  end if;
   end loop;

   disableRawMode;	-- no solution for atexit()
   Put_Line ("The End");
end Kila;
	
