with LV;
with LV.Font;
with LV.Theme;
with LV.Tasks;
with LV.HAL.Tick;

-- with Test_Theme_1;
with Disaply_Simulator;

with Lv.Objx; use Lv.Objx;
with Lv.Objx.Cont;
with Lv.Objx.Line;
with Lv.Area;
with Lv.Objx.Page;

procedure Main is

Points : aliased constant Lv.Area.Point_Array := ((10, 10), (150, 150))
       with Convention => C;

  Scr  : Cont.Instance;
   H   : Cont.Instance;
   L   : Line.Instance;

begin
   LV.Init;

   Disaply_Simulator.Initialize;

         Scr := Cont.Create (No_Obj, No_Obj);
         Scr_Load (Scr);

   -- Test_Theme_1.Init;
   		-- Page.Set_Scrl_Layout (Parent, Cont.Layout_Pretty);
   		-- H := Cont.Create (Parent, H);
 		L := Line.Create (Scr, No_Obj);
      	Line.Set_Points (L, Points'Access, 2);
       
   loop
      LV.Tasks.Handler;
      delay 0.005;
      lV.HAL.Tick.Inc (5);
   end loop;
end Main;
