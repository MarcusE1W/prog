package LV_Conf is

   LV_DPI     : constant := 100;
   LV_HOR_RES : constant := 240;
   LV_VER_RES : constant := 240;

end LV_Conf;
