with Ada.Text_IO;
with SDL.Log;
with SDL.Video.Palettes;
with SDL.Video.Rectangles;
with SDL.Video.Renderers;
with SDL.Video.Renderers.Makers;
with SDL.Video.Windows.Makers;


procedure Mandelsdl is
	window : SDL.Video.Windows.Window;
	Pixel : SDL.Video.Rectangles.Point := (X => 200, Y => 200);
	Colour : SDL.Video.Palettes.Colour := (Red => 100, Green => 150, Blue => 200, Alpha => 200);
	MyRenderer : SDL.Video.Renderers.Renderer;
  wait: Character;
begin
   -- turn on debugging to send messages to the console
   SDL.Log.Set (Category => SDL.Log.Application, Priority => SDL.Log.Debug);

   -- init Window
   if SDL.Initialise (Flags => SDL.Enable_Screen) = True then
      SDL.Video.Windows.Makers.Create (
      			Win    => window,
                Title  => "Point test",
								X      => 100,
								Y      => 100,
                Width  => 600,
                Height => 400,
                Flags  => SDL.Video.Windows.Resizable);

		-- create renderer
		SDL.Video.Renderers.Makers.Create (MyRenderer, window);

		-- draw one point
		SDL.Video.Renderers.Set_Draw_Colour( MyRenderer, Colour );
		SDL.Video.Renderers.Draw( MyRenderer, Pixel);

		-- update renderer and window with latest changes, in this case show point
		SDL.Video.Renderers.Present( MyRenderer);

		-- wait for key press in terminal
		SDL.Log.Put_Debug ("Press any key here in the terminal to exit");
		Ada.Text_IO.Get_Immediate(wait);

		-- clean up window
		window.Finalize;
		SDL.Finalise;

   end if;
end Mandelsdl;
