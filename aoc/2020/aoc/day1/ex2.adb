with Ada.Text_IO; use Ada.Text_IO;

with Ada.Integer_Text_IO;

procedure Expense is
	type Dattype is Array (1..200) of Integer; 

    dat : DatType; 


function ReadFile return DatType is
	file         : File_Type;
    file_Name : constant String := "dat.dat";
    index : Integer := 1;
    data : DatType;

begin
	Open (file, In_File, file_Name);

	while not End_Of_File (file) loop
	       Ada.Integer_Text_IO.Get(file, data(index) );
	       index := index + 1;
	end loop;

	Close (File);
	return data;
end ReadFile;



begin

	dat := ReadFile;	

	New_Line;
	
	for element of dat loop
		for other of dat loop
			for more of dat loop

				if element + other + more  = 2020 then 
					Put(" element: ");
					Ada.Integer_Text_IO.Put( element );
					New_Line;

					Put(" other: ");
					Ada.Integer_Text_IO.Put( other );
					New_Line;
					
					Put(" more: ");
					Ada.Integer_Text_IO.Put( more );
					New_Line;

					Put(" mult ");
					Ada.Integer_Text_IO.Put( element * other * more );
					New_Line;
				end if;

			end loop; -- more
		end loop; -- other
	end loop; -- element
	
	New_Line;
end Expense;

