with Ada.Text_IO; use Ada.Text_IO;
with Ada.Strings; use Ada.Strings;
with Ada.Strings.Unbounded;
with Ada.Strings.Bounded;
with Ada.Integer_Text_IO;
with Ada.Sequential_IO;
with Ada.Sequential_IO;
with Ada.Streams.Stream_IO;

procedure tst is

	package IIO renames Ada.Integer_Text_IO;
	package U_Str renames Ada.Strings.Unbounded;
	package B_Str is new Ada.Strings.Bounded.Generic_Bounded_Length (Max => 50);
	package St_IO renames Ada.Streams.Stream_IO;

	Type PolicyType is record
		min : Integer;
		dummy1 : Character;
		max: Integer;
		dummy2 : Character;
		testchar : Character;
		dummy3 : Character;
		dummy4 : Character;
		--passwd : U_Str.Unbounded_String;
		passwd : B_Str.Bounded_String;
		--passwd : String(1..50);
	end record;

	package Seq_IO is new Ada.Sequential_IO (PolicyType);
	
	-- file         : Seq_IO.File_Type;
	file         : St_IO.File_Type;
    file_Name : constant String := "data.dat";
	policy : PolicyType;

begin

	St_IO.Open (file, St_IO.In_File, file_Name);
	St_Stream (policy);

	while not St_IO.End_Of_File (file) loop

		
		PolicyType'Read( file, policy);
		
			-- print data
			IIO.Put(policy.min, 4);
			IIO.Put(policy.max, 4);
			Put (" " & policy.testchar & " ");
			Put_Line( B_Str.To_String( policy.passwd) ) ;

	    
	end loop;

	St_IO.Close (file);

	New_Line;

end tst;

