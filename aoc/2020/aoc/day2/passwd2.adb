with Ada.Text_IO; use Ada.Text_IO;

with Ada.Integer_Text_IO;


procedure Passwd2 is


	package IIO renames Ada.Integer_Text_IO;
	
	file         : File_Type;
    file_Name : constant String := "data.dat";
	min, max: Integer;
	dummy: Character;
	testchar: character;
	result: Integer := 0;
	match1: Boolean;
	match2: Boolean;

begin

	Open (file, In_File, file_Name);

	while not End_Of_File (file) loop

		IIO.Get(file, min);
	    Get(file, dummy); -- '-'
		IIO.Get(file, max);
	    Get(file, dummy); -- space
	    Get(file, testchar);
	    Get(file, dummy); -- ':'
	    Get(file, dummy); -- space

		
		declare
			passwd : String := Get_Line(file);
		begin
			-- print data
			IIO.Put(min, 4);
			IIO.Put(max, 4);
			Put (" " & testchar & " ");
			Put(passwd & " -- ");

			match1 := ( passwd(min) = testchar);
			match2 := ( passwd(max) = testchar);
			
			-- check only one match
			If (match1 Xor match2 ) then
				result := result + 1;
				Put_Line("VALID");
			else
				Put_Line( "not valid");
			end if;
			
		end;
	    
	end loop;

	Close (File);

	New_Line;
	Put_Line( "Number of valid passwords for variant 2 : " & Integer'Image(result) );	

end Passwd2;

