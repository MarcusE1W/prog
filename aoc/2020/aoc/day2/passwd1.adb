with Ada.Text_IO; use Ada.Text_IO;

with Ada.Integer_Text_IO;


procedure Passwd1 is


	package IIO renames Ada.Integer_Text_IO;
	
	file         : File_Type;
    file_Name : constant String := "data.dat";
	min, max: Integer;
	dummy: Character;
	testchar: character;
	count: Integer;
	result: Integer := 0;

begin

	Open (file, In_File, file_Name);

	while not End_Of_File (file) loop

		IIO.Get(file, min);
	    Get(file, dummy); -- '-'
		IIO.Get(file, max);
	    Get(file, dummy); -- space
	    Get(file, testchar);
	    Get(file, dummy); -- ':'
	    Get(file, dummy); -- space

		
		declare
			passwd : String := Get_Line(file);
		begin
			-- print data
			IIO.Put(min, 4);
			IIO.Put(max, 4);
			Put (" " & testchar & " ");
			Put(passwd & " -- ");

			-- check how often testchar is in passwd
			count := 0;
			for char of passwd loop
				If char = testchar then
					count := Count + 1;
				end if;
			end loop;

			-- check count against limits
			If count >= min and count <= max then
				result := result + 1;
				Put_Line("VALID");
			else
				Put_Line( "not valid");
			end if;
			
		end;
	    
	end loop;

	Close (File);

	New_Line;
	Put_Line( "Number of valid passwords for variant 1 : " & Integer'Image(result) );	

end Passwd1;

