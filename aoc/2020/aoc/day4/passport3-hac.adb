with Hac_Pack; Use Hac_Pack;

procedure Passport1 is

	file         : File_Type;
    file_Name : constant VString := +"data.dat";
    result : Natural := 0;

    hit : Natural;     

    type EnumType is ( byr, iyr, eyr, hgt, hcl, ecl, pid ); -- minus cid

    type FieldlistType is array( EnumType ) of VString;
    
    -- fieldlist : FieldlistType := ( +"byr:", +"iyr:", +"eyr:", +"hgt:", +"hcl:", +"ecl:", +"pid:" );
    fieldlist : FieldlistType;


	
	-- type fieldmaskType is array (EnumType) of Boolean;

    fieldmask : Natural := 0;
    allmost_correct : constant Natural := 7; -- all found

    passport : Natural := 0; 

	line: VString;
begin
	Open (file, file_name);

	fieldlist(byr) := +"byr:";
	fieldlist(iyr) := +"iyr:";
	fieldlist(eyr) := +"eyr:";
	fieldlist(hgt) := +"hgt:";
	fieldlist(hcl) := +"hcl:";
	fieldlist(ecl) := +"ecl:";
	fieldlist(pid) := +"pid:";
	


	while not End_Of_File (file) loop
	    	Get_line(file, line);



			-- if empty line summ up, otherwise keep checkinng
			if line = "" then
				passport := passport + 1;
		    	if fieldmask = allmost_correct then 
		    		result := result + 1;
		    		Put_Line (" Hurray");
					New_Line;
				else
		    		Put_Line (" Booohh");
					New_Line;
		    	end if;

				-- for field in fieldlist'first..fieldlist'last loop Put (field); end loop;
		    	fieldmask := 0;
			else
				Put_Line(  line );
			
				for element in byr..pid loop
					       	
					hit := Index ( line,  fieldlist( element) );

					if hit > 0 then 
						fieldmask := fieldmask + 1;
			       	end if; -- hit
			       	Put(fieldmask, 3);
			    end loop; -- element
			 end if; --  line
	    New_Line;
	end loop; -- while

	-- collect last passport
	passport := passport + 1;
   	if fieldmask = allmost_correct then 
   		result := result + 1;
   		Put_Line (" Hurray");
		New_Line;
	else
   		Put_Line (" Booohh");
		New_Line;
   	end if;


	Close (file);

	Put( "Answer: "); Put(result); Put(" of "); Put(passport);
	New_line;

end Passport1;

