with Ada.Text_IO; use Ada.Text_IO;

with Ada.Integer_Text_IO;

with Ada.Strings;
with Ada.Strings.Fixed;

with Ada.Strings.Maps;

procedure Passport1 is

package IIO renames Ada.Integer_Text_IO;
	 
	file         : File_Type;
    file_Name : constant String := "data.dat";
    result : Natural := 0;

    hit : Natural;     

    type EnumType is ( byr, iyr, eyr, hgt, hcl, ecl, pid ); -- minus cid
    type FieldString is array (EnumType) of String(1..4);

    fieldlist : constant FieldString := ( "byr:", "iyr:", "eyr:", "hgt:", "hcl:", "ecl:", "pid:"); -- minus "cid:"


	type fieldmaskType is array (EnumType) of Boolean;

    fieldmask : fieldmaskType := ( others => false );
    allmost_correct : constant fieldmaskType := ( others => true ); -- all found

    passport : Natural := 0;


begin
	Open (file, In_File, file_Name);

	while not End_Of_File (file) loop
		declare
	    	line : String := Get_line(file);
	    begin



			-- if empty line summ up, otherwise keep checkinng
			if line = "" then
				passport := passport + 1;
		    	if fieldmask = allmost_correct then 
		    		result := result + 1;
		    		Put_Line (" Hurray");
					New_Line;
				else
		    		Put_Line (" Booohh");
					New_Line;
		    	end if;

				for field of fieldlist loop Put (field); end loop;
		    	fieldmask := ( others => false);
			else
				Put_Line(  line );
			
				for element in byr..pid loop --minus cid
					       	
					hit := Ada.Strings.Fixed.Index
					        (Source  => line,
					         Pattern => fieldlist( element ),
					         From    => 1);


					if hit > 0 then 
						fieldmask( element ) := true;
			       	end if; -- hit
			       	Put( " " & Boolean'Image ( fieldmask(element) )   );
			    end loop; -- element
			 end if; --  line
	    end; -- declare
	    New_Line;
	end loop; -- while

	Close (file);

	Put_Line( "Answer: " & Natural'Image(result) & " of " & Natural'Image(passport) );

end Passport1;

