with Hac_Pack; Use Hac_Pack;

procedure seat1 is
		file : File_Type;
    file_Name : constant VString := +"data.dat";
    line : VString;
    result : Natural := 0;

-- 		maxseats: constant Natural := 127 * 8 + 8;
-- 		maxseats: constant Integer := 1024;
-- 		maxseats: constant := 127 * 8 + 8;
		maxseats: constant := 1024;
		subtype SeatRange is range 0 .. maxseats;

--     type SeatPlanType is array ( 0 .. maxseats ) of Boolean;
--     type SeatPlanType is array ( 0 .. 1024 ) of Boolean;

begin
	Open (file, file_name);

	while not End_Of_File (file) loop
	    	Get_line(file, line);

	end loop; -- while

	Close (file);
end seat1;
