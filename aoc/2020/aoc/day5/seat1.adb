with Ada.Text_IO; use Ada.Text_IO;

procedure Seat1 is


		file : File_Type;
    file_Name : constant String := "data.dat";
    seat : Natural;
    result : Natural := 0;

		function ReadLine return Natural is

				subtype RowCodeRange is Natural range 1..7; -- first 7 Char of the seat number code
				subtype SeatCodeRange is Natural range 1..3; -- last 3 Char of the seat number code
				rowcode : Character;
				seatcode : Character;
		begin
				declare
			      line : String := Get_line(file);
			  begin
						for code in RowCodeRange loop
 					 		rowcode := line(code);
-- 								rowcode := 'C';
						end loop;
				end;
				return 12; --TODO
		end ReadLine;


begin
	Open (file, In_File, file_Name);

	while not End_Of_File (file) loop
			seat := ReadLine;
			if seat > result then
					result := seat;
			end if;
	end loop;

	Close (File);

end Seat1;
