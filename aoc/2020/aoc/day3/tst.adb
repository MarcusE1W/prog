with Ada.Text_IO; use Ada.Text_IO;

with Ada.Integer_Text_IO;

procedure Tst is

package IIO renames Ada.Integer_Text_IO;
	 
	file         : File_Type;
    file_Name : constant String := "data.dat";
    pos :  Integer := 1;
    result : Integer := 0;

begin
	Open (file, In_File, file_Name);

	declare
		line_ignore : String := Get_line(file);
	begin
		Put_Line( line_ignore);
		while not End_Of_File (file) loop
		       declare
		       	  line : String := Get_line(file);
		       begin

				  pos := pos + 3;
				  
				  if pos > line'last then
				  	 pos := pos - line'last;
				  end if;

				  if line(pos) = '#' then					  
				  		result := result + 1;
				  		line(pos) := 'O';
				  	 else
				  		line(pos) := 'X';					  	 
				  end if;
					  
		       	  Put_Line( line );
		       	  declare
		       	  	nix : String := Get_line(file);
		       	  begin
		       	  	null;
		       	  end;
		       	  	
		       end; -- declare
		end loop;

	end; --declare
	Close (file);

	Put_Line( "Answer: " & Integer'Image(result) );

end Tst;

