with Ada.Text_IO; use Ada.Text_IO;

with Ada.Integer_Text_IO;

procedure Slope2 is

package IIO renames Ada.Integer_Text_IO;
	 
	file         : File_Type;
    file_Name : constant String := "data.dat";
    steps : constant Array (1..5) of Integer := (1,3,5,7,1);
    pos : Array (1..5) of Integer := (others => 1);
    result : Array (1..5) of Integer := (others => 0);
    answer : Long_Integer := 1;

begin
	Open (file, In_File, file_Name);

	declare
		line_ignore : String := Get_line(file);
		flag : Boolean := true;
	begin
		Put_Line( line_ignore);
		while not End_Of_File (file) loop
		       declare
		       	  line : String := Get_line(file);
		       	  map : String := line;

		       begin


				  -- check all slopes
				  for i in pos'range loop

					if Not(i = 5 and flag) then -- special last case
					
					  pos(i) := pos(i) + steps(i);
					  
					  if pos(i) > line'last then
					  	 pos(i) := pos(i) - line'last;
					  end if;

					  if line(pos(i)) = '#' then					  
					  		result(i) := result(i) + 1;
					  end if;
					  
					  map(pos(i)) := Integer'Image(i)(2);--cheating to get the character

					end if;

					flag := Not flag;
					  		
				  end loop;
		       	  
		       	  Put_Line( map );
		       end; -- declare
		end loop;

	end; --declare
	Close (file);

	for element of result loop
		Put_Line( Integer'Image(element) );
		answer := answer * Long_Integer( element );
	end loop;

	Put_Line( "Answer: " & Long_Integer'Image( answer ) );

end Slope2;

