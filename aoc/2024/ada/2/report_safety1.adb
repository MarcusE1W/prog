with Ada.Text_IO; use Ada.Text_IO;
with Ada.Containers.Vectors;
with Ada.Integer_Text_IO;

procedure report_safety1 is

  package I renames Ada.Integer_Text_IO;

   package Integer_Vectors is new
     Ada.Containers.Vectors
       (Index_Type   => Natural,
        Element_Type => Integer);
  
  file         : File_Type;
  file_Name : constant String := "mini.dat";
  -- file_Name : constant String := "full.dat";
  tmp : Integer := 0;
  safe_count := 0; 
begin
	Open (file, In_File, file_Name);

	while not End_Of_File(file) loop
      declare
		line : Integer_Vectors.Vector;
		safe : Boolean := True;	
		tmp,  diff : Integer := 0;	
      begin	  

	    while not End_Of_Line(file) loop
	      I.Get( file, tmp);
		  line.append(tmp);				
	    end loop;

		tmp := line.First_Element;

		diff := line(2) - tmp 
		exit when abs(diff) > 3;	
			
		for index in line.First_Index+1..line.Last_Index-1 loop
		  diff	 
		end loop;
		Put_Line( "Sum: " & Integer'Image(sum) );
		Put_Line( "Sum_abs: " & Integer'Image(sum_abs) );	
	    Skip_Line(file);	-- go to the next line
	    New_Line;
	  end;		
	end loop;
  	Close(file);	
end report_safety1;
