with Ada.Text_IO; use Ada.Text_IO;

with Ada.Integer_Text_IO;

procedure distance2 is

package I renames Ada.Integer_Text_IO;

type ABListType is (list_a,	 list_b);
type ValueArray is array(1..1000) of Integer;
type ListType is array (ABListType) of ValueArray;	
lists : ListType;	   
		  	 
Procedure Read_Data( lists : out ListType;	 maxdata : out Integer) is
	file         : File_Type;
    -- file_Name : constant String := "mini.dat";
    file_Name : constant String := "full.dat";
	index : Integer := 1;	
begin
	Open (file, In_File, file_Name);

	while not End_Of_File (file) loop
			I.Get( file, lists(list_a)(index) );	
			I.Get( file, lists(list_b)(index) );

			I.Put(lists(list_a)(index) );	
			I.Put(lists(list_b)(index) );
			New_Line;
			index := @ + 1;	 			
    end loop;
	Close(file);
	Put_Line( "======================== end load ====" );		 
	maxdata := index -1;					
end; --Read_Data

procedure Swap( x,	y : in out Integer) is
  tmp : Integer := 0;
begin
  tmp := x;
  x := y;
  y := tmp;
end Swap;	
	 
procedure BubbleSort( list : in out ValueArray;  maxdata : Integer ) is
  tmp: Integer := 0;
begin
	for i in 1..maxdata-1 loop
		for j in 1..maxdata-i loop
		   if list(j) > list(j+1) then
		      Swap( list(j), list(j+1) );
		   end if;
		end loop;
    end loop;
end BubbleSort;	

function CalcDistance( lists : in ListType;	 maxdata : in Integer ) return Integer is
  distance : Integer := 0;
begin
  for i in 1..maxdata loop
    distance := @ + abs( lists(list_b)(i) - lists(list_a)(i) );
  end loop;
  return distance;
end CalcDistance;

			 
procedure Printlist( list : in ValueArray; maxdata : in Integer ) is
   last : Integer := 0;
begin
	for i in 1..maxdata loop
	  if list(i) < last then
	    Put( "Upsiee   ");
	  end if;
	  Put_Line( Integer'Image(list(i)) );
	  last := list(i);				   
	end loop;
end Printlist;  

function CalcSimilarities( lists : in ListType; maxdata : in Integer ) return Integer is
  similarity : Integer := 0;
  

  function FindNumber( list : in ValueArray; find: in Integer; maxdata : in Integer ) return Integer is
    sim : Integer := 0;
  begin
    for i in 1..maxdata loop
	  if list(i) = find then
	    sim := @ + find;
	  end if;  
	end loop;
	return sim;	
  end FindNUmber;			
  
begin
  for i in 1..maxdata loop
    similarity := @ + FindNumber( lists(list_b), lists(list_a)(i), maxdata );
  end loop;
  return similarity;	
end CalcSimilarities;		   
		  	
maxdata : Integer := 0;		 	
distance : Integer := 0;		 	
similarity : Integer := 0;		 	
	
begin
	Read_Data( lists, maxdata);
	Put_Line( "Maxdata: " & Integer'Image(maxdata) );	
	-- Quicksort( lists(list_a), lists(list_a)'first, maxdata );	
	-- Quicksort( lists(list_b), lists(list_a)'first, maxdata );
	BubbleSort( lists(list_a), maxdata );
	BubbleSort( lists(list_b), maxdata );
	Put_Line( "List a:");	  
	Printlist( lists(list_a), maxdata );
	Put_Line( "List b:");	 
	Printlist( lists(list_b),	 maxdata );
	distance := CalcDistance( lists, maxdata );
	New_Line;
	Put_Line( "Distance: " & Integer'Image(distance) );
	New_Line;
	similarity := CalcSimilarities( lists, maxdata);
	Put_Line( "Similarity: " & Integer'Image(similarity) );	
end distance2;	
