with Ada.Text_IO; use Ada.Text_IO;

with Ada.Integer_Text_IO;

procedure distance1 is

package I renames Ada.Integer_Text_IO;

type ABListType is (list_a,	 list_b);
type ValueArray is array(1..1000) of Integer;
-- type ValueArray is array(1..10) of Integer;
type ListType is array (ABListType) of ValueArray;	
-- type ListType is array (1..2) of ValueArray;	
-- type ListType is array (ABListType,	 1..1000) of Integer;	
lists : ListType;	   
		  	 
Procedure Read_Data( lists : out ListType;	 maxdata : out Integer) is
	file         : File_Type;
    -- file_Name : constant String := "mini.dat";
    file_Name : constant String := "full.dat";
	index : Integer := 1;	
begin
	Open (file, In_File, file_Name);

	while not End_Of_File (file) loop
			-- I.Get( file, lists(1, index));	
			I.Get( file, lists(list_a)(index) );	
			I.Get( file, lists(list_b)(index) );

			I.Put(lists(list_a)(index) );	
			I.Put(lists(list_b)(index) );
			New_Line;
			index := @ + 1;	 			
    end loop;
	Close(file);
	Put_Line( "======================== end load ====" );		 
	maxdata := index -1;					
end; --Read_Data

procedure Swap( x,	y : in out Integer) is
  tmp : Integer := 0;
begin
  tmp := x;
  x := y;
  y := tmp;
end Swap;	
	 
procedure Quicksort( list : in out ValueArray;  start, stop : Integer ) is  		  
begin
  if stop-start > 1 then
  	 declare
	   pivot : Integer := list(start);
	   left : Integer := start;		   
	   right : Integer := stop;	
	   tmp : Integer := 0;
  	 begin
	   Put_Line( "Start right: " & Integer'Image(right) & " left:" & Integer'Image(left) );
	   while left <= right loop
	   	  while list(left) < pivot loop
		    left := @ +1;
		  end loop;
		  Put_Line( "!before right: " & Integer'Image(right) & "max:" & Integer'Image(list'last) );
		  while list(right) > pivot loop
		    right := @ -1;	
			Put_Line( "!In right: " & Integer'Image(right) & " max:" & Integer'Image(list'last) );		  
		  end loop;
		  if left <= right then
		     tmp := list(left);
			 list(left) := list(right);
			 list(right) := tmp;
			 left := @ +1;
			 right := @ -1;
		  end if; 
	   end loop;
	   Put_Line( "right: " & Integer'Image(right) & " left:" & Integer'Image(left) );
	   Quicksort( list,	 start, right); 
	   Quicksort( list,	 left, stop); 
  	 end;	
  end if;	
end;	

procedure BubbleSort( list : in out ValueArray;  maxdata : Integer ) is
  tmp: Integer := 0;
begin
	for i in 1..maxdata-1 loop
		for j in 1..maxdata-i loop
		   if list(j) > list(j+1) then
		      Swap( list(j), list(j+1) );
		   end if;
		end loop;
    end loop;
end BubbleSort;	

function CalcDistance( lists : in ListType;	 maxdata : in Integer ) return Integer is
  distance : Integer := 0;
begin
  for i in 1..maxdata loop
    distance := @ + abs( lists(list_b)(i) - lists(list_a)(i) );
  end loop;
  return distance;
end CalcDistance;

			
procedure Printlist( list : in ValueArray; maxdata : in Integer ) is
   last : Integer := 0;
begin
	for i in 1..maxdata loop
	  if list(i) < last then
	    Put( "Upsiee   ");
	  end if;
	  Put_Line( Integer'Image(list(i)) );
	  last := list(i);				   
	end loop;
end Printlist;  

maxdata, result : Integer := 0;		 	
	
begin
	Read_Data( lists, maxdata);
	Put_Line( "Maxdata: " & Integer'Image(maxdata) );	
	-- Quicksort( lists(list_a), lists(list_a)'first, maxdata );	
	-- Quicksort( lists(list_b), lists(list_a)'first, maxdata );
	BubbleSort( lists(list_a), maxdata );
	BubbleSort( lists(list_b), maxdata );
	Put_Line( "List a:");	  
	Printlist( lists(list_a), maxdata );
	Put_Line( "List b:");	 
	Printlist( lists(list_b),	 maxdata );
	result := CalcDistance( lists, maxdata );
	New_Line;
	Put_Line( "Result: " & Integer'Image(result) );
end distance1;	
