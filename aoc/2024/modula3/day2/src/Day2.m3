MODULE Day2 EXPORTS Main;
(* Advent of code 2024 Day 2 *)

IMPORT IO, Rd, Fmt;

CONST
  datFile: TEXT = "mini.dat";
  (* datFile: TEXT = "full.dat"; *)

VAR
  file: Rd.T;
  value : INTEGER;
  (* line : TEXT; *)
    
BEGIN
  file := IO.OpenRead( datFile );
  TRY
    
    WHILE NOT IO.EOF(file) DO
      TRY
        value :=IO.GetInt(file);
      EXCEPT
        IO.Error => IO.Put("An I/O error occurred." & " value: " & Fmt.Int(value) & " EOF: " & Fmt.Bool( IO.EOF(file)) & "\n" );
      END;
      (* line := IO.GetLine(file); *)
      IO.PutInt(value);
      IO.Put("EOF: " & Fmt.Bool( IO.EOF(file)  ) & "\n" );
    END; (* While *)
    
  FINALLY  
    Rd.Close(file);
  END;

  IO.Put("\n Ladies and Dudes, the result is: ");
  (* IO.PutInt (result); *)
  IO.Put( "\n" );
END Day2.
