MODULE Day4 EXPORTS Main;
(* Advent of code 2022 Day 4 *)

IMPORT IO, Rd;

CONST
    (* datFile: TEXT = "dat.test"; *)
    datFile: TEXT = "dat.original";

TYPE
  SectionsType = RECORD
    s1x, s1y : INTEGER := 0;
    s2x, s2y: INTEGER := 0;
  END;
    
VAR
    result : INTEGER := 0;
    dummy : CHAR := '-';

PROCEDURE ReadSections( file: Rd.T ) : SectionsType =
  VAR
    sec : SectionsType;
  BEGIN
    sec.s1x :=IO.GetInt(file);
    dummy := IO.GetChar(file);
    IO.PutInt(sec.s1x);
    IO.PutChar( ' ' );

    sec.s1y :=IO.GetInt(file);
    dummy := IO.GetChar(file);
    IO.PutInt(sec.s1y);
    IO.PutChar( ' ' );

    sec.s2x :=IO.GetInt(file);
    dummy := IO.GetChar(file);
    IO.PutInt(sec.s2x);
    IO.PutChar( ' ' );

    sec.s2y :=IO.GetInt(file);
    dummy := IO.GetChar(file);
    IO.PutInt(sec.s2y);
    IO.PutChar( ' ' );
    
    IO.Put( "\n" );

    RETURN sec;
  END ReadSections;

PROCEDURE CheckContains(a,b,x,y : INTEGER) : INTEGER =
  BEGIN
    IF a <= x AND b >= y THEN
      RETURN 1
    END;
    RETURN 0;
  END CheckContains;

PROCEDURE FullyContains( sec : SectionsType) : INTEGER =
  
  VAR
    fullyYes : INTEGER := 0;
  BEGIN
    fullyYes := CheckContains( sec.s1x, sec.s1y, sec.s2x, sec.s2y );
    
    IF fullyYes # 1 THEN
      fullyYes := CheckContains( sec.s2x, sec.s2y, sec.s1x, sec.s1y );
    END;
    RETURN fullyYes;
  END FullyContains;
  

  
PROCEDURE ReadData() : INTEGER =
  VAR inFile: Rd.T;
      sections : SectionsType;
      sum : INTEGER :=0;
BEGIN
  inFile := IO.OpenRead( datFile );
  REPEAT
    sections := ReadSections(inFile);  
    sum := sum + FullyContains(sections);
    IO.PutInt(sum);
    IO.Put("\n");
  UNTIL IO.EOF(inFile);
  Rd.Close(inFile);
  RETURN sum;  
END ReadData;
  
    
BEGIN
    result:=ReadData();

    IO.Put("\n Ladies and Dudes, the result is: ");
    IO.PutInt (result);
    IO.Put( "\n" );
END Day4.
