MODULE Day3 EXPORTS Main;
(* Advent of code 2022 Day 3 *)

IMPORT IO, Rd, Text;

CONST
    datFile: TEXT = "dat.orig";

VAR
    result : INTEGER := 0;


PROCEDURE SharedItem( READONLY text : TEXT ) : CHAR =
  VAR
    len, secondhalf : INTEGER := 0;
    pos : INTEGER := 0;
  BEGIN
    len := Text.Length( text);
    secondhalf := len DIV 2;
    IO.PutInt( len);
    IO.Put( "\n" );
    FOR i := 0 TO secondhalf-1 DO
      pos := Text.FindChar( text, Text.GetChar(text, i), secondhalf );
      IF pos > 0 THEN
        IO.PutChar( Text.GetChar(text, pos) );
        IO.Put( "\n" );
        RETURN Text.GetChar(text, pos);
      END; (* IF *)
    END (* FOR *)
  END SharedItem;

  
PROCEDURE PrioIs( ch :  CHAR ) : INTEGER =
  VAR
    ascii : INTEGER := 0;
  BEGIN
    ascii := ORD( ch );
    IF ascii > 96 THEN (* ascii of 'a' is 97 *)
      RETURN ascii - 97 + 1 ;
    ELSE
      RETURN ascii - 65 + 27 (* ascii of 'A' is 65 *)
    END;
  END PrioIs;

  
PROCEDURE ReadData() : INTEGER =
  VAR inFile: Rd.T;
      text: TEXT;
      shareditem: CHAR;
      prio, sum : INTEGER :=0;
BEGIN
  inFile := IO.OpenRead( datFile );
  REPEAT
    text := IO.GetLine(inFile);
    IO.Put( text );
    IO.Put( "\n" );
    shareditem := SharedItem(text);
    prio := PrioIs(shareditem);
    sum := sum + prio;
  UNTIL IO.EOF(inFile);
  Rd.Close(inFile);
  RETURN sum;  
END ReadData;
  
    
BEGIN
    result:=ReadData();

    IO.Put("\n Ladies and Dudes, the result is: ");
    IO.PutInt (result);
END Day3.
