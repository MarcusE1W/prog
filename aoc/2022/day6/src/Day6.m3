MODULE Day6 EXPORTS Main;
(* Advent of code 2022 Day6 *)

IMPORT IO, Rd, Text, Thread;

CONST
    (* datFile: TEXT = "dat.test4"; *)
    datFile: TEXT = "dat.original";

    
VAR
    result : INTEGER := 0;

PROCEDURE HasDouble( txt: ARRAY [1..4] OF CHAR) : BOOLEAN =
  VAR
    (* i,j : CARDINAL; *)
  BEGIN
  FOR i := 1 TO 4 DO
    FOR j := 1 TO 4 DO
      IF (i # j) AND (txt[i] = txt[j]) THEN
        RETURN TRUE;
      END;
    END;
  END;
  RETURN FALSE;
END HasDouble;
    
PROCEDURE GetMarker( txt : TEXT ) : INTEGER =
  VAR
    mark  : INTEGER := 3;
    arry : ARRAY[1..4] OF CHAR;
    double : BOOLEAN := TRUE;
  BEGIN
    REPEAT
      INC (mark);
      FOR i := 1 TO 4  DO
        arry[i] := Text.GetChar( txt, mark-(4-i)-1);
        (* IO.Put("\narry "); IO.PutInt(i); *)
        (* IO.Put(" =  "); IO.PutChar(arry[i]); *)
      END;
      double := HasDouble( arry );
      IF NOT double THEN
        RETURN mark;
      END;
      
    UNTIL mark = Text.Length(txt)-1;
    IO.Put( "\n Sorry, that was disapointing \n" );
    RETURN 0; (* this should not happen *)
  END GetMarker;

    
PROCEDURE ReadData() : INTEGER =
  VAR inFile: Rd.T;
      marker : INTEGER:=0;
      text : TEXT;
  BEGIN
    TRY
      inFile := IO.OpenRead( datFile );
      REPEAT
        TRY
          text := IO.GetLine(inFile);
        EXCEPT
        | IO.Error => IO.Put("\n GetLine IO.Error Exception, Dude\n" )
        END; (* TRY *)
        IO.Put(text);
        marker := GetMarker( text );
        
      UNTIL IO.EOF(inFile);
      RETURN marker;
    FINALLY
        Rd.Close(inFile);
    END; (* TRY *)
END ReadData;
  
    
BEGIN
    result:=ReadData();

    IO.Put("\n Ladies and Dudes, the result is: ");
    IO.PutInt (result);
    IO.Put( "\n" );
END Day6.
