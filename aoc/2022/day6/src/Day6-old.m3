MODULE Day6 EXPORTS Main;
(* Advent of code 2022 Day6 *)

IMPORT IO, Rd, Text;

CONST
    datFile: TEXT = "dat.test5";
    (* datFile: TEXT = "dat.original"; *)

    
VAR
    result : INTEGER := 0;

PROCEDURE GetMarker( txt : TEXT ) : INTEGER =
  VAR
    mark  : INTEGER := 0;
    i, downto  : INTEGER := 0;
    ch,ich : CHAR := '0';
    buhh : BOOLEAN := FALSE;
  BEGIN
    REPEAT
      INC (mark);
      buhh := FALSE;
      ch := Text.GetChar(txt, mark);
      i := mark-1;
      downto := MAX(0, mark-3);
      IO.Put("\ndownto: "); IO.PutInt(downto);
      IO.Put(" ch: "); IO.PutChar(ch);
      WHILE downto <= i DO
        IO.Put("\ni: "); IO.PutInt(i);
        IO.Put(" mark: "); IO.PutInt(mark);
        ich := Text.GetChar(txt, i);
        IO.Put(" i ch: "); IO.PutChar(ich);
        IF  ich = ch THEN
          IO.Put("\nBuhh"); IO.PutInt(i);
          mark := (4 - (mark - i)) + mark-1;
          i := -10000; (* stop while loop *)
          buhh := TRUE;
        ELSE
          DEC(i);
        END; (* IF *)
      END; (* WHILE*)
      IO.Put("\n\nWhile end");
      IO.Put("\ni: "); IO.PutInt(i);
      IO.Put(" mark: "); IO.PutInt(mark);
      IO.Put("\n");
      IF i+1=downto AND NOT buhh AND mark >= 3 THEN
        RETURN mark+1;
      END (* IF *)
    UNTIL mark = Text.Length(txt)-1;
    IO.Put( "\n Sorry, that was disapointing \n" );
  END GetMarker;

    
PROCEDURE ReadData() : INTEGER =
  VAR inFile: Rd.T;
      marker : INTEGER:=0;
      buffer : ARRAY [1..4] OF CHAR;
      text : TEXT;
BEGIN
  inFile := IO.OpenRead( datFile );
  REPEAT
    text := IO.GetLine(inFile);
    IO.Put(text);
    marker := GetMarker( text );

  UNTIL IO.EOF(inFile);
  Rd.Close(inFile);
  RETURN marker;  
END ReadData;
  
    
BEGIN
    result:=ReadData();

    IO.Put("\n Ladies and Dudes, the result is: ");
    IO.PutInt (result);
    IO.Put( "\n" );
END Day6.
