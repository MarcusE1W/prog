UNSAFE MODULE Milo EXPORTS Main;
(* Modula 3 version of the Kilo editor *)

(* IMPORT IO; *)
IMPORT Term;
(* IMPORT TermIO; *)
(* IMPORT TermC, Termios; *)
IMPORT Rd;
IMPORT Wr;
IMPORT IO;
IMPORT Stdio;
IMPORT ASCII;
IMPORT Word;
IMPORT Text;


EXCEPTION Error (TEXT);

TYPE
  editorConfig = RECORD (* Step 29 *)
    screenrows: INTEGER;
    screencols: INTEGER := 24;
  END;
          
VAR
  exit : BOOLEAN;
  e : editorConfig;

PROCEDURE Die( errorText : TEXT) RAISES {Error} =
  BEGIN
    Wr.PutText(Stdio.stdout, Esc() & "[2J");
    Wr.PutText(Stdio.stdout, Esc() & "[H");

    Term.MakeRaw(FALSE);
    (* RAISE Error( errorText ); *)
  END Die;

PROCEDURE Ctrl_Key(c : CHAR) : CHAR =
  BEGIN
    (* nicked form interface ASCII.m3 *)
    RETURN VAL (Word.And (ORD (c), 31), CHAR);
  END Ctrl_Key;
  

PROCEDURE editorDrawRows() =
  BEGIN
    FOR i:=1 TO e.screencols DO
      Wr.PutText(Stdio.stdout, "~\r\n");
    END;
  END editorDrawRows;

PROCEDURE Esc() : TEXT =
  BEGIN
    RETURN Text.FromChar(ASCII.ESC);
  END Esc;
  
PROCEDURE editorRefreshScreen() =
  BEGIN
    Wr.PutText(Stdio.stdout, Esc() & "[2J");
    Wr.PutText(Stdio.stdout, Esc() & "[H");

    editorDrawRows();
    
    Wr.PutText(Stdio.stdout, Esc() & "[H");
  END editorRefreshScreen;   

PROCEDURE disableRawMode() =
  BEGIN
    Term.MakeRaw(FALSE);
  END disableRawMode;

PROCEDURE enableRawMode() =
  BEGIN
    Term.MakeRaw(TRUE);
  END enableRawMode;
  
PROCEDURE editorReadKey() : CHAR =
  VAR
    c : CHAR;
  BEGIN
    (* c := Term.GetChar(); *)
    c := Rd.GetChar(Stdio.stdin);
    RETURN c;
  END editorReadKey;

PROCEDURE getCursorPosition( VAR rows,cols : INTEGER) : INTEGER =
  VAR
    c : CHAR;
    t : TEXT;
  BEGIN
    Wr.PutText(Stdio.stdout, Esc() & "[6n" );

    Wr.PutText(Stdio.stdout, "\r\n");

    (*   c := Rd.GetChar(Stdio.stdin); *)
    (*   Wr.PutChar(Stdio.stdout, c); *)
    
    c := Rd.GetChar(Stdio.stdin);
    WHILE NOT Rd.EOF(Stdio.stdin) DO
      IF c IN ASCII.Controls THEN
        IO.PutInt( ORD(c));
        Wr.PutText( Stdio.stdout, " : ");
        Wr.PutChar(Stdio.stdout, c);
      ELSE
        Wr.PutChar(Stdio.stdout, c);
      END;
      Wr.PutText(Stdio.stdout, "\r\n");
        
    END;
    rows := 12;
    cols :=12;
    
    RETURN -1;
    
  END getCursorPosition;
  
PROCEDURE getWindowSize( VAR rows, cols: INTEGER) : INTEGER =
  (* Step 30 *)
  BEGIN
    Wr.PutText(Stdio.stdout, Esc() & "[999C" & Esc() & "[999B" );
    RETURN getCursorPosition( rows, cols); 
  END getWindowSize;

  
PROCEDURE editorProcessKeypress() : BOOLEAN=
  VAR c : CHAR;
      exit: BOOLEAN;
  BEGIN
    c := editorReadKey();

    (* CASE c OF *)
    (* |  Ctrl_Key ('q') => exit := TRUE; *)
    (* ELSE *)
    (* END; *)

    IF c = Ctrl_Key('q') THEN exit:= TRUE END;
    RETURN exit;
  END editorProcessKeypress;

PROCEDURE initEditor(VAR e: editorConfig) =
  BEGIN
    IF getWindowSize( e.screenrows, e.screencols) = -1 THEN
      Die("GetWindowSize");
    END;
  END initEditor;
  
BEGIN
  enableRawMode();
  initEditor(e);
  exit := FALSE;

  (* REPEAT *)
  (*   editorRefreshScreen(); *)
  (*   exit := editorProcessKeypress(); *)
  (* UNTIL exit=TRUE ; (\* LOOP *\) *)

  disableRawMode();

  Wr.PutText(Stdio.stdout, "\nThat's it folks\n");
  
END Milo.
