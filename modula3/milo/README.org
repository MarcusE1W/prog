* Library Options
** Utermio
*** This library looks quite similar to termios.h in C.
*** Does not get installed in Linux
*** Several variants on Github but mostly as HTML docu
** Term package
Is for some reasons part of the caltech-parser package/folder on github but gets installed into ~cm3/pkg~
*** Term
Safe Interface to access unsafe interfaces TermC and Termios
*** TermC
Unsafe interface
- TermC.i3
- TermC.h
- TermC.c
*** Termios
Unsafe interface
- Termios.i3
- Termios.c
*** TermIO
Some Object for IO?
* String/TEXT functions
** ORD
i := ORD( c );
** VAL
** NewLine
Print Wr.EOL
Print /r/n
** TEXT concatination
&
Text.Concat
* ASCII
- Find all the ASCII signs there
- Ctrl key masking, not quite sure how, though

* steps
** before
*** Die function
not really working that ways ?
*** MakeRaw
Term.MakeRaw sets pretty much the same flags as kilo, it's a good substitute
*** EditorProcessKeyPress
The CASE function is useless. Seems a sequence of IF clauses is required.
** step26
editorconfig is saved automatically by Term.MakeRaw TODO: double check
no need yet for editorConfig RECORD/struct
** step28
add screenrows, screencols to editorConfig RECORD
add initEditor function
** step29
there is a ioctl function in ~Unix.i3~ but no winsize struct/RECORD as the ways the terminal size is determined changes in step 30 anyway this might not be a big problem.

    
