MODULE VSplit EXPORTS Main;
IMPORT Trestle, TextVBT, BorderedVBT, HVSplit, Axis,
       HVBar, Pixmap;

VAR
  v :=BorderedVBT.New(
          HVSplit.Cons(
              Axis.T.Ver,
              BorderedVBT.New(TextVBT.New("Top")),
              HVBar.New(size := 3.0, txt := Pixmap.Gray),
              BorderedVBT.New(TextVBT.New("Bottom"))),
          size := 3.0,
          txt := Pixmap.Gray);
BEGIN
  Trestle.Install(v);
  Trestle.AwaitDelete(v)
END VSplit.

