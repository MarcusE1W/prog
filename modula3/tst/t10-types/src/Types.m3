MODULE Types EXPORTS Main;
(* Describe Types and Standard Functions *)

IMPORT IO;

PROCEDURE NewLine() =
  BEGIN
    IO.Put("\n");
  END NewLine;

PROCEDURE TextOutput() =
  VAR i: INTEGER := 42;
  BEGIN
    (* String *)
    IO.Put( "First, print a string, line terminator is manual" & "\n" );

    (* Integer *)
    IO.PutInt(i);
    IO.Put("\n");
  END TextOutput;

PROCEDURE Numbers() =
  TYPE LovelyRange = [5..10];
  VAR i: INTEGER := 1;
      f: REAL;
  BEGIN
    IO.Put( "Numbers! \n");
    INC(i,3);
    DEC(i);
    
  END Numbers;

PROCEDURE Arrays() =
  (* one dimensional *)
  TYPE A1 = ARRAY [5..10] OF REAL;

  TYPE Test_Range = [1..10];
  TYPE Test_Array = ARRAY Test_Range OF BOOLEAN;
       
  (* two dimensional *)
  TYPE MD1_T = ARRAY [1..5],[1..7] OF BOOLEAN; (* simple, but not flexible to use*)
       
  TYPE MD2_T_line = ARRAY[1..4] OF BOOLEAN;
  TYPE MD2_T = ARRAY[1..5] OF MD2_T_line;

(* use array constructors to initiate array values *)
  VAR test_array : Test_Array := Test_Array{FALSE, ..}; (* initiate the singel dimensional array *)
  VAR md2: MD2_T := MD2_T{ MD2_T_line{FALSE, ..}, ..}; (* initiate multi-dimensional array *)

  (* CONST directions: ARRAY [1..8],[1..2] OF INTEGER = ARRAY [1..8],[1..2] OF INTEGER { *)
  (* ARRAY [1..2] OF INTEGER{-1,-1}, *)
  (* ARRAY [1..2] OF INTEGER{-1,0}, *)
  (* ARRAY [1..2] OF INTEGER{-1,1}, *)
  (* ARRAY [1..2] OF INTEGER{0,-1}, *)
  (* ARRAY [1..2] OF INTEGER{0,1}, *)
  (* ARRAY [1..2] OF INTEGER{1,-1}, *)
  (* ARRAY [1..2] OF INTEGER{1,0}, *)
  (* ARRAY [1..2] OF INTEGER{1,1} *)
  (* }; *)

      
  BEGIN
    IO.Put("Arrays! \n");
    IO.PutInt( NUMBER(md2) );
    NewLine();
    IO.PutInt( NUMBER(MD2_T) );
    NewLine();
    IO.PutInt( NUMBER(MD2_T_line) );
    NewLine();
    (* FIRST() *)
    (* LAST() *)
    (* SUBARRAY() *)
    
  END Arrays;

BEGIN
  TextOutput();
  Numbers();
  (* Text-String() *)
  Arrays();
  (* Exceptions() *)
END Types.
