MODULE Main;
(* Sieve *)

IMPORT IO;

CONST
    maxSieve: INTEGER = 100;

TYPE
    SieveRange = [2..maxSieve];
    SieveType = ARRAY SieveRange OF BOOLEAN;

VAR
    sieve: SieveType := SieveType{TRUE, ..}; (* initialise with TRUE *)

BEGIN
    IO.Put( "Let's print\n");

    FOR i:= FIRST(sieve) TO LAST(sieve) DO
        IF sieve[i] = TRUE THEN

           FOR j:= i TO LAST(sieve) BY i DO
               sieve[j] := FALSE;
           END (* FOR *);

           IO.PutInt(i); IO.Put("  ");
        END (* IF *);
    END (* FOR *);

    IO.Put("\n Done \n");
END Main.
