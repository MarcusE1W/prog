<*PRAGMA LL*>

MODULE Point2 EXPORTS Main;

IMPORT VBT, Trestle, Region, Rect, Point, PaintOp;

TYPE

  Mandel2VBT = VBT.Leaf OBJECT
    mandel2Rgn: Region.T;
  END;

VAR 
  v2 := NEW(Mandel2VBT, mandel2Rgn := Region.Empty);

  
PROCEDURE DrawPoints(v: Mandel2VBT) =

  CONST
    resX : INTEGER = 300;
    resY : INTEGER = 300;
    
  VAR
    region := Region.Empty;

  BEGIN
    FOR x := 1 TO resX DO
      FOR y := 1 TO resY DO
          WITH rect = Rect.FromPoint(Point.T{x, y}) DO
            region := Region.JoinRect(rect, region);
            Repaint(v, region);
          END;
      END; (*FOR y*)

    END; (*FOR x*)
END DrawPoints;


PROCEDURE Repaint(v: Mandel2VBT; READONLY rgn: Region.T) =
  BEGIN
    VBT.PaintRegion(v, rgn, PaintOp.Bg);
    VBT.PaintRegion(v, Region.Meet(v.mandel2Rgn, rgn), PaintOp.Fg) 
  END Repaint;


<*FATAL ANY*>
BEGIN
  Trestle.Install(v2);
  DrawPoints(v2);
  Trestle.AwaitDelete(v2)
  (* Close with Ctrl-C in terminal *)
END Point2.
