MODULE Hello EXPORTS Main;

IMPORT TextVBT, Trestle;

VAR v := TextVBT.New("Hello Trestle");

BEGIN
  Trestle.Install(v);
  Trestle.AwaitDelete(v)
END Hello.
