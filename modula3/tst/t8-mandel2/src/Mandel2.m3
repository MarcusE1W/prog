
<*PRAGMA LL*>

MODULE Mandel2 EXPORTS Main;

IMPORT VBT, Trestle, Region, Rect, Point, PaintOp;

TYPE
  (* MandelVBT = VBT.Leaf OBJECT  *)
  (*   mandelRgn : Region.T  *)
  (* OVERRIDES *)
  (*   mouse := Mouse; *)
  (*   repaint := Repaint; *)
  (*   reshape := Reshape *)
  (* END; *)

  Mandel2VBT = VBT.Leaf OBJECT
    mandel2Rgn: Region.T;
  (* METHODS *)
    (* init (): Mandel2VBT := Init *)
  (* OVERRIDES *)
  (*   repaint := Repaint; *)
  END;


  
VAR 
  (* v := NEW(MandelVBT, mandelRgn := MandelPaint()); *)
  v2 := NEW(Mandel2VBT, mandel2Rgn := Region.Empty);

(*  v2 := BorderedVBT.New(
            RigidVBT.FromHV(NEW(Mandel2VBT).init(), 100.0, 100.0));*)

(* PROCEDURE Init (v: Mandel2VBT): Mandel2VBT = *)
(*   BEGIN *)
(*     v.mandel2Rgn := NEW(Region.T); *)
(*     (\* TODO reset region ? *\) *)

(*     RETURN (v); *)
(*   END Init; *)


  
PROCEDURE MandelPoint( READONLY ix, iy : REAL; READONLY maxIter : INTEGER) : INTEGER =

VAR
  z0x, z0y, z1x, z1y : REAL := 0.0;
  r_abs : REAL := 0.0;
  iteration : INTEGER := 0;

BEGIN
  WHILE (iteration < maxIter) AND (r_abs < 4.0) DO
    z1x := z0x * z0x - z0y * z0y;
    z1y := 2.0 * (z0x * z0y); (* z0x * z0y + z0x * z0y *)
    
    z1x := z1x + ix;
    z1y := z1y + iy;
    
    z0x := z1x;
    z0y := z1y;
    
    r_abs := (z1x * z1x) + (z1y * z1y);
    iteration := iteration + 1;
  END; (* While*)
  RETURN iteration;
END MandelPoint;


(* PROCEDURE MandelPaint(v: Mandel2VBT): Region.T = *)
PROCEDURE MandelPaint(v: Mandel2VBT) =

  CONST
    resX : INTEGER = 300;
    resY : INTEGER = 300;
    
    iminX : REAL = -2.0;
    iminY : REAL = -1.7;
    imaxX : REAL = 1.5;
    imaxY : REAL = 1.7;

    maxIter : INTEGER = 100; 
    
    istepX : REAL = (imaxX - iminX) / FLOAT( resX );
    istepY : REAL = (imaxY - iminY) / FLOAT( resY );
        
  VAR
    region := Region.Empty;

    ix: REAL := iminX;
    iy: REAL := iminY;

    iter : INTEGER := 0;
    
  BEGIN
    FOR x := 1 TO resX DO
      iy := iminY;
      FOR y := 1 TO resY DO
        iter := MandelPoint ( ix, iy, maxIter);
        IF iter # maxIter THEN
          WITH rect = Rect.FromPoint(Point.T{x, y}) DO
            region := Region.JoinRect(rect, region);
            Repaint(v, region);
          END;
        END; (*IF*)
        iy := iy + istepY;
      END; (*FOR y*)
      ix := ix + istepX
    END; (*FOR x*)
    (* RETURN region; *)
END MandelPaint;
  
PROCEDURE Repaint(v: Mandel2VBT; READONLY rgn: Region.T) =
  BEGIN
    VBT.PaintRegion(v, rgn, PaintOp.Bg);
    VBT.PaintRegion(v, Region.Meet(v.mandel2Rgn, rgn), PaintOp.Fg) 
  END Repaint;

(* PROCEDURE Reshape(v: MandelVBT;  *)
(*     READONLY cd: VBT.ReshapeRec) = *)
(*   VAR delta := *)
(*     Point.Sub( *)
(*       Rect.Middle(cd.new),  *)
(*       Rect.Middle(v.mandelRgn.r)); *)
(*   BEGIN *)
(*     v.mandelRgn := Region.Add(v.mandelRgn, delta); *)
(*     Repaint(v, Region.Full) *)
(*   END Reshape; *)

(* PROCEDURE Mouse(v: MandelVBT; READONLY cd: VBT.MouseRec) = *)
  (* VAR delta: Point.T; *)
  (* BEGIN *)
  (*   IF cd.clickType = VBT.ClickType.FirstDown THEN *)
  (*     delta :=  *)
  (*       Point.Sub(cd.cp.pt, Rect.Middle(v.mandelRgn.r)); *)
  (*     v.mandelRgn := Region.Add(v.mandelRgn, delta); *)
  (*     Repaint(v, Region.Full) *)
  (*   END *)
  (* END Mouse; *)

<*FATAL ANY*>
BEGIN
  Trestle.Install(v2);
  MandelPaint(v2);
  Trestle.AwaitDelete(v2)
END Mandel2.
