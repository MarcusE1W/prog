MODULE A;
IMPORT IO;

PROCEDURE DoIt() = 
  BEGIN
    IO.Put("Hello World\n");
  END DoIt; 

BEGIN
END A.
