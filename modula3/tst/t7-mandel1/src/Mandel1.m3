(* Copyright (C) 1993, Digital Equipment Corporation           *)
(* All rights reserved.                                        *)
(* See the file COPYRIGHT for a full description.              *)
(*                                                             *)
(* Last Modified On Thu Jan 13 11:14:26 PST 1994 by kalsow     *)

<*PRAGMA LL*>

MODULE Mandel1 EXPORTS Main;

IMPORT VBT, Trestle, Region, Rect, Point, PaintOp;

TYPE
  MandelVBT = VBT.Leaf OBJECT 
    mandelRgn : Region.T 
  OVERRIDES
    mouse := Mouse;
    repaint := Repaint;
    reshape := Reshape
  END;
  
VAR 
  v := NEW(MandelVBT, mandelRgn := MandelPaint());

PROCEDURE MandelPoint( READONLY ix, iy : REAL; READONLY maxIter : INTEGER) : INTEGER =

VAR
  z0x, z0y, z1x, z1y : REAL := 0.0;
  r_abs : REAL := 0.0;
  iteration : INTEGER := 0;

BEGIN
  WHILE (iteration < maxIter) AND (r_abs < 4.0) DO
    z1x := z0x * z0x - z0y * z0y;
    z1y := 2.0 * (z0x * z0y); (* z0x * z0y + z0x * z0y *)
    
    z1x := z1x + ix;
    z1y := z1y + iy;
    
    z0x := z1x;
    z0y := z1y;
    
    r_abs := (z1x * z1x) + (z1y * z1y);
    iteration := iteration + 1;
  END; (* While*)
  RETURN iteration;
END MandelPoint;


PROCEDURE MandelPaint(): Region.T =

  CONST
    resX : INTEGER = 150;
    resY : INTEGER = 150;
    
    iminX : REAL = -2.0;
    iminY : REAL = -1.7;
    imaxX : REAL = 1.5;
    imaxY : REAL = 1.7;

    maxIter : INTEGER = 100; 
    
    istepX : REAL = (imaxX - iminX) / FLOAT( resX );
    istepY : REAL = (imaxY - iminY) / FLOAT( resY );
        
  VAR
    res := Region.Empty;

    ix: REAL := iminX;
    iy: REAL := iminY;

    iter : INTEGER := 0;
    
  BEGIN
    FOR x := 1 TO resX DO
      iy := iminY;
      FOR y := 1 TO resY DO
        iter := MandelPoint ( ix, iy, maxIter);
        IF iter # maxIter THEN
          WITH rect = Rect.FromPoint(Point.T{x, y}) DO
            res := Region.JoinRect(rect, res)
          END;
        END; (*IF*)
        iy := iy + istepY;
      END; (*FOR y*)
      ix := ix + istepX
    END; (*FOR x*)
    RETURN res;
END MandelPaint;
  
PROCEDURE Repaint(v: MandelVBT; READONLY rgn: Region.T) =
  BEGIN
    VBT.PaintRegion(v, rgn, PaintOp.Bg);
    VBT.PaintRegion(v, 
      Region.Meet(v.mandelRgn, rgn), PaintOp.Fg) 
  END Repaint;

PROCEDURE Reshape(v: MandelVBT; 
    READONLY cd: VBT.ReshapeRec) =
  VAR delta :=
    Point.Sub(
      Rect.Middle(cd.new), 
      Rect.Middle(v.mandelRgn.r));
  BEGIN
    v.mandelRgn := Region.Add(v.mandelRgn, delta);
    Repaint(v, Region.Full)
  END Reshape;

PROCEDURE Mouse(v: MandelVBT; READONLY cd: VBT.MouseRec) =
  VAR delta: Point.T;
  BEGIN
    IF cd.clickType = VBT.ClickType.FirstDown THEN
      delta := 
        Point.Sub(cd.cp.pt, Rect.Middle(v.mandelRgn.r));
      v.mandelRgn := Region.Add(v.mandelRgn, delta);
      Repaint(v, Region.Full)
    END
  END Mouse;

<*FATAL ANY*>
BEGIN
  Trestle.Install(v);
  Trestle.AwaitDelete(v)
END Mandel1.
