MODULE GameOfLife EXPORTS Main;

IMPORT IO;
(* IMPORT Random;  *)

TYPE LifeRange = [1..5];

CONST n : LifeRange = 5;
CONST m : LifeRange = 5;

TYPE Life_A_line = ARRAY [1..n] OF BOOLEAN;
TYPE Life_A = ARRAY [1..m] OF Life_A_line;

     (* direction
       -1,-1  -1,0  -1,1
        0,-1         0,1
        1,-1   1,0   1,1
        *)

CONST direction: ARRAY [1..8],[1..2] OF INTEGER = ARRAY [1..8],[1..2] OF INTEGER {
  ARRAY [1..2] OF INTEGER{-1,-1},
  ARRAY [1..2] OF INTEGER{-1,0},
  ARRAY [1..2] OF INTEGER{-1,1},
  ARRAY [1..2] OF INTEGER{0,-1},
  ARRAY [1..2] OF INTEGER{0,1},
  ARRAY [1..2] OF INTEGER{1,-1},
  ARRAY [1..2] OF INTEGER{1,0},
  ARRAY [1..2] OF INTEGER{1,1}
  };
     
PROCEDURE NewLine() =
  BEGIN
    IO.Put("\n");
  END NewLine;
     
(* PROCEDURE createLife() : Life_A = *)
(*   VAR a: Life_A := Life_A{ Life_A_line{FALSE, ..}, .. }; (\* initiate the ARRAY with FALSE, not really needed *\) *)
(*   BEGIN *)
(*     WITH rand = NEW(Random.Default).init() DO *)
(*       FOR i := FIRST(Life_A) TO LAST(Life_A) DO *)
(*         FOR j := FIRST(Life_A_line) TO LAST(Life_A_line) DO *)
(*             a[i,j] := rand.boolean(); *)
(*           END; (\*for*\) *)
(*         END; (\*for*\) *)
(*     END; (\*with*\) *)
(*     RETURN a; *)
(*   END createLife; *)

PROCEDURE createLife() : Life_A =
  VAR a: Life_A := Life_A{ Life_A_line{FALSE, ..},
                           Life_A_line{FALSE, TRUE, TRUE, FALSE, FALSE },
                           Life_A_line{TRUE, TRUE, FALSE, ..  },
                           Life_A_line{FALSE, FALSE, FALSE, TRUE, FALSE },
                           Life_A_line{FALSE, FALSE, TRUE, FALSE, FALSE }
                           }; 
  BEGIN
    RETURN a;
  END createLife;


PROCEDURE newGeneration(READONLY old: Life_A) : Life_A =
  VAR new: Life_A := Life_A{ Life_A_line{FALSE, ..}, .. }; (* initiate the ARRAY with FALSE *)
  VAR neighbour: INTEGER := 0;
  VAR x,y: INTEGER := 0;
  BEGIN
    IO.Put("++++++++++++++++++\n");
    FOR i := FIRST(Life_A) TO LAST(Life_A) DO
      FOR j := FIRST(Life_A_line) TO LAST(Life_A_line) DO

        neighbour :=0;
        FOR d := FIRST(direction) TO LAST(direction) DO
          x := i + direction[d,1];
          y := j + direction[d,2];
          IF x>0 AND x<=n AND y>0 AND y<=m AND old[x,y] THEN
            INC(neighbour);
          END (*if*);
        END (*for*);
        IO.PutInt(neighbour);
        
        IF old[i,j] AND (neighbour > 3 OR neighbour <2) THEN
          new[i,j] := FALSE;
        ELSIF NOT old[i,j] AND neighbour = 3 THEN
          new[i,j] := TRUE;
        ELSE
          new[i,j] := old[i,j];
        END (*if*);
        
      END (*for*);
      NewLine();
    END (*for*);
    IO.Put("+++++++++++++++++++++\n");
    RETURN new;
  END newGeneration;
  
PROCEDURE print_Life_Array( READONLY a : Life_A) =
  BEGIN
    NewLine();
    NewLine();
    IO.Put("========= \n");
    FOR i := FIRST(Life_A) TO LAST(Life_A) DO
      FOR j := FIRST(Life_A_line) TO LAST(Life_A_line) DO
        IF a[i,j] THEN
          IO.Put("O");
        ELSE
          IO.Put("-");
        END (* if *)
      END; (* for *)
      NewLine();
    END; (* for *)
  END print_Life_Array;

VAR life: Life_A; (* init comes anyway *)
(* VAR i: INTEGER; *)
BEGIN
  life := createLife();
  FOR i:=1 TO 7 DO
    IO.Put("+++ "); IO.PutInt(i); IO.Put(" +++\n");
    life := newGeneration(life);
    print_Life_Array(life);
  END (*for*);
END GameOfLife.
