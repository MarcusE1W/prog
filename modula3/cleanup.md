# Cleanup of Modula3 cm3 Github and webpage

## Github

### Main folder
[ ] move some stuff in subfolders

### Build
- [ ] consolidate build description
- [ ] there is a arm circle.ci script in .circle and the latest release mentions that it is regularuly build against ARM, however there are no binnary versions, not even X86.


### Main Readme
- [ ] Introduction to whats in the main folder
- [ ] reference to install instructions
- [ ] link to documentation


### Subfolder Readmes
- [ ] give subfolders a README where felpful to describe what's in there
- [ ] convert DESC to README.md


## Website
- [ ] rethink the approaxch for updates
- [ ] identify and update or fix or remove all broken links
