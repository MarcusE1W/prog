INTERFACE Ray;

IMPORT Ctypes;
IMPORT Word;

(* Direct C bindings *)
<* EXTERNAL raylib *>
(* CONST *)
  (* Library = "libraylib.so"; *)

(* C function declarations with EXTERNAL pragma *)
<* EXTERNAL "InitWindow" *>
PROCEDURE InitWindow_C(width, height: Ctypes.int; title: ADDRESS);

<* EXTERNAL "CloseWindow" *>
PROCEDURE CloseWindow_C();

<* EXTERNAL "WindowShouldClose" *>
PROCEDURE WindowShouldClose_C(): Ctypes.int;

<* EXTERNAL "SetTargetFPS" *>
PROCEDURE SetTargetFPS_C(fps: Ctypes.int);

<* EXTERNAL "BeginDrawing" *>
PROCEDURE BeginDrawing_C();

<* EXTERNAL "EndDrawing" *>
PROCEDURE EndDrawing_C();

<* EXTERNAL "ClearBackground" *>
PROCEDURE ClearBackground_C(color: Color_t);

<* EXTERNAL "DrawRectangle" *>
PROCEDURE DrawRectangle_C(posX, posY, width, height: Ctypes.int; color: Color_t);

(* Modula-3 friendly wrapper procedures *)
PROCEDURE InitWindow(width, height: INTEGER; title: TEXT);

PROCEDURE CloseWindow();

PROCEDURE WindowShouldClose(): BOOLEAN;

PROCEDURE SetTargetFPS(fps: INTEGER);

PROCEDURE BeginDrawing();

PROCEDURE EndDrawing();

PROCEDURE ClearBackground(color: Color);

PROCEDURE DrawRectangle(posX, posY, width, height: INTEGER; color: Color);

(* Predefined colors *)
CONST
  RAYWHITE = Color{r := 245, g := 245, b := 245, a := 255};
  PURPLE = Color{r := 200, g := 122, b := 255, a := 255};

END Ray.
