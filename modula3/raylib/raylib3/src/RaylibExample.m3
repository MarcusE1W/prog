MODULE RaylibExample EXPORTS Main;

IMPORT Ctypes;
IMPORT Word;
IMPORT Ray;

(* Main program *)
VAR
  screenWidth: INTEGER := 800;
  screenHeight: INTEGER := 450;
  
BEGIN
  (* Initialize window *)
  Ray.InitWindow(screenWidth, screenHeight, "Raylib Example - Modula-3");
  
  (* Set target FPS *)
  SetTargetFPS(60);
  
  (* Main game loop *)
  WHILE NOT WindowShouldClose() DO
    (* Begin drawing *)
    BeginDrawing();
    
    (* Clear background to white *)
    ClearBackground(RAYWHITE);
    
    (* Draw a purple rectangle in the center *)
    DrawRectangle(
      screenWidth DIV 4,    (* x position *)
      screenHeight DIV 4,   (* y position *)
      screenWidth DIV 2,    (* width *)
      screenHeight DIV 2,   (* height *)
      PURPLE               (* color *)
    );
    
    (* End drawing *)
    EndDrawing();
  END;
  
  (* De-initialize *)
  CloseWindow();
END RaylibExample.
