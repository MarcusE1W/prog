MODULE RaylibExample EXPORTS Main;

IMPORT Ctypes;
IMPORT Word;

(* Direct C bindings *)
<* EXTERNAL raylib *>
(* CONST *)
  (* Library = "libraylib.so"; *)

TYPE
  Color_t = RECORD
    r, g, b, a: Ctypes.unsigned_char;
  END;

  (* High-level Modula-3 friendly color type *)
  Color = RECORD
    r, g, b, a: Word.T;
  END;

(* C function declarations with EXTERNAL pragma *)
<* EXTERNAL "InitWindow" *>
PROCEDURE InitWindow_C(width, height: Ctypes.int; title: ADDRESS);

<* EXTERNAL "CloseWindow" *>
PROCEDURE CloseWindow_C();

<* EXTERNAL "WindowShouldClose" *>
PROCEDURE WindowShouldClose_C(): Ctypes.int;

<* EXTERNAL "SetTargetFPS" *>
PROCEDURE SetTargetFPS_C(fps: Ctypes.int);

<* EXTERNAL "BeginDrawing" *>
PROCEDURE BeginDrawing_C();

<* EXTERNAL "EndDrawing" *>
PROCEDURE EndDrawing_C();

<* EXTERNAL "ClearBackground" *>
PROCEDURE ClearBackground_C(color: Color_t);

<* EXTERNAL "DrawRectangle" *>
PROCEDURE DrawRectangle_C(posX, posY, width, height: Ctypes.int; color: Color_t);

(* Modula-3 friendly wrapper procedures *)
PROCEDURE InitWindow(width, height: INTEGER; title: TEXT) =
BEGIN
  InitWindow_C(Ctypes.int(width), Ctypes.int(height), M3toC(title));
END InitWindow;

PROCEDURE CloseWindow() =
BEGIN
  CloseWindow_C();
END CloseWindow;

PROCEDURE WindowShouldClose(): BOOLEAN =
BEGIN
  RETURN WindowShouldClose_C() # 0;
END WindowShouldClose;

PROCEDURE SetTargetFPS(fps: INTEGER) =
BEGIN
  SetTargetFPS_C(Ctypes.int(fps));
END SetTargetFPS;

PROCEDURE BeginDrawing() =
BEGIN
  BeginDrawing_C();
END BeginDrawing;

PROCEDURE EndDrawing() =
BEGIN
  EndDrawing_C();
END EndDrawing;

PROCEDURE ClearBackground(color: Color) =
VAR
  cColor: Color_t;
BEGIN
  cColor.r := Ctypes.unsigned_char(color.r);
  cColor.g := Ctypes.unsigned_char(color.g);
  cColor.b := Ctypes.unsigned_char(color.b);
  cColor.a := Ctypes.unsigned_char(color.a);
  ClearBackground_C(cColor);
END ClearBackground;

PROCEDURE DrawRectangle(posX, posY, width, height: INTEGER; color: Color) =
VAR
  cColor: Color_t;
BEGIN
  cColor.r := Ctypes.unsigned_char(color.r);
  cColor.g := Ctypes.unsigned_char(color.g);
  cColor.b := Ctypes.unsigned_char(color.b);
  cColor.a := Ctypes.unsigned_char(color.a);
  DrawRectangle_C(
    Ctypes.int(posX), 
    Ctypes.int(posY), 
    Ctypes.int(width), 
    Ctypes.int(height), 
    cColor
  );
END DrawRectangle;

(* Predefined colors *)
CONST
  RAYWHITE = Color{r := 245, g := 245, b := 245, a := 255};
  PURPLE = Color{r := 200, g := 122, b := 255, a := 255};

(* Main program *)
VAR
  screenWidth: INTEGER := 800;
  screenHeight: INTEGER := 450;
  
BEGIN
  (* Initialize window *)
  InitWindow(screenWidth, screenHeight, "Raylib Example - Modula-3");
  
  (* Set target FPS *)
  SetTargetFPS(60);
  
  (* Main game loop *)
  WHILE NOT WindowShouldClose() DO
    (* Begin drawing *)
    BeginDrawing();
    
    (* Clear background to white *)
    ClearBackground(RAYWHITE);
    
    (* Draw a purple rectangle in the center *)
    DrawRectangle(
      screenWidth DIV 4,    (* x position *)
      screenHeight DIV 4,   (* y position *)
      screenWidth DIV 2,    (* width *)
      screenHeight DIV 2,   (* height *)
      PURPLE               (* color *)
    );
    
    (* End drawing *)
    EndDrawing();
  END;
  
  (* De-initialize *)
  CloseWindow();
END RaylibExample.
