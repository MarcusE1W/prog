INTERFACE Raylib;

IMPORT Ctypes;

<*EXTERNAL "InitWindow"*> PROCEDURE InitWindow(width, height: Ctypes.int; title: Ctypes.char_star);
<*EXTERNAL "CloseWindow"*> PROCEDURE CloseWindow();
<*EXTERNAL "WindowShouldClose"*> PROCEDURE WindowShouldClose() : BOOLEAN;
<*EXTERNAL "BeginDrawing"*> PROCEDURE BeginDrawing();
<*EXTERNAL "EndDrawing"*> PROCEDURE EndDrawing();
<*EXTERNAL "ClearBackground"*> PROCEDURE ClearBackground(color: Ctypes.unsigned_int);
<*EXTERNAL "DrawRectangle"*> PROCEDURE DrawRectangle(x, y, width, height: Ctypes.int; color: Ctypes.unsigned_int);

END Raylib.
