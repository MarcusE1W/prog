UNSAFE MODULE Main;

IMPORT Raylib, Ctypes, M3toC;

CONST
  ScreenWidth = 800;
  ScreenHeight = 450;
  Title = "Modula-3 + Raylib";

BEGIN
  (* Initialize the window *)
  Raylib.InitWindow(ScreenWidth, ScreenHeight, M3toC.CopyTtoS(Title));

  (* Main loop *)
  WHILE NOT Raylib.WindowShouldClose() DO (* Crash? no boolean conversion from C *)
    (* Start drawing *)
    Raylib.BeginDrawing();

    (* Clear the background to white *)
    Raylib.ClearBackground(16_FFFFFFFF); (* White color in ARGB format *)

    (* Draw a red rectangle *)
    Raylib.DrawRectangle(100, 100, 200, 150, 16_FF0000FF); (* Red color in ARGB format *)

    (* End drawing *)
    Raylib.EndDrawing();
  END;

  (* Close the window *)
  Raylib.CloseWindow();
END Main.
